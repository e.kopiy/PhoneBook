unit uPBGenerator;

interface

uses
  System.Classes, LibXL, ShellAPI, Winapi.Windows, SysUtils, Vcl.Dialogs,
  Vcl.ComCtrls, Vcl.Forms;

type
  TPBGenerateThread = class(TThread)
  private
    { Private declarations }
    Progress: integer;
    Success: Boolean;
    function PBGenerate: boolean;
    function GetShortName(id_dep: integer): String;
    function GetObjType(id_dep: String): String;
    function CheckEmplInDep(id_dep: integer): boolean;
    function GetObjName(id_dep: integer): String;
    function ServiceIsWell(ServId: integer): boolean;
    procedure SetProgress;
    procedure FreeVCL;
    procedure FillEmptyService(IdServ: Integer; InpSheet: TSheet; InpRowToWrite: integer; CasualFormat: TFormat; ServFormat: TFormat; var OutSheet: TSheet; var OutRowToWrite: integer);
    procedure FillNotEmptyService(IdServ: Integer; InpSheet: TSheet; InpRowToWrite: integer; CasualFormat: TFormat; ServFormat: TFormat; var OutSheet: TSheet; var OutRowToWrite: integer);
    procedure FillDepart(IdDep: Integer; InpSheet: TSheet; InpRowToWrite: integer; CasualFormat: TFormat; DepFormat: TFormat; var OutSheet: TSheet; var OutRowToWrite: integer);
    procedure FillSubDepart(IdDep: Integer; InpSheet: TSheet; InpRowToWrite: integer; CasualFormat: TFormat; SubDepFormat: TFormat; var OutSheet: TSheet; var OutRowToWrite: integer);
  protected
    procedure Execute; override;
  end;

implementation

uses main, DataMod;

{ 
  Important: Methods and properties of objects in visual components can only be
  used in a method called using Synchronize, for example,

      Synchronize(UpdateCaption);  

  and UpdateCaption could look like,

    procedure TPBGenerateThread.UpdateCaption;
    begin
      Form1.Caption := 'Updated in a thread';
    end; 
    
    or 
    
    Synchronize( 
      procedure 
      begin
        Form1.Caption := 'Updated in thread via an anonymous method' 
      end
      )
    );
    
  where an anonymous method is passed.
  
  Similarly, the developer can call the Queue method with similar parameters as 
  above, instead passing another TThread class as the first parameter, putting
  the calling thread in a queue with the other thread.
    
}

{ TPBGenerateThread }

function TPBGenerateThread.GetObjType(id_dep: String): String;
var
  i: Integer;
  IdPtr: ^Integer;
begin
  for i := 0 to Form1.DepTree.Items.Count - 1 do
  begin
    IdPtr:= Form1.DepTree.Items[i].Data;
    if IdPtr=nil then
      raise Exception.Create('IdPtr is nil');
    if StrToInt(id_dep)=IdPtr^ then
    begin
      if Form1.DepTree.Items[i].Level=0 then  Result:= '������';
      if Form1.DepTree.Items[i].Level=1 then  Result:= '�����';
      if Form1.DepTree.Items[i].Level=2 then  Result:= '���������';
    end;
  end;
end;

function TPBGenerateThread.GetShortName(id_dep: integer): String;
begin
  dm.DoQuery(dm.RTQuery, 'SELECT dep_name_short FROM dep_list WHERE id_dep=' + IntToStr(id_dep));
  Result:= dm.RTQuery.FieldByName('dep_name_short').AsString;
end;

function TPBGenerateThread.GetObjName(id_dep: integer): String;
begin
  dm.DoQuery(dm.RTQuery, 'SELECT dep_name FROM dep_list WHERE id_dep=' + IntToStr(id_dep));
  Result:= dm.RTQuery.FieldByName('dep_name').AsString;
end;

function FindParent(Node, ParentNode: TTreeNode): boolean;
begin
  Result:=false;
  Repeat
    if Node=ParentNode then
    begin
      Result:=true;
      break;
    end;
    Node:=Node.Parent;
  until not Assigned(Node);
end;

function TPBGenerateThread.CheckEmplInDep(id_dep: integer): boolean;
begin
  Result:= False;
  dm.DoQuery(dm.RTQuery, 'SELECT * FROM empl_list WHERE id_dep=' + IntToStr(id_dep));
  if not dm.RTQuery.IsEmpty then
    Result:= True;
end;

procedure TPBGenerateThread.FillEmptyService(IdServ: Integer; InpSheet: TSheet; InpRowToWrite: integer; CasualFormat: TFormat; ServFormat: TFormat; var OutSheet: TSheet; var OutRowToWrite: integer);
var
  BufSheet: TSheet;
  BufRowToWrite: integer;
  dep_name: PAnsiChar;
begin
  dep_name:= PAnsiChar(AnsiString(GetObjName(IdServ)));
  BufSheet:= InpSheet;
  BufRowToWrite:= InpRowToWrite;
  BufSheet.writeStr(BufRowToWrite, 1, dep_name, CasualFormat);
  BufRowToWrite:= 6;
  BufSheet.writeStr(BufRowToWrite, 1, dep_name, ServFormat);
  BufSheet.setRow(BufRowToWrite, 48);
  BufRowToWrite:= 10;
  inc(BufRowToWrite, 1);

  OutSheet:= BufSheet;
  OutRowToWrite:= BufRowToWrite;
end;

procedure TPBGenerateThread.FillNotEmptyService(IdServ: Integer; InpSheet: TSheet; InpRowToWrite: integer; CasualFormat: TFormat; ServFormat: TFormat; var OutSheet: TSheet; var OutRowToWrite: integer);
var
  SqlStr: String;
  BufSheet: TSheet;
  BufRowToWrite: integer;
  dep_name, empl_post, empl_FIO, empl_int_phone, empl_fax, empl_te_number: PAnsiChar;
begin
  dep_name:= PAnsiChar(AnsiString(GetObjName(IdServ)));
  BufSheet:= InpSheet;
  BufRowToWrite:= InpRowToWrite;
  BufSheet.writeStr(BufRowToWrite, 1, dep_name, CasualFormat);
  BufRowToWrite:= 6;
  BufSheet.writeStr(BufRowToWrite, 1, dep_name, ServFormat);
  BufSheet.setRow(BufRowToWrite, 48);
  BufRowToWrite:= 10;
  SqlStr:= 'SELECT empl_post, CONCAT (empl_fname,' + QuotedStr(' ');
  SqlStr:= SqlStr + ', empl_lfname,' + QuotedStr(' ') + ', empl_lname) ';
  SqlStr:= SqlStr + 'as `���`, empl_int_phone, empl_fax, empl_city_te_number';
  SqlStr:= SqlStr + ', id_dep FROM empl_list WHERE id_dep=' + IntToStr(IdServ);
  SqlStr:= SqlStr + ' order by id_dep, empl_post';
  dm.DoQuery(dm.RTQuery, SqlStr);
  while not dm.RTQuery.Eof do
  begin
    empl_post:= PAnsiChar(AnsiString(dm.RTQuery.FieldByName('empl_post').AsString));
    empl_FIO:= PAnsiChar(AnsiString(dm.RTQuery.FieldByName('���').AsString));
    BufSheet.writeStr(BufRowToWrite, 1, empl_post, CasualFormat);
    BufSheet.writeStr(BufRowToWrite, 2, empl_FIO, CasualFormat);
    if not dm.RTQuery.FieldByName('empl_int_phone').AsString.IsEmpty then
    begin
      empl_int_phone:= PAnsiChar(AnsiString(dm.RTQuery.FieldByName('empl_int_phone').AsString));
      BufSheet.writeStr(BufRowToWrite, 4, empl_int_phone, CasualFormat);
    end;
    if not dm.RTQuery.FieldByName('empl_fax').AsString.IsEmpty then
    begin
      empl_fax:= PAnsiChar(AnsiString(dm.RTQuery.FieldByName('empl_fax').AsString));
      BufSheet.writeStr(BufRowToWrite, 5, empl_fax, CasualFormat);
    end;
    if not dm.RTQuery.FieldByName('empl_city_te_number').AsString.IsEmpty and not dm.RTQuery.FieldByName('empl_int_phone').AsString.Equals('0') then
    begin
      empl_te_number:= PAnsiChar(AnsiString(dm.RTQuery.FieldByName('empl_city_te_number').AsString));
      BufSheet.writeStr(BufRowToWrite, 6, empl_te_number, CasualFormat);
    end;
    inc(BufRowToWrite);
    dm.RTQuery.Next;
  end;
  inc(BufRowToWrite, 1);

  OutSheet:= BufSheet;
  OutRowToWrite:= BufRowToWrite;
end;

procedure TPBGenerateThread.FillDepart(IdDep: Integer; InpSheet: TSheet; InpRowToWrite: integer; CasualFormat: TFormat; DepFormat: TFormat; var OutSheet: TSheet; var OutRowToWrite: integer);
var
  SqlStr: String;
  BufSheet: TSheet;
  BufRowToWrite: integer;
  dep_name, empl_post, empl_FIO, empl_int_phone, empl_fax, empl_te_number: PAnsiChar;
begin
  dep_name:= PAnsiChar(AnsiString(GetObjName(IdDep)));
  BufSheet:= InpSheet;
  BufRowToWrite:= InpRowToWrite;
  BufSheet.writeStr(BufRowToWrite, 1, dep_name, DepFormat);
  BufSheet.setMerge(BufRowToWrite, BufRowToWrite, 1, 6);
  BufSheet.setRow(BufRowToWrite, 49, CasualFormat, False);
  inc(BufRowToWrite);
  BufSheet.setRow(BufRowToWrite, 8, CasualFormat, False);
  inc(BufRowToWrite);
  SqlStr:= 'SELECT empl_post, CONCAT (empl_fname,' + QuotedStr(' ');
  SqlStr:= SqlStr + ', empl_lfname,' + QuotedStr(' ') + ', empl_lname) ';
  SqlStr:= SqlStr + 'as `���`, empl_int_phone, empl_fax, empl_city_te_number';
  SqlStr:= SqlStr + ', id_dep FROM empl_list WHERE id_dep=' + IntToStr(IdDep);
  SqlStr:= SqlStr + ' order by id_dep, empl_post';
  dm.DoQuery(dm.RTQuery, SqlStr);
  while not dm.RTQuery.Eof do
  begin
    empl_post:= PAnsiChar(AnsiString(dm.RTQuery.FieldByName('empl_post').AsString));
    empl_FIO:= PAnsiChar(AnsiString(dm.RTQuery.FieldByName('���').AsString));
    BufSheet.writeStr(BufRowToWrite, 1, empl_post, CasualFormat);
    BufSheet.writeStr(BufRowToWrite, 2, empl_FIO, CasualFormat);
    if not dm.RTQuery.FieldByName('empl_int_phone').AsString.IsEmpty then
    begin
      empl_int_phone:= PAnsiChar(AnsiString(dm.RTQuery.FieldByName('empl_int_phone').AsString));
      BufSheet.writeStr(BufRowToWrite, 4, empl_int_phone, CasualFormat);
    end;
    if not dm.RTQuery.FieldByName('empl_fax').AsString.IsEmpty then
    begin
      empl_fax:= PAnsiChar(AnsiString(dm.RTQuery.FieldByName('empl_fax').AsString));
      BufSheet.writeStr(BufRowToWrite, 5, empl_fax, CasualFormat);
    end;
    if not dm.RTQuery.FieldByName('empl_city_te_number').AsString.IsEmpty and not dm.RTQuery.FieldByName('empl_int_phone').AsString.Equals('0') then
    begin
      empl_te_number:= PAnsiChar(AnsiString(dm.RTQuery.FieldByName('empl_city_te_number').AsString));
      BufSheet.writeStr(BufRowToWrite, 6, empl_te_number, CasualFormat);
    end;
    inc(BufRowToWrite);
    dm.RTQuery.Next;
  end;
  inc(BufRowToWrite, 1);

  OutSheet:= BufSheet;
  OutRowToWrite:= BufRowToWrite;
end;

procedure TPBGenerateThread.FillSubDepart(IdDep: Integer; InpSheet: TSheet; InpRowToWrite: integer; CasualFormat: TFormat; SubDepFormat: TFormat; var OutSheet: TSheet; var OutRowToWrite: integer);
var
  SqlStr: String;
  BufSheet: TSheet;
  BufRowToWrite: integer;
  dep_name, empl_post, empl_FIO, empl_int_phone, empl_fax, empl_te_number: PAnsiChar;
begin
  dep_name:= PAnsiChar(AnsiString(GetObjName(IdDep)));
  BufSheet:= InpSheet;
  BufRowToWrite:= InpRowToWrite;
  BufSheet.writeStr(BufRowToWrite, 1, dep_name, SubDepFormat);
  BufSheet.setMerge(BufRowToWrite, BufRowToWrite, 1, 6);
  BufSheet.setRow(BufRowToWrite, 40, CasualFormat, False);
  inc(BufRowToWrite);
  BufSheet.setRow(BufRowToWrite, 8, CasualFormat, False);
  inc(BufRowToWrite);
  SqlStr:= 'SELECT empl_post, CONCAT (empl_fname,' + QuotedStr(' ');
  SqlStr:= SqlStr + ', empl_lfname,' + QuotedStr(' ') + ', empl_lname) ';
  SqlStr:= SqlStr + 'as `���`, empl_int_phone, empl_fax, empl_city_te_number';
  SqlStr:= SqlStr + ', id_dep FROM empl_list WHERE id_dep=' + IntToStr(IdDep);
  SqlStr:= SqlStr + ' order by id_dep, empl_post';
  dm.DoQuery(dm.RTQuery, SqlStr);
  while not dm.RTQuery.Eof do
  begin
    empl_post:= PAnsiChar(AnsiString(dm.RTQuery.FieldByName('empl_post').AsString));
    empl_FIO:= PAnsiChar(AnsiString(dm.RTQuery.FieldByName('���').AsString));
    BufSheet.writeStr(BufRowToWrite, 1, empl_post, CasualFormat);
    BufSheet.writeStr(BufRowToWrite, 2, empl_FIO, CasualFormat);
    if not dm.RTQuery.FieldByName('empl_int_phone').AsString.IsEmpty then
    begin
      empl_int_phone:= PAnsiChar(AnsiString(dm.RTQuery.FieldByName('empl_int_phone').AsString));
      BufSheet.writeStr(BufRowToWrite, 4, empl_int_phone, CasualFormat);
    end;
    if not dm.RTQuery.FieldByName('empl_fax').AsString.IsEmpty then
    begin
      empl_fax:= PAnsiChar(AnsiString(dm.RTQuery.FieldByName('empl_fax').AsString));
      BufSheet.writeStr(BufRowToWrite, 5, empl_fax, CasualFormat);
    end;
    if not dm.RTQuery.FieldByName('empl_city_te_number').AsString.IsEmpty and not dm.RTQuery.FieldByName('empl_int_phone').AsString.Equals('0') then
    begin
      empl_te_number:= PAnsiChar(AnsiString(dm.RTQuery.FieldByName('empl_city_te_number').AsString));
      BufSheet.writeStr(BufRowToWrite, 6, empl_te_number, CasualFormat);
    end;
    inc(BufRowToWrite);
    dm.RTQuery.Next;
  end;
  inc(BufRowToWrite, 1);

  OutSheet:= BufSheet;
  OutRowToWrite:= BufRowToWrite;
end;

function TPBGenerateThread.ServiceIsWell(ServId: integer): boolean;
// ���������, ���� �� ���������� � ������ ��� � ����������� ��������
var
  IdString: String;
begin
  Result:= False;
  IdString:= Form1.GetChilds(ServId);
  if not IdString.IsEmpty then
  begin
    dm.DoQuery(dm.RTQuery, 'SELECT * FROM empl_list WHERE id_dep in (' + IdString + ')');
    if not dm.RTQuery.IsEmpty then
      Result:= True;
  end;
end;

procedure TPBGenerateThread.SetProgress;
begin
  Form1.ProgressBar1.Position:=Progress;
end;

procedure TPBGenerateThread.FreeVCL;
begin
  Form1.Button2.Enabled:= True;
  Form1.ProgressBar1.Position:=0;
  Form1.ProgressBar1.Visible:= False;
end;

function TPBGenerateThread.PBGenerate: boolean;
var
  Book: TBook;
  ActiveSheet, TPL_Sheet, StylesSheet: TSheet;
  FCasualText, FServName, FDepName, FSubDepName : TFormat;
  Node: TTreeNode;
  IdPtr: ^Integer;
  i, RowToWrite, ProgressStep, StepCnt: Integer;
  AnsiRepDateTime: PAnsiChar;
  StrRepDateTime: PChar;
  PathToRep, short_name: PAnsiChar;
  RepDateTime : TDateTime;
begin
  Success:= False;
  Book := TBinBook.Create;
  Book.load('tpl.xls');
  StylesSheet:= Book.getSheet(1);
  FCasualText:= StylesSheet.cellFormat(10, 1);
  FCasualText.wrap:= True;
  FServName:= StylesSheet.cellFormat(6, 1);
  FDepName:= StylesSheet.cellFormat(15, 1);
  FSubDepName:= StylesSheet.cellFormat(14, 1);
  FServName.wrap:= True;
  FDepName.wrap:= True;
  FSubDepName.wrap:= True;
  TPL_Sheet:= Book.getSheet(2);
  Progress:= 0;
  Synchronize(SetProgress);
  ProgressStep:= Round((Form1.DepTree.Items.Count/100) + 1);
  StepCnt:= 0;
  try
  for i:= 0 to Form1.DepTree.Items.Count - 1 do
  begin
    inc(StepCnt);
    if StepCnt=ProgressStep then
    begin
      inc(Progress);
      Synchronize(SetProgress);
      StepCnt:= 0;
    end;
    if Form1.DepTree.Items[i].Level = 0 then
    begin
      RowToWrite:= 0;
      IdPtr:= Form1.DepTree.Items[i].Data;
      if IdPtr=nil then
        raise Exception.Create('IdPtr is nil');
      if ServiceIsWell(IdPtr^) then
      begin
        if CheckEmplInDep(IdPtr^) then
        begin
          short_name:= PAnsiChar(AnsiString(GetShortName(IdPtr^)));
          ActiveSheet:= Book.addSheet(short_name, TPL_Sheet);
          ActiveSheet.setPrintArea(0, 100, 1, 6);
          FillNotEmptyService(IdPtr^, ActiveSheet, 0, FCasualText, FServName, ActiveSheet, RowToWrite);
          Node:= Form1.DepTree.Items[i].GetNext;
          if Assigned(Node) then
          begin
            repeat
              IdPtr:= Node.Data;
              if IdPtr=nil then
                raise Exception.Create('IdPtr is nil');
              if CheckEmplInDep(IdPtr^) and GetObjType(IntToStr(IdPtr^)).Equals('�����') then
              begin
                FillDepart(IdPtr^, ActiveSheet, RowToWrite, FCasualText, FDepName, ActiveSheet, RowToWrite);
              end;
              if CheckEmplInDep(IdPtr^) and GetObjType(IntToStr(IdPtr^)).Equals('���������') then
              begin
                FillSubDepart(IdPtr^, ActiveSheet, RowToWrite, FCasualText, FSubDepName, ActiveSheet, RowToWrite);
              end;
              Node:=Node.GetNext;
            until not (Assigned(Node)) or not FindParent(Node,Form1.DepTree.Items[i]);
          end;
        end
        else
        begin
          short_name:= PAnsiChar(AnsiString(GetShortName(IdPtr^)));
          ActiveSheet:= Book.addSheet(short_name, TPL_Sheet);
          ActiveSheet.clearPrintArea;
          ActiveSheet.setPrintArea(0, 100, 1, 6);
          FillEmptyService(IdPtr^, ActiveSheet, 0, FCasualText, FServName, ActiveSheet, RowToWrite);
          Node:= Form1.DepTree.Items[i].GetNext;
          if Assigned(Node) then
          begin
            repeat
              IdPtr:= Node.Data;
              if IdPtr=nil then
                raise Exception.Create('IdPtr is nil {PBGenerate}');
              if CheckEmplInDep(IdPtr^) and GetObjType(IntToStr(IdPtr^)).Equals('�����') then
              begin
                FillDepart(IdPtr^, ActiveSheet, RowToWrite, FCasualText, FDepName, ActiveSheet, RowToWrite);
              end;
              if CheckEmplInDep(IdPtr^) and GetObjType(IntToStr(IdPtr^)).Equals('���������') then
              begin
                FillSubDepart(IdPtr^, ActiveSheet, RowToWrite, FCasualText, FSubDepName, ActiveSheet, RowToWrite);
              end;
              Node:=Node.GetNext;
            until not (Assigned(Node)) or not FindParent(Node,Form1.DepTree.Items[i]);
          end;
        end;
        ActiveSheet.setCol(7, 0);//Fix bug with printarea width
        ActiveSheet.setCol(8, 0);//Fix bug with printarea width
        ActiveSheet.setCol(9, 0);//Fix bug with printarea width
        ActiveSheet.setPrintArea(0, RowToWrite + 1, 1, 6);
      end;
    end;
  end;
  except
    on E: Exception do begin ShowMessage('������!#'+E.Message+'# I=' + IntToStr(i) + ', RowToWrite=' + IntToStr(RowToWrite) + ', Sheet.name=' + String(ActiveSheet.name) + ', ID dep=' + IntToStr(IdPtr^) + ', DepName=' + GetObjName(IdPtr^)); end;
  end;
  Book.delSheet(1); //Delete StylesSheet
  Book.delSheet(1); //Delete TPL Sheet

  Progress:= 100;
  Synchronize(SetProgress);

  RepDateTime:= Now;
  AnsiRepDateTime:= PAnsiChar(AnsiString(FormatDateTime('dd.mm.yy hh_mm_ss', RepDateTime)));
  PathToRep:= PAnsiChar(AnsiString(ExtractFilePath(Application.ExeName) + '����������\'+'���������� ' + AnsiRepDateTime + '.xls'));
  StrRepDateTime:= PChar(ExtractFilePath(Application.ExeName) + '����������\'+'���������� ' + String(AnsiRepDateTime) + '.xls');
  try
    Book.save(PathToRep);
    Synchronize(FreeVCL);
    ShowMessage('��������� ����������� ���������. ����� ��������, ����������, �������� ������� ��� ��������� ����������');
    ShellExecute(0, 'open', StrRepDateTime, nil, nil, SW_SHOW);
    Book.Free;
    Result:= True;
  except
    on E: Exception do
    begin
      ShowMessage('������ ���������� �����. �������� �� ������ ��� ����� ����� ������� "������ ��� ������"?');
      Book.Free;
      Synchronize(FreeVCL);
      Result:= False;
      exit;
    end;
  end;
end;

procedure TPBGenerateThread.Execute;
begin
  PBGenerate;
end;

end.
