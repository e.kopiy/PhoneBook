unit confirmForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm2 = class(TForm)
    Label1: TLabel;
    Button1: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;
  confirmed: boolean;

implementation

{$R *.dfm}

uses addObjectForm;

procedure TForm2.Button1Click(Sender: TObject);
begin
  confirmed:= True;
  Form2.Close;
end;

procedure TForm2.Button2Click(Sender: TObject);
begin
  confirmed:= False;
  Form2.Close;
end;

procedure TForm2.FormShow(Sender: TObject);
begin
  confirmed:= False;
  Button2.Left:= Round(Form2.Width/2) + 3;
  Button1.Left:= (Button2.Left) - 84;
end;

end.
