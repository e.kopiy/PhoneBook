object Form3: TForm3
  Left = 309
  Top = 169
  BorderStyle = bsSingle
  Caption = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1086#1073#1098#1077#1082#1090#1072
  ClientHeight = 610
  ClientWidth = 817
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label7: TLabel
    Left = 8
    Top = 16
    Width = 117
    Height = 13
    Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1090#1080#1087' '#1086#1073#1098#1077#1082#1090#1072
  end
  object Button1: TButton
    Left = 382
    Top = 503
    Width = 97
    Height = 42
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 279
    Top = 503
    Width = 97
    Height = 42
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 1
    OnClick = Button2Click
  end
  object SheetObjectData: TPageControl
    Left = 8
    Top = 64
    Width = 801
    Height = 417
    ActivePage = EmplSheet
    TabOrder = 2
    OnChange = SheetObjectDataChange
    object EmplSheet: TTabSheet
      Caption = 'EmplSheet'
      TabVisible = False
      object Label1: TLabel
        Left = 394
        Top = 9
        Width = 44
        Height = 13
        Caption = #1060#1072#1084#1080#1083#1080#1103
      end
      object Label8: TLabel
        Left = 394
        Top = 53
        Width = 19
        Height = 13
        Caption = #1048#1084#1103
      end
      object Label9: TLabel
        Left = 394
        Top = 99
        Width = 49
        Height = 13
        Caption = #1054#1090#1095#1077#1089#1090#1074#1086
      end
      object Label2: TLabel
        Left = 11
        Top = 9
        Width = 144
        Height = 13
        Caption = #1058#1080#1087' '#1074#1099#1096#1077#1089#1090#1086#1103#1097#1077#1075#1086' '#1086#1073#1098#1077#1082#1090#1072
      end
      object Label3: TLabel
        Left = 11
        Top = 53
        Width = 199
        Height = 13
        Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1074#1099#1096#1077#1089#1090#1086#1103#1097#1077#1075#1086' '#1086#1073#1098#1077#1082#1090#1072
      end
      object Label4: TLabel
        Left = 394
        Top = 146
        Width = 121
        Height = 13
        Caption = #1053#1086#1084#1077#1088' '#1090#1077#1083#1077#1092#1086#1085#1072' '#1073#1077#1079' +7'
      end
      object Label20: TLabel
        Left = 394
        Top = 190
        Width = 102
        Height = 13
        Caption = #1053#1086#1084#1077#1088' '#1092#1072#1082#1089#1072' '#1073#1077#1079' +7'
      end
      object Label23: TLabel
        Left = 394
        Top = 236
        Width = 146
        Height = 13
        Caption = #1042#1085#1091#1090#1088#1077#1085#1085#1080#1081' '#1085#1086#1084#1077#1088' '#1090#1077#1083#1077#1092#1086#1085#1072
      end
      object Label5: TLabel
        Left = 394
        Top = 282
        Width = 31
        Height = 13
        Caption = #1040#1076#1088#1077#1089
      end
      object Label6: TLabel
        Left = 394
        Top = 327
        Width = 43
        Height = 13
        Caption = #1050#1072#1073#1080#1085#1077#1090
      end
      object Label10: TLabel
        Left = 521
        Top = 146
        Width = 102
        Height = 13
        Caption = #1092#1086#1088#1084#1072#1090': ('#1082#1086#1076')'#1085#1086#1084#1077#1088
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGray
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsItalic]
        ParentFont = False
      end
      object Label21: TLabel
        Left = 502
        Top = 190
        Width = 102
        Height = 13
        Caption = #1092#1086#1088#1084#1072#1090': ('#1082#1086#1076')'#1085#1086#1084#1077#1088
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGray
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsItalic]
        ParentFont = False
      end
      object Label17: TLabel
        Left = 11
        Top = 99
        Width = 343
        Height = 13
        Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1074#1099#1096#1077#1089#1090#1086#1103#1097#1080#1081' '#1086#1073#1098#1077#1082#1090' ('#1056#1086#1076#1080#1090#1077#1083#1100#1089#1082#1080#1081' '#1086#1073#1098#1077#1082#1090'->'#1054#1073#1098#1077#1082#1090'):'
      end
      object Label13: TLabel
        Left = 11
        Top = 327
        Width = 57
        Height = 13
        Caption = #1044#1086#1083#1078#1085#1086#1089#1090#1100
      end
      object Label25: TLabel
        Left = 11
        Top = 281
        Width = 113
        Height = 13
        Caption = #1053#1086#1084#1077#1088' '#1090#1077#1083#1077#1092#1086#1085#1072' '#1043#1040#1058#1057
      end
      object EdLastName: TEdit
        Left = 394
        Top = 28
        Width = 366
        Height = 21
        TabOrder = 0
      end
      object EdFirstName: TEdit
        Left = 394
        Top = 72
        Width = 366
        Height = 21
        TabOrder = 1
      end
      object EdMidName: TEdit
        Left = 393
        Top = 118
        Width = 366
        Height = 21
        TabOrder = 2
      end
      object CmbUpperObjType: TComboBox
        Left = 11
        Top = 28
        Width = 367
        Height = 21
        Style = csDropDownList
        ItemIndex = 0
        TabOrder = 3
        Text = #1057#1083#1091#1078#1073#1072
        OnChange = CmbUpperObjTypeChange
        Items.Strings = (
          #1057#1083#1091#1078#1073#1072
          #1054#1090#1076#1077#1083
          #1054#1090#1076#1077#1083#1077#1085#1080#1077)
      end
      object EdMobilePhone: TEdit
        Left = 394
        Top = 165
        Width = 367
        Height = 21
        TabOrder = 4
      end
      object EdFax: TEdit
        Left = 394
        Top = 210
        Width = 367
        Height = 21
        TabOrder = 5
      end
      object EdInternalPhone: TEdit
        Left = 394
        Top = 255
        Width = 367
        Height = 21
        TabOrder = 6
      end
      object EdAddress: TEdit
        Left = 394
        Top = 300
        Width = 367
        Height = 21
        TabOrder = 7
      end
      object EdRoomNumb: TEdit
        Left = 394
        Top = 347
        Width = 367
        Height = 21
        TabOrder = 8
      end
      object ChkBoxIsMobile: TCheckBox
        Left = 641
        Top = 145
        Width = 97
        Height = 17
        Caption = #1054#1090#1089#1091#1090#1089#1090#1074#1091#1077#1090
        TabOrder = 9
        OnClick = ChkBoxIsMobileClick
      end
      object ChkBoxIsFax: TCheckBox
        Left = 627
        Top = 191
        Width = 97
        Height = 12
        Caption = #1054#1090#1089#1091#1090#1089#1090#1074#1091#1077#1090
        Checked = True
        State = cbChecked
        TabOrder = 10
        OnClick = ChkBoxIsFaxClick
      end
      object ChkBoxIsInternal: TCheckBox
        Left = 548
        Top = 235
        Width = 97
        Height = 17
        Caption = #1054#1090#1089#1091#1090#1089#1090#1074#1091#1077#1090
        TabOrder = 11
        OnClick = ChkBoxIsInternalClick
      end
      object Edit4: TEdit
        Left = 11
        Top = 72
        Width = 367
        Height = 21
        TabOrder = 12
        OnChange = Edit4Change
      end
      object ListBox1: TListBox
        Left = 11
        Top = 118
        Width = 367
        Height = 116
        ItemHeight = 13
        ParentShowHint = False
        ShowHint = True
        TabOrder = 13
        OnClick = ListBox1Click
      end
      object Edit7: TEdit
        Left = 11
        Top = 347
        Width = 367
        Height = 21
        TabOrder = 14
      end
      object CheckBox1: TCheckBox
        Left = 130
        Top = 280
        Width = 87
        Height = 17
        Caption = #1054#1090#1089#1091#1090#1089#1090#1074#1091#1077#1090
        TabOrder = 15
        OnClick = CheckBox1Click
      end
      object Edit8: TEdit
        Left = 11
        Top = 300
        Width = 367
        Height = 21
        NumbersOnly = True
        TabOrder = 16
      end
      object Memo2: TMemo
        Left = 11
        Top = 240
        Width = 367
        Height = 34
        Hint = #1055#1086#1083#1085#1086#1077' '#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1074#1099#1073#1088#1072#1085#1085#1086#1075#1086' '#1086#1073#1098#1077#1082#1090#1072
        ParentShowHint = False
        ShowHint = True
        TabOrder = 17
      end
    end
    object ServiceSheet: TTabSheet
      Caption = 'ServiceSheet'
      ImageIndex = 1
      TabVisible = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Label19: TLabel
        Left = 11
        Top = 8
        Width = 154
        Height = 13
        Caption = #1055#1086#1083#1085#1086#1077' '#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1089#1083#1091#1078#1073#1099
      end
      object Label26: TLabel
        Left = 11
        Top = 56
        Width = 277
        Height = 13
        Caption = #1057#1086#1082#1088#1072#1097#1077#1085#1085#1086#1077' '#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1089#1083#1091#1078#1073#1099' '#1076#1083#1103' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1072
      end
      object Edit3: TEdit
        Left = 11
        Top = 27
        Width = 366
        Height = 21
        TabOrder = 0
      end
      object Edit9: TEdit
        Left = 11
        Top = 75
        Width = 366
        Height = 21
        TabOrder = 1
      end
    end
    object DepartSheet: TTabSheet
      Caption = 'DepartSheet'
      ImageIndex = 2
      TabVisible = False
      object Label14: TLabel
        Left = 11
        Top = 8
        Width = 113
        Height = 13
        Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1086#1090#1076#1077#1083#1072
      end
      object Label15: TLabel
        Left = 11
        Top = 54
        Width = 144
        Height = 13
        Caption = #1058#1080#1087' '#1074#1099#1096#1077#1089#1090#1086#1103#1097#1077#1075#1086' '#1086#1073#1098#1077#1082#1090#1072
      end
      object Label16: TLabel
        Left = 11
        Top = 100
        Width = 199
        Height = 13
        Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1074#1099#1096#1077#1089#1090#1086#1103#1097#1077#1075#1086' '#1086#1073#1098#1077#1082#1090#1072
      end
      object Label18: TLabel
        Left = 11
        Top = 146
        Width = 343
        Height = 13
        Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1074#1099#1096#1077#1089#1090#1086#1103#1097#1080#1081' '#1086#1073#1098#1077#1082#1090' ('#1056#1086#1076#1080#1090#1077#1083#1100#1089#1082#1080#1081' '#1086#1073#1098#1077#1082#1090'->'#1054#1073#1098#1077#1082#1090'):'
      end
      object Edit2: TEdit
        Left = 11
        Top = 27
        Width = 366
        Height = 21
        TabOrder = 0
      end
      object ComboBox3: TComboBox
        Left = 11
        Top = 73
        Width = 367
        Height = 21
        Style = csDropDownList
        ItemIndex = 0
        TabOrder = 1
        Text = #1057#1083#1091#1078#1073#1072
        OnChange = ComboBox3Change
        Items.Strings = (
          #1057#1083#1091#1078#1073#1072
          #1054#1090#1076#1077#1083)
      end
      object ListBox2: TListBox
        Left = 11
        Top = 165
        Width = 367
        Height = 135
        ItemHeight = 13
        TabOrder = 2
        OnClick = ListBox2Click
      end
      object Edit5: TEdit
        Left = 11
        Top = 119
        Width = 366
        Height = 21
        TabOrder = 3
        OnChange = Edit5Change
      end
      object Memo1: TMemo
        Left = 11
        Top = 306
        Width = 367
        Height = 34
        Hint = #1055#1086#1083#1085#1086#1077' '#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1074#1099#1073#1088#1072#1085#1085#1086#1075#1086' '#1086#1073#1098#1077#1082#1090#1072
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
      end
    end
    object SubDepatSheet: TTabSheet
      AlignWithMargins = True
      Caption = 'SubDepatSheet'
      ImageIndex = 3
      TabVisible = False
      object Label11: TLabel
        Left = 8
        Top = 6
        Width = 131
        Height = 13
        Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1086#1090#1076#1077#1083#1077#1085#1080#1103
      end
      object Label12: TLabel
        Left = 8
        Top = 52
        Width = 144
        Height = 13
        Caption = #1058#1080#1087' '#1074#1099#1096#1077#1089#1090#1086#1103#1097#1077#1075#1086' '#1086#1073#1098#1077#1082#1090#1072
      end
      object Label22: TLabel
        Left = 8
        Top = 147
        Width = 169
        Height = 13
        Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1074#1099#1096#1077#1089#1090#1086#1103#1097#1080#1081' '#1086#1073#1098#1077#1082#1090':'
      end
      object Label24: TLabel
        Left = 8
        Top = 101
        Width = 343
        Height = 13
        Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1074#1099#1096#1077#1089#1090#1086#1103#1097#1080#1081' '#1086#1073#1098#1077#1082#1090' ('#1056#1086#1076#1080#1090#1077#1083#1100#1089#1082#1080#1081' '#1086#1073#1098#1077#1082#1090'->'#1054#1073#1098#1077#1082#1090'):'
      end
      object Edit1: TEdit
        Left = 8
        Top = 24
        Width = 366
        Height = 21
        TabOrder = 0
      end
      object ComboBox1: TComboBox
        Left = 8
        Top = 71
        Width = 367
        Height = 21
        Style = csDropDownList
        ItemIndex = 0
        TabOrder = 1
        Text = #1057#1083#1091#1078#1073#1072
        OnChange = ComboBox1Change
        Items.Strings = (
          #1057#1083#1091#1078#1073#1072
          #1054#1090#1076#1077#1083)
      end
      object ListBox3: TListBox
        Left = 8
        Top = 166
        Width = 367
        Height = 135
        ItemHeight = 13
        TabOrder = 2
        OnClick = ListBox3Click
      end
      object Edit6: TEdit
        Left = 8
        Top = 120
        Width = 367
        Height = 21
        TabOrder = 3
        OnChange = Edit6Change
      end
      object Memo3: TMemo
        Left = 8
        Top = 307
        Width = 367
        Height = 34
        Hint = #1055#1086#1083#1085#1086#1077' '#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1074#1099#1073#1088#1072#1085#1085#1086#1075#1086' '#1086#1073#1098#1077#1082#1090#1072
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
      end
    end
  end
  object CmbObjectType: TComboBox
    Left = 8
    Top = 35
    Width = 217
    Height = 21
    Style = csDropDownList
    ItemIndex = 3
    TabOrder = 3
    Text = #1057#1086#1090#1088#1091#1076#1085#1080#1082
    OnChange = CmbObjectTypeChange
    Items.Strings = (
      #1057#1083#1091#1078#1073#1072
      #1054#1090#1076#1077#1083
      #1054#1090#1076#1077#1083#1077#1085#1080#1077
      #1057#1086#1090#1088#1091#1076#1085#1080#1082)
  end
end
