procedure TDataModule1.RefreshChildNodes(ParentNode: TTreeNode);
var
  i: Integer;
  pId: ^Integer;
begin
  try
    dm.DoQuery(dm.RTQuery, 'SELECT dep_name, id_dep FROM dep_list WHERE id_par='+IntToStr(Integer(ParentNode.Data^)));
    while not dm.RTQuery.Eof do
    begin
      New(pId);
      pId^ := dm.RTQuery.FieldByName('id_dep').AsInteger;
      Form1.ObjTree.Items.AddChildObject(ParentNode, dm.RTQuery.FieldByName('dep_name').AsString, pId);
      dm.RTQuery.Next;
    end;
  except
    on E: Exception do
      //nothing
  end;
  for i:= 0 to ParentNode.Count - 1 do
    RefreshChildNodes(ParentNode.Item[i]);
end;

procedure TDataModule1.RefreshNodes();
var
  rec_counter: Integer;
  pId: ^Integer;
begin
  try
    dm.DoQuery(dm.RTQuery, 'SELECT * FROM dep_list ORDER BY id_dep_group ');
      while not dm.RTQuery.Eof do
      begin
        if (dm.RTQuery.FieldByName('id_dep_group').AsString).Equals('������') then
        begin
          New(pId);
          pId^ := dm.RTQuery.FieldByName('id_dep').AsInteger;
          Form1.ObjTree.Items.AddObject(nil, dm.RTQuery.FieldByName('dep_name').AsString, pId);
        end;
        dm.RTQuery.Next;
      end;
  except
    on E: Exception do
      //nothing
  end;

  for rec_counter:=Form1.ObjTree.Items.Count-1 downto 0 do
  begin
    RefreshChildNodes(Form1.ObjTree.Items[rec_counter]);
  end;
end;