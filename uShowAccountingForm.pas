unit uShowAccountingForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Grids, Vcl.StdCtrls, Vcl.ComCtrls,
  Data.DB, Vcl.DBGrids, uStat;

type
  TAccForm = class(TForm)
    StringGrid1: TStringGrid;
    Label1: TLabel;
    DBGrid1: TDBGrid;
    Button1: TButton;
    Button2: TButton;
    procedure FormShow(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure GetDateRange;
    procedure VCLInit;
    procedure LoadData;
    procedure LoadDepStat;
    procedure LoadEmplStat;
  private
    DateRangeStr: String;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AccForm: TAccForm;

implementation

{$R *.dfm}

uses main, DataMod, uReportGenerator;

procedure TAccForm.Button1Click(Sender: TObject);
begin
  AccForm.Close;
end;

procedure TAccForm.FormResize(Sender: TObject);
begin
  StringGrid1.Width:= AccForm.Width;
  StringGrid1.ColWidths[1]:= AccForm.Width - 266;
  DBGrid1.Width:= AccForm.Width;
end;

procedure TAccForm.GetDateRange;
var
  Range: TDateRange;
begin
  DateRangeStr:= '';
  if Form1.CheckBox1.Checked then
  begin
    Range:= Form1.GetDateRange;
    DateRangeStr:= ' t1.item_date >= ' + QuotedStr(Range[0]);
    DateRangeStr:= DateRangeStr + ' and t1.item_date <= ' + QuotedStr(Range[1]) + ' and ';
  end;
end;

procedure TAccForm.VCLInit;
begin
  StringGrid1.ColWidths[0]:= 266;
  StringGrid1.ColWidths[1]:= 280;
  AccForm.Width:= 1160;
  case Form1.ComboBox1.ItemIndex of
  0:
    begin
      StringGrid1.RowCount:= 7;
      StringGrid1.Height:= 165;
      StringGrid1.Cells[0, 0]:= '������������ ������';//dep_name
      StringGrid1.Cells[0, 1]:= '���������� �����������';//from query
      StringGrid1.Cells[0, 2]:= '���������� �����������, ����������� �����';//second qiery
      StringGrid1.Cells[0, 3]:= '����� ��������';//costs
      StringGrid1.Cells[0, 4]:= '����� �����������';//second query
      StringGrid1.Cells[0, 5]:= '���������� �������, ����������� ������������';//third query
      StringGrid1.Cells[0, 6]:= '���������� �����, �������������� ������������';//total_amount
      Label1.Top:= 173;
      DBGrid1.Top:= 190;
      Button1.Top:= 475;
    end;
  1:
    begin
      StringGrid1.RowCount:= 8;
      StringGrid1.Height:= 189;
      StringGrid1.Cells[0, 0]:= '������������ ������';
      StringGrid1.Cells[0, 1]:= '����������� ������';
      StringGrid1.Cells[0, 2]:= '���������� �����������';
      StringGrid1.Cells[0, 3]:= '���������� �����������, ����������� �����';
      StringGrid1.Cells[0, 4]:= '����� ��������';
      StringGrid1.Cells[0, 5]:= '����� �����������';
      StringGrid1.Cells[0, 6]:= '���������� �������, ����������� ������������';
      StringGrid1.Cells[0, 7]:= '���������� �����, �������������� ������������';
      Label1.Top:= 197;
      DBGrid1.Top:= 214;
      Button1.Top:= 499;
    end;
  2:
    begin
      StringGrid1.RowCount:= 8;
      StringGrid1.Height:= 189;
      StringGrid1.Cells[0, 0]:= '������������ ���������';
      StringGrid1.Cells[0, 1]:= '����������� ������';
      StringGrid1.Cells[0, 2]:= '���������� �����������';
      StringGrid1.Cells[0, 3]:= '���������� �����������, ����������� �����';
      StringGrid1.Cells[0, 4]:= '����� ��������';
      StringGrid1.Cells[0, 5]:= '����� �����������';
      StringGrid1.Cells[0, 6]:= '���������� �������, ����������� ������������';
      StringGrid1.Cells[0, 7]:= '���������� �����, �������������� ������������';
      Label1.Top:= 197;
      DBGrid1.Top:= 214;
      Button1.Top:= 499;
    end;
  3:
    begin
      StringGrid1.RowCount:= 7;
      StringGrid1.Height:= 165;
      StringGrid1.Cells[0, 0]:= '���';
      StringGrid1.Cells[0, 1]:= '����������� ������';
      StringGrid1.Cells[0, 2]:= '���������';
      StringGrid1.Cells[0, 3]:= '����� ��������(���.)';
      StringGrid1.Cells[0, 4]:= '����� ��������(�����.)';
      StringGrid1.Cells[0, 5]:= '����� ��������(����)';
      StringGrid1.Cells[0, 6]:= '����������';
      Label1.Top:= 173;
      DBGrid1.Top:= 190;
      Button1.Top:= 475;
    end;
  end;
end;

procedure TAccForm.LoadEmplStat;
var
  SqlStr, IdEmpl: String;
  buf: TEmplInfo;
begin
  buf.Phones:= TStringList.Create;
  IdEmpl:= Form1.GridPhoneBook.DataSource.DataSet.FieldByName('id_empl').AsString;
//BEGIN: �������� ���������� � ����������
  SqlStr:= 'SELECT t2.dep_name, t1.empl_post, t1.empl_phone, t1.empl_int_phone, t1.empl_city_te_number,';
  SqlStr:= SqlStr + ' CONCAT_WS('+QuotedStr(' ')+', t1.empl_lname, t1.empl_fname, t1.empl_lfname)';
  SqlStr:= SqlStr + ' as `���` FROM empl_list t1 LEFT JOIN dep_list t2 on t1.id_dep=t2.id_dep';
  SqlStr:= SqlStr + ' WHERE t1.id_empl=';
  SqlStr:= SqlStr + Form1.GridPhoneBook.DataSource.DataSet.FieldByName('id_empl').AsString;
  dm.DoQuery(dm.RTQuery, SqlStr);

  StringGrid1.Cells[1, 0]:= dm.RTQuery.FieldByName('���').AsString;
  StringGrid1.Cells[1, 1]:= dm.RTQuery.FieldByName('dep_name').AsString;
  StringGrid1.Cells[1, 2]:= dm.RTQuery.FieldByName('empl_post').AsString;
  StringGrid1.Cells[1, 3]:= dm.RTQuery.FieldByName('empl_phone').AsString;
  StringGrid1.Cells[1, 4]:= dm.RTQuery.FieldByName('empl_int_phone').AsString;
  StringGrid1.Cells[1, 5]:= dm.RTQuery.FieldByName('empl_city_te_number').AsString;
//END: �������� ���������� � ����������

//BEGIN: ����������
  buf:= GetEmplInfo(IdEmpl, DateRangeStr);
  StringGrid1.Cells[1, 6]:= buf.OverCost;
//END: ����������

//BEGIN: ����������� ������� ����������
  SqlStr:= 'SELECT CONCAT_WS(' + QuotedStr(' -> ') + ', t4.dep_name, t3.dep_name) as `��������������`,';
  SqlStr:= SqlStr +  't2.empl_post as `���������`, CONCAT_WS(' + QuotedStr(' ');
  SqlStr:= SqlStr + ', t2.empl_lname, CONCAT(LEFT(t2.empl_fname,1),' + QuotedStr('.');
  SqlStr:= SqlStr +  '), CONCAT(LEFT(t2.empl_lfname,1),' + QuotedStr('.') + '))';
  SqlStr:= SqlStr + ' as `���`, CONCAT(t1.item_date, ' + QuotedStr(' ') + ', t1.item_time)';
  SqlStr:= SqlStr + ' as `����`, t1.item_amount as `������������`, t1.item_cost';
  SqlStr:= SqlStr + ' as `�����` FROM call_data t1 LEFT JOIN empl_list t2 on ';
  SqlStr:= SqlStr + 't1.id_empl=t2.id_empl LEFT JOIN dep_list t3 on t2.id_dep=t3.id_dep ';
  SqlStr:= SqlStr + 'LEFT JOIN dep_list t4 on t4.id_dep=t3.id_par ';
  SqlStr:= SqlStr + 'WHERE ' + DateRangeStr + ' t2.id_empl=';
  SqlStr:= SqlStr + Form1.GridPhoneBook.DataSource.DataSet.FieldByName('id_empl').AsString;
  dm.DoQuery(dm.AccGridQuery, SqlStr);
  dm.RefreshQuery(dm.AccGridQuery, 'detailization');
//END: ����������� ������� �����������
end;

procedure TAccForm.LoadDepStat;
var
  SqlStr, CostLimit, IdDep, DepName: String;
  Row: Integer;
  buf: TDepInfo;
begin
  CostLimit:= IntToStr(main.Settings.CostLimit);
  IdDep:= Form1.AccGridDeps.DataSource.DataSet.FieldByName('id_dep').AsString;
  Buf:= GetDepInfo(StrToInt(IdDep), DateRangeStr, 'WithSubDeps');
  Row:= 0;
    
  StringGrid1.Cells[1, Row]:= buf.DepName;
  if not GetObjType(StrToInt(IdDep)).Equals('������') then
  begin
    Row:= 1;
    StringGrid1.Cells[1, 1]:= buf.UpperName;
  end;  
  StringGrid1.Cells[1, 1 + Row]:= buf.EmplCnt;
  StringGrid1.Cells[1, 2 + Row]:= buf.OvercostEmplCnt;
  StringGrid1.Cells[1, 3 + Row]:= buf.Costs;
  StringGrid1.Cells[1, 4 + Row]:= buf.SumOvercost;
  StringGrid1.Cells[1, 5 + Row]:= buf.CallCnt;
  StringGrid1.Cells[1, 6 + Row]:= buf.MinutesCnt;

  SqlStr:= 'SELECT CONCAT_WS(' + QuotedStr(' -> ') + ', t4.dep_name, t3.dep_name) as `��������������`,';
  SqlStr:= SqlStr + 't2.empl_post as `���������`, CONCAT_WS(' + QuotedStr(' ');
  SqlStr:= SqlStr + ', t2.empl_lname, CONCAT(LEFT(t2.empl_fname,1),' + QuotedStr('.');
  SqlStr:= SqlStr +  '), CONCAT(LEFT(t2.empl_lfname,1),' + QuotedStr('.') + '))';
  SqlStr:= SqlStr + ' as `���`, CONCAT(t1.item_date, ' + QuotedStr(' ') + ', t1.item_time)';
  SqlStr:= SqlStr + ' as `����`, t1.item_amount as `������������`, t1.item_cost';
  SqlStr:= SqlStr + ' as `�����` FROM call_data t1 LEFT JOIN empl_list t2 on ';
  SqlStr:= SqlStr + 't1.id_empl=t2.id_empl LEFT JOIN dep_list t3 on t2.id_dep=t3.id_dep ';
  SqlStr:= SqlStr + 'LEFT JOIN dep_list t4 on t4.id_dep=t3.id_par ';
  SqlStr:= SqlStr + 'WHERE ' + DateRangeStr;
  SqlStr:= SqlStr + ' t2.id_dep in (' + buf.SubIdString + ')';
  dm.DoQuery(dm.AccGridQuery, SqlStr);
  dm.RefreshQuery(dm.AccGridQuery, 'detailization');  
end;

procedure TAccForm.LoadData;
var
  SqlStr, IdString, DateRangeStr, CostLimit: String;
begin
  case Form1.ComboBox1.ItemIndex of
    0: LoadDepStat;
    1: LoadDepStat;
    2: LoadDepStat;
    3: LoadEmplStat;
  end;
  AccForm.Height:= Button1.Top + 76;
end;

procedure TAccForm.FormShow(Sender: TObject);
begin
  VCLInit;
  GetDateRange;
  LoadData;
end;

end.
