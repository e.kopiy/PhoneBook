program PhoneBook;

uses
  Vcl.Forms,
  main in 'main.pas' {Form1},
  addObjectForm in 'addObjectForm.pas' {Form3},
  settingsForm in 'settingsForm.pas' {Form8},
  confirmForm in 'confirmForm.pas' {Form2},
  DataMod in 'DataMod.pas' {DataModule1: TDataModule},
  showObjectForm in 'showObjectForm.pas' {Form4},
  LibXL in 'LibXL\LibXL.pas',
  uPBGenerator in 'uPBGenerator.pas',
  ImportForm in 'ImportForm.pas' {FormImport},
  AccountingImport in 'AccountingImport.pas',
  uRegHashUtils in 'uRegHashUtils.pas',
  uPrintForm in 'uPrintForm.pas' {PrintForm},
  uReportGenerator in 'uReportGenerator.pas',
  uShowAccountingForm in 'uShowAccountingForm.pas' {AccForm},
  uStat in 'uStat.pas',
  uRestoreForm in 'uRestoreForm.pas' {RestoreForm};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm3, Form3);
  Application.CreateForm(TForm8, Form8);
  Application.CreateForm(TForm2, Form2);
  Application.CreateForm(TDM, DM);
  Application.CreateForm(TForm4, Form4);
  Application.CreateForm(TFormImport, FormImport);
  Application.CreateForm(TPrintForm, PrintForm);
  Application.CreateForm(TAccForm, AccForm);
  Application.CreateForm(TRestoreForm, RestoreForm);
  Application.Run;
end.
