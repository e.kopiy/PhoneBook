unit uStat;

interface

uses
  System.Classes, SysUtils, LibXL, ShellAPI, Winapi.Windows,
  Vcl.ComCtrls, Vcl.Dialogs, Vcl.Forms;

type
  TOvercostCnt = record
    EmplCnt: String;
    DepCnt: String;
    OverCost: String;
  end;

type
  TEmplInfo = record
    LastFirstMidName: String;
    Post: String;
    Phones: TStringList;
    MinutesCnt: String;
    CallCnt: String;
    OverCost: String;
  end;

type
  TSummaryStat = record
    CallCnt: String;
    MinutesCnt: String;
    EmplCnt: String;
    Costs: String;
    SubIdString: String;
  end;

type
  TDepInfo = record
    DepName: String;
    UpperName: String;
    EmplCnt: String;
    Costs: String;
    OvercostEmplCnt: String;
    CallCnt: String;
    MinutesCnt: String;
    SumOvercost: String;
    SubIdString: String;
  end;

function GetDepInfo(IdDep: Integer; InpDateRangeStr: String; CalcType: String): TDepInfo;
function GetOvercosted(IdDep: String; InpDateRangeStr: String; CalcType: String): TOvercostCnt;
function GetSummaryStat(IdDep: String; InpDateRangeStr: String; CalcType: String): TSummaryStat;
function GetEmplInfo(IdEmpl: String; InpDateRangeStr: String): TEmplInfo;
function GetObjType(IdDep: Integer): String;

implementation

uses main, DataMod;

function GetObjType(IdDep: Integer): String;
var
  SqlStr: String;
begin
  Result:= '';
  SqlStr:= 'SELECT id_dep_group FROM dep_list WHERE id_dep=' + IntToStr(IdDep);
  dm.DoQuery(dm.RTQuery, SqlStr);
  Result:= dm.RTQuery.FieldByName('id_dep_group').AsString;
end;

function GetSummaryStat(IdDep: String; InpDateRangeStr: String; CalcType: String): TSummaryStat;
var
  MinCnt, CallCnt, EmplCnt, Costs: integer;
  SqlStr, CurrIdDep: String;
begin
if iddep.Equals('302') then
  CallCnt:= 0;

  CallCnt:= 0;
  MinCnt:= 0;
  EmplCnt:= 0;
  Costs:= 0;
  Result.SubIdString:= '';
  if CalcType.Equals('WithSubDeps') then
  begin
    SqlStr:= 'SET @a = ' + IdDep;
    dm.DoEditQuery(dm.RTQuery1, SqlStr);
    SqlStr:= 'SELECT id_dep FROM dep_list WHERE id_dep in (SELECT id FROM (';
    SqlStr:= SqlStr + 'SELECT @index:=INSTR(@a,' + QuotedStr(',') + ') as coma_index,';
    SqlStr:= SqlStr + ' @id:=SUBSTRING_INDEX(@a, ' + QuotedStr(',') + ', 1)  as id,';
    SqlStr:= SqlStr + ' @b:=SUBSTRING(@a,IF(@index,@index+1,0)),';
    SqlStr:= SqlStr + ' @a:=CONCAT_WS(IF(LENGTH(@b)>0,' + QuotedStr(',') + ',' + QuotedStr('') + '),';
    SqlStr:= SqlStr + ' @b,(SELECT group_concat(id_dep) FROM dep_list WHERE id_par=@id)) as array';
    SqlStr:= SqlStr + ' FROM dep_list WHERE LENGTH(@a)>0) as tt1)';
    dm.DoQuery(dm.RTQuery, SqlStr);
    while not dm.RTQuery.Eof do
    begin
      CurrIdDep:= dm.RTQuery.FieldByName('id_dep').AsString;
      Result.SubIdString:= Result.SubIdString + ',' + CurrIdDep;
      SqlStr:= 'SELECT SUM(t1.costs) as `costs` FROM grouped_costs_date_id t1 ';
      SqlStr:= SqlStr + 'LEFT JOIN empl_list t2 on t1.id_empl=t2.id_empl ';
      SqlStr:= SqlStr + 'LEFT JOIN dep_list t3 on t2.id_dep=t3.id_dep ';
      SqlStr:= SqlStr + 'WHERE ' + InpDateRangeStr + ' t2.id_dep =' + CurrIdDep;
      if dm.DoQuery(dm.RTQuery1, SqlStr) then
        inc(costs, dm.RTQuery1.FieldByName('costs').AsInteger);

      SqlStr:= 'SELECT COUNT(id_empl) as `empl_cnt` FROM empl_list WHERE id_dep = ' + CurrIdDep;
      if dm.DoQuery(dm.RTQuery1, SqlStr) then
        inc(EmplCnt, dm.RTQuery1.FieldByName('empl_cnt').AsInteger);

      SqlStr:= 'SELECT SUM(t1.item_amount) as `minutes`, COUNT(t1.id_item) as `calls`';
      SqlStr:= SqlStr + ' FROM call_data t1 LEFT JOIN empl_list t2 on t1.id_empl=t2.id_empl';
      SqlStr:= SqlStr + ' WHERE ' + InpDateRangeStr + ' t2.id_dep=' + CurrIdDep;
      if dm.DoQuery(dm.RTQuery1, SqlStr) then
      begin
        inc(CallCnt, dm.RTQuery1.FieldByName('calls').AsInteger);
        inc(MinCnt, dm.RTQuery1.FieldByName('minutes').AsInteger);
      end;
      dm.RTQuery.Next;
    end;
    Delete(Result.SubIdString, 1, 1);
  end;
  if CalcType.Equals('WithoutSubDeps') then
  begin
      SqlStr:= 'SELECT SUM(t1.costs) as `costs` FROM grouped_costs_date_id t1 ';
      SqlStr:= SqlStr + 'LEFT JOIN empl_list t2 on t1.id_empl=t2.id_empl ';
      SqlStr:= SqlStr + 'LEFT JOIN dep_list t3 on t2.id_dep=t3.id_dep ';
      SqlStr:= SqlStr + 'WHERE ' + InpDateRangeStr + ' t2.id_dep =' + IdDep;
      if dm.DoQuery(dm.RTQuery1, SqlStr) then
        inc(costs, dm.RTQuery1.FieldByName('costs').AsInteger);

      SqlStr:= 'SELECT COUNT(id_empl) as `empl_cnt` FROM empl_list WHERE id_dep = ' + IdDep;
      if dm.DoQuery(dm.RTQuery1, SqlStr) then
        inc(EmplCnt, dm.RTQuery1.FieldByName('empl_cnt').AsInteger);

      SqlStr:= 'SELECT SUM(t1.item_amount) as `minutes`, COUNT(t1.id_item) as `calls`';
      SqlStr:= SqlStr + ' FROM call_data t1 LEFT JOIN empl_list t2 on t1.id_empl=t2.id_empl';
      SqlStr:= SqlStr + ' WHERE ' + InpDateRangeStr + ' t2.id_dep=' + IdDep;
      if dm.DoQuery(dm.RTQuery1, SqlStr) then
      begin
        inc(CallCnt, dm.RTQuery1.FieldByName('calls').AsInteger);
        inc(MinCnt, dm.RTQuery1.FieldByName('minutes').AsInteger);
      end;
  end;
  if CallCnt=0 then
    Result.CallCnt:= '0'
  else
    Result.CallCnt:= IntToStr(CallCnt);
  if Costs=0 then
    Result.Costs:= '0'
  else
    Result.Costs:= IntToStr(Costs);
  if MinCnt=0 then
    Result.MinutesCnt:= '0'
  else
    Result.MinutesCnt:= IntToStr(MinCnt);
  if EmplCnt=0 then
    Result.EmplCnt:= '0'
  else
    Result.EmplCnt:= IntToStr(EmplCnt);
end;

function GetOvercosted(IdDep: String; InpDateRangeStr: String; CalcType: String): TOvercostCnt;
var
  SqlStr, CurrIdDep: String;
  OverCostEmplCnt, OverCostDepCnt, OverCost: Integer;
begin
  OverCostDepCnt:= 0;
  OverCostEmplCnt:= 0;
  OverCost:= 0;
  if CalcType.Equals('WithSubDeps') then
  begin
    SqlStr:= 'SET @a = ' + IdDep;
    dm.DoEditQuery(dm.RTQuery1, SqlStr);
    SqlStr:= 'SELECT id_dep FROM dep_list WHERE id_dep in (SELECT id FROM (';
    SqlStr:= SqlStr + 'SELECT @index:=INSTR(@a,' + QuotedStr(',') + ') as coma_index,';
    SqlStr:= SqlStr + ' @id:=SUBSTRING_INDEX(@a, ' + QuotedStr(',') + ', 1)  as id,';
    SqlStr:= SqlStr + ' @b:=SUBSTRING(@a,IF(@index,@index+1,0)),';
    SqlStr:= SqlStr + ' @a:=CONCAT_WS(IF(LENGTH(@b)>0,' + QuotedStr(',') + ',' + QuotedStr('') + '),';
    SqlStr:= SqlStr + ' @b,(SELECT group_concat(id_dep) FROM dep_list WHERE id_par=@id)) as array';
    SqlStr:= SqlStr + ' FROM dep_list WHERE LENGTH(@a)>0) as tt1)';
    dm.DoQuery(dm.RTQuery, SqlStr);
    while not dm.RTQuery.Eof do
    begin
      CurrIdDep:= dm.RTQuery.FieldByName('id_dep').AsString;
      SqlStr:= 'SELECT sum(t3.total_costs - ' + IntToStr(main.Settings.CostLimit)+')';
      SqlStr:= SqlStr + ' as `overcost`, COUNT(t3.id_empl) as `empl_cnt`';
      SqlStr:= SqlStr + ' FROM (SELECT t1.id_empl as `id_empl`, SUM(t1.costs) as `total_costs`';
      SqlStr:= SqlStr + ' FROM grouped_costs_date_id t1 LEFT JOIN empl_list t2';
      SqlStr:= SqlStr + ' on t1.id_empl=t2.id_empl WHERE ' + InpDateRangeStr;
      SqlStr:= SqlStr + ' t2.id_dep=' + CurrIdDep + ' GROUP BY t1.id_empl) t3 ';
      SqlStr:= SqlStr + 'WHERE t3.total_costs > ' + IntToStr(main.Settings.CostLimit);
      if dm.DoQuery(dm.RTQuery1, SqlStr) then
      begin
        if not dm.RTQuery1.FieldByName('overcost').AsString.IsEmpty then
        begin
          inc(OverCost, dm.RTQuery1.FieldByName('overcost').AsInteger);
          inc(OverCostDepCnt);
          inc(OverCostEmplCnt, dm.RTQuery1.FieldByName('empl_cnt').AsInteger);
        end;
      end;
      dm.RTQuery.Next;
    end;
  end;
  if CalcType.Equals('WithoutSubDeps') then
  begin
    SqlStr:= 'SELECT sum(t3.total_costs - ' + IntToStr(main.Settings.CostLimit) + ')';
    SqlStr:= SqlStr + ' as `overcost`, COUNT(t3.id_empl) as `empl_cnt`';
    SqlStr:= SqlStr + ' FROM (SELECT t1.id_empl as `id_empl`, SUM(t1.costs) as `total_costs`';
    SqlStr:= SqlStr + ' FROM grouped_costs_date_id t1 LEFT JOIN empl_list t2';
    SqlStr:= SqlStr + ' on t1.id_empl=t2.id_empl WHERE ' + InpDateRangeStr;
    SqlStr:= SqlStr + ' t2.id_dep=' + IdDep + ' GROUP BY t1.id_empl) t3 ';
    SqlStr:= SqlStr + 'WHERE t3.total_costs > ' + IntToStr(main.Settings.CostLimit);
    if dm.DoQuery(dm.RTQuery1, SqlStr) then
    begin
      if not dm.RTQuery1.FieldByName('overcost').AsString.IsEmpty then
      begin
        OverCost:= dm.RTQuery1.FieldByName('overcost').AsInteger;
        OverCostEmplCnt:= dm.RTQuery1.FieldByName('empl_cnt').AsInteger;
      end;
    end;
  end;

  if OverCostEmplCnt=0 then
    Result.EmplCnt:= '0'
  else
    Result.EmplCnt:= IntToStr(OverCostEmplCnt);

  if OverCostDepCnt=0 then
    Result.DepCnt:= '0'
  else
    Result.DepCnt:= IntToStr(OverCostDepCnt);

  if OverCost=0 then
    Result.OverCost:= '0'
  else
    Result.OverCost:= IntToStr(OverCost);
end;

function GetDepInfo(IdDep: Integer; InpDateRangeStr: String; CalcType: String): TDepInfo;
var
  SqlStr: String;
  tmp1: TSummaryStat;
  tmp2: TOvercostCnt;
begin
  Result.DepName:= '';
  Result.OvercostEmplCnt:= '';
  Result.CallCnt:= '';
  Result.MinutesCnt:= '';
  Result.SumOvercost:= '';
  Result.EmplCnt:= '';
  Result.Costs:= '';
  Result.UpperName:= '';
  Result.SubIdString:= '';

  SqlStr:= 'SELECT CONCAT_WS('+QuotedStr(', ')+',t2.id_dep_group,t2.dep_name) as `upper` FROM dep_list t1 LEFT JOIN dep_list t2 on';
  SqlStr:= SqlStr + ' t1.id_par=t2.id_dep WHERE t1.id_dep=' + IntToStr(IdDep);
  dm.DoQuery(dm.RTQuery, SqlStr);
    Result.UpperName:= dm.RTQuery.FieldByName('upper').AsString;

  tmp1:= GetSummaryStat(IntToStr(IdDep), InpDateRangeStr, CalcType);
  tmp2:= GetOvercosted(IntToStr(IdDep), InpDateRangeStr, CalcType);

  SqlStr:= 'SELECT dep_name FROM dep_list WHERE id_dep=' + IntToStr(IdDep);
  dm.DoQuery(dm.RTQuery, SqlStr);

  Result.Costs:= tmp1.Costs;
  Result.EmplCnt:= tmp1.EmplCnt;
  Result.SubIdString:= tmp1.SubIdString;
  Result.DepName:= dm.RTQuery.FieldByName('dep_name').AsString;
  Result.OvercostEmplCnt:= tmp2.EmplCnt;
  Result.SumOvercost:= tmp2.OverCost;
  Result.CallCnt:= tmp1.CallCnt;
  Result.MinutesCnt:= tmp1.MinutesCnt;
end;

function GetEmplInfo(IdEmpl: String; InpDateRangeStr: String): TEmplInfo;
var
  SqlStr: String;
begin
  SqlStr:= 'SELECT t2.empl_post, t2.empl_phone, t2.empl_int_phone, t2.empl_city_te_number,';
  SqlStr:= SqlStr + ' t2.empl_lname, LEFT(t2.empl_fname, 1) as `empl_fname`, LEFT(t2.empl_lfname,1) as `empl_lfname`';
  SqlStr:= SqlStr + ' FROM empl_list t2 LEFT JOIN grouped_costs_date_id t1 on t2.id_empl=t1.id_empl';
  SqlStr:= SqlStr + ' WHERE ' + InpDateRangeStr + ' t1.id_empl=' + IdEmpl;
  if dm.DoQuery(dm.RTQuery, SqlStr) then
  begin
    if dm.RTQuery.RecordCount > 0 then
    begin
      Result.Post:= dm.RTQuery.FieldByName('empl_post').AsString;

      Result.Phones.Clear;
      Result.Phones.Add(dm.RTQuery.FieldByName('empl_phone').AsString);
      Result.Phones.Add(dm.RTQuery.FieldByName('empl_int_phone').AsString);
      Result.Phones.Add(dm.RTQuery.FieldByName('empl_city_te_number').AsString);

      if not dm.RTQuery.FieldByName('empl_lname').AsString.IsEmpty then
        Result.LastFirstMidName:= dm.RTQuery.FieldByName('empl_lname').AsString + ' ';
      if not dm.RTQuery.FieldByName('empl_fname').AsString.IsEmpty then
        Result.LastFirstMidName:= Result.LastFirstMidName + dm.RTQuery.FieldByName('empl_fname').AsString + '. ';
      if not dm.RTQuery.FieldByName('empl_lfname').AsString.IsEmpty then
        Result.LastFirstMidName:= Result.LastFirstMidName + dm.RTQuery.FieldByName('empl_lfname').AsString + '.';

      SqlStr:= 'SELECT SUM(t1.item_amount) as `Minutes`, COUNT(t1.id_empl) as `CallCnt` FROM call_data t1';
      SqlStr:= SqlStr + ' WHERE ' + InpDateRangeStr + ' t1.id_empl=' + IdEmpl;
      dm.DoQuery(dm.RTQuery, SqlStr);
        if dm.RTQuery.FieldByName('Minutes').AsString.IsEmpty then
          Result.MinutesCnt:= '0'
        else
          Result.MinutesCnt:= dm.RTQuery.FieldByName('Minutes').AsString;
        if dm.RTQuery.FieldByName('CallCnt').AsString.IsEmpty then
          Result.CallCnt:= '0'
        else
          Result.CallCnt:= dm.RTQuery.FieldByName('CallCnt').AsString;

      SqlStr:= 'SELECT sum(t3.total_costs - ' + IntToStr(main.Settings.CostLimit)+')';
      SqlStr:= SqlStr + ' as `overcost`, COUNT(t3.id_empl) as `empl_cnt`';
      SqlStr:= SqlStr + ' FROM (SELECT t1.id_empl as `id_empl`, SUM(t1.costs) as `total_costs`';
      SqlStr:= SqlStr + ' FROM grouped_costs_date_id t1';
      SqlStr:= SqlStr + ' WHERE ' + InpDateRangeStr;
      SqlStr:= SqlStr + ' t1.id_empl=' + IdEmpl + ') t3 ';
      SqlStr:= SqlStr + 'WHERE t3.total_costs > ' + IntToStr(main.Settings.CostLimit);
      dm.DoQuery(dm.RTQuery, SqlStr);
        if dm.RTQuery.FieldByName('overcost').AsString.IsEmpty then
          Result.OverCost:= '0'
        else
          Result.OverCost:= dm.RTQuery.FieldByName('overcost').AsString;
    end
    else
      Result.Post:= 'empty';
  end
  else
    Result.Post:= 'empty';
end;

end.
