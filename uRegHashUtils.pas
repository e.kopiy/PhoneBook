unit uRegHashUtils;

interface

uses System.Classes, System.Hash, System.SysUtils, Registry, Winapi.Windows, main;

  function GetHash(FileName: String): String;
  procedure WriteSettings(InpSettings: TSettings);
  procedure ReadSettings(var InpSettings: TSettings; var Result: integer);

const
  SETTINGS_KEY= '\Software\PhoneBook';

implementation

function GetHash(FileName: String): String;
var
  HashMD5: THashMD5;
  Stream: TStream;
  Readed: Integer;
  Buffer: PByte;
  BufLen: Integer;
begin
  HashMD5:= THashMD5.Create;

  BufLen:= 64*1024;
  Buffer:= AllocMem(BufLen);
  try
     Stream:= TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
     try
        while Stream.Position < Stream.Size do
        begin
           Readed:= Stream.Read(Buffer^, BufLen);
           if Readed> 0 then
           begin
              HashMD5.Update(Buffer^, Readed);
           end;
        end;
     finally
        Stream.Free;
     end;
  finally
     FreeMem(Buffer)
  end;
  Result:= HashMD5.HashAsString;
end;

procedure WriteSettings(InpSettings: TSettings);
var
  Reg: TRegistry;
begin
  Reg:= TRegistry.Create;
  try
    Reg.RootKey := HKEY_CURRENT_USER;
    if Reg.OpenKey(SETTINGS_KEY, True) then
    begin
      Reg.WriteInteger('ColPhoneCode', InpSettings.ColPhoneCode);
      Reg.WriteInteger('ColPhoneId', InpSettings.ColPhoneId);
      Reg.WriteInteger('ColAmount', InpSettings.ColAmount);
      Reg.WriteInteger('ColDateTime', InpSettings.ColDateTime);
      Reg.WriteInteger('ColCost', InpSettings.ColCost);
      Reg.WriteInteger('CostLimit', InpSettings.CostLimit);
      Reg.CloseKey;
    end;
  finally
    Reg.Free;
  end;
end;

procedure ReadSettings(var InpSettings: TSettings; var Result: integer);
var
  Reg: TRegistry;
begin
  Result:= 0;
  Reg:= TRegistry.Create;
  try
    Reg.RootKey:= HKEY_CURRENT_USER;
    if Reg.OpenKeyReadOnly(SETTINGS_KEY) then
    begin
      if Reg.ValueExists('ColPhoneCode') then
      begin
        InpSettings.ColPhoneCode:= Reg.ReadInteger('ColPhoneCode');
        inc(Result);
      end;
      if Reg.ValueExists('ColPhoneId') then
      begin
        InpSettings.ColPhoneId:= Reg.ReadInteger('ColPhoneId');
        inc(Result);
      end;
      if Reg.ValueExists('ColAmount') then
      begin
        InpSettings.ColAmount:= Reg.ReadInteger('ColAmount');
        inc(Result);
      end;
      if Reg.ValueExists('ColDateTime') then
      begin
        InpSettings.ColDateTime:= Reg.ReadInteger('ColDateTime');
        inc(Result);
      end;
      if Reg.ValueExists('ColCost') then
      begin
        InpSettings.ColCost:= Reg.ReadInteger('ColCost');
        inc(Result);
      end;
      if Reg.ValueExists('CostLimit') then
      begin
        InpSettings.CostLimit:= Reg.ReadInteger('CostLimit');
        inc(Result);
      end;
      Reg.CloseKey;
    end;
  finally
    Reg.Free;
  end;
end;

end.
