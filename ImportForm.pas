unit ImportForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.ComCtrls, AccountingImport, uRegHashUtils;

type
  TFormImport = class(TForm)
    ListBox1: TListBox;
    Label1: TLabel;
    Button1: TButton;
    Button2: TButton;
    ProgressBar1: TProgressBar;
    Memo1: TMemo;
    Label2: TLabel;
    Label3: TLabel;
    Memo2: TMemo;
    Label5: TLabel;
    Label4: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ShowListToImport(Dir:string);
    procedure Button2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    CustCanClose: Boolean;
    AccImportThread: TAccountingImport;
  end;

var
  FormImport: TFormImport;

implementation

{$R *.dfm}

uses DataMod;

function IsFileImported(FileName: String): boolean;
var
  Hash, SqlStr: String;
begin
  Result:= False;
  Hash:= uRegHashUtils.GetHash(FileName);
  SqlStr:= 'SELECT * FROM imported_hash WHERE hash=' + QuotedStr(Hash);
  if dm.DoQuery(dm.RTQuery, SqlStr) then
    Result:= True;
end;

function AddImportedHash(FileName: String): boolean;
var
  Hash, SqlStr: String;
begin
  Hash:= uRegHashUtils.GetHash(FileName);
  SqlStr:='INSERT INTO imported_hash (hash)';
  SqlStr:= SqlStr + ' VALUES (' + QuotedStr(Hash) + ')';
  dm.DoEditQuery(dm.RTQuery, SqlStr);
end;

procedure TFormImport.Button1Click(Sender: TObject);
begin
  if ListBox1.ItemIndex < 0 then
    ShowMessage('����������, �������� ���� ��� �������')
  else
  begin
    Memo1.Lines.Clear;
    Memo2.Lines.Clear;
    AccImportThread:= TAccountingImport.Create(True);
    AccImportThread.Priority:= tpNormal;
    AccImportThread.FreeOnTerminate:= True;
    AccImportThread.FilePath:= ExtractFilePath(Application.ExeName) + 'import\';
    AccImportThread.FilePath:= AccImportThread.FilePath + ListBox1.Items[ListBox1.ItemIndex];
    if IsFileImported(AccImportThread.FilePath) then
    begin
      ShowMessage('�� ��� ������������� ���� ����. ��������� ������ ����������');
      Exit;
    end
    else
    begin
      AccImportThread.Start;
      Button1.Enabled:= False;
      Button2.Enabled:= False;
      CustCanClose:= False;
    end;
  end;
  AddImportedHash(AccImportThread.FilePath);
end;

procedure TFormImport.ShowListToImport(Dir:string);
var
  SearchRec: TSearchRec;
begin
 if FindFirst(Dir + '*.xls', faAnyFile, SearchRec)= 0 then
  begin
    repeat
      ListBox1.Items.Add(SearchRec.Name);
    until FindNext(SearchRec)<>0;
    FindClose(SearchRec);
  end;
end;

procedure TFormImport.Button2Click(Sender: TObject);
begin
  ListBox1.Items.Clear;
  FormImport.Close;
end;

procedure TFormImport.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Memo1.Lines.Clear;
  Memo2.Lines.Clear;
  FormImport.Width:=298; // Set size to default
end;

procedure TFormImport.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose:= CustCanClose;
end;

procedure TFormImport.FormShow(Sender: TObject);
begin
  CustCanClose:= True;
  FormImport.Width:=298;
  ListBox1.Items.Clear;
  ShowListToImport(ExtractFilePath(Application.ExeName) + '\import\');
end;

end.
