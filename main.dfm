object Form1: TForm1
  Left = 0
  Top = 0
  Caption = #1059#1095#1077#1090' '#1088#1072#1089#1093#1086#1076#1086#1074
  ClientHeight = 620
  ClientWidth = 1227
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poMainFormCenter
  ShowHint = True
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 872
    Top = 208
    Width = 31
    Height = 13
    Caption = 'Label2'
  end
  object PageControl1: TPageControl
    Left = -4
    Top = 0
    Width = 1234
    Height = 22
    ActivePage = TabSheet1
    TabOrder = 0
    OnChange = PageControl1Change
    object TabSheet1: TTabSheet
      Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082
    end
    object TabSheet2: TTabSheet
      Caption = #1059#1095#1077#1090' '#1088#1072#1089#1093#1086#1076#1086#1074
      ImageIndex = 1
    end
  end
  object ListToDel: TListBox
    Left = 504
    Top = 2
    Width = 40
    Height = 16
    ItemHeight = 13
    TabOrder = 1
    Visible = False
  end
  object ListBox1: TListBox
    Left = 368
    Top = 2
    Width = 39
    Height = 16
    ItemHeight = 13
    TabOrder = 2
    Visible = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 21
    Width = 1228
    Height = 595
    Color = clWindow
    ParentBackground = False
    TabOrder = 3
    object DepTree: TTreeView
      Left = 3
      Top = 9
      Width = 190
      Height = 585
      HideSelection = False
      Indent = 19
      ReadOnly = True
      SortType = stBoth
      TabOrder = 0
      OnChange = DepTreeChange
    end
    object GroupBox1: TGroupBox
      Left = 199
      Top = 3
      Width = 853
      Height = 153
      Caption = #1055#1086#1080#1089#1082
      TabOrder = 1
      object Label1: TLabel
        Left = 135
        Top = 58
        Width = 44
        Height = 13
        Caption = #1060#1072#1084#1080#1083#1080#1103
      end
      object Label4: TLabel
        Left = 259
        Top = 58
        Width = 19
        Height = 13
        Caption = #1048#1084#1103
      end
      object Label5: TLabel
        Left = 383
        Top = 58
        Width = 49
        Height = 13
        Caption = #1054#1090#1095#1077#1089#1090#1074#1086
      end
      object Label6: TLabel
        Left = 507
        Top = 58
        Width = 84
        Height = 13
        Caption = #1053#1086#1084#1077#1088' '#1090#1077#1083#1077#1092#1086#1085#1072
      end
      object Label7: TLabel
        Left = 623
        Top = 58
        Width = 25
        Height = 13
        Caption = #1060#1072#1082#1089
      end
      object Label8: TLabel
        Left = 736
        Top = 58
        Width = 93
        Height = 13
        Caption = #1042#1085#1091#1090#1088#1077#1085#1085#1080#1081' '#1085#1086#1084#1077#1088
      end
      object Label9: TLabel
        Left = 135
        Top = 12
        Width = 112
        Height = 13
        Caption = #1042#1099#1096#1077#1089#1090#1086#1103#1097#1080#1081' '#1086#1073#1098#1077#1082#1090
      end
      object Label10: TLabel
        Left = 383
        Top = 12
        Width = 31
        Height = 13
        Caption = #1043#1086#1088#1086#1076
      end
      object Label3: TLabel
        Left = 135
        Top = 58
        Width = 73
        Height = 13
        Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
      end
      object Label11: TLabel
        Left = 623
        Top = 12
        Width = 57
        Height = 13
        Caption = #1044#1086#1083#1078#1085#1086#1089#1090#1100
      end
      object Label12: TLabel
        Left = 736
        Top = 12
        Width = 85
        Height = 13
        Caption = #1053#1086#1084#1077#1088' '#1090#1077#1083'. '#1043#1040#1058#1057
      end
      object Label13: TLabel
        Left = 623
        Top = 104
        Width = 83
        Height = 13
        Caption = #1053#1072#1095#1072#1083#1100#1085#1072#1103' '#1076#1072#1090#1072
      end
      object Label14: TLabel
        Left = 736
        Top = 104
        Width = 110
        Height = 13
        Caption = #1050#1086#1085#1077#1095#1085#1072#1103' '#1076#1072#1090#1072' ('#1074#1082#1083'.)'
      end
      object Edit7: TEdit
        Left = 507
        Top = 77
        Width = 110
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnChange = Edit7Change
      end
      object Edit9: TEdit
        Left = 135
        Top = 31
        Width = 242
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnChange = Edit9Change
        OnClick = Edit9Click
        OnKeyPress = Edit9KeyPress
      end
      object Button4: TButton
        Left = 6
        Top = 72
        Width = 112
        Height = 26
        Caption = #1057#1090#1077#1088#1077#1090#1100' '#1074#1089#1077
        TabOrder = 2
        OnClick = Button4Click
      end
      object Edit6: TEdit
        Left = 135
        Top = 77
        Width = 118
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        OnChange = Edit6Change
      end
      object Edit12: TEdit
        Left = 259
        Top = 77
        Width = 118
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        OnChange = Edit12Change
      end
      object Edit13: TEdit
        Left = 383
        Top = 77
        Width = 118
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        OnChange = Edit13Change
      end
      object Edit10: TEdit
        Left = 623
        Top = 77
        Width = 107
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
        OnChange = Edit10Change
      end
      object Edit14: TEdit
        Left = 736
        Top = 77
        Width = 110
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        OnChange = Edit14Change
      end
      object GroupBox3: TGroupBox
        Left = 6
        Top = 16
        Width = 112
        Height = 49
        Caption = #1056#1077#1078#1080#1084
        TabOrder = 8
        object ComboBox1: TComboBox
          Left = 7
          Top = 18
          Width = 100
          Height = 21
          Style = csDropDownList
          ItemIndex = 3
          TabOrder = 0
          Text = #1057#1086#1090#1088#1091#1076#1085#1080#1082
          OnChange = ComboBox1Change
          Items.Strings = (
            #1057#1083#1091#1078#1073#1072
            #1054#1090#1076#1077#1083
            #1054#1090#1076#1077#1083#1077#1085#1080#1077
            #1057#1086#1090#1088#1091#1076#1085#1080#1082)
        end
      end
      object Edit15: TEdit
        Left = 135
        Top = 77
        Width = 242
        Height = 21
        TabOrder = 9
        OnChange = Edit15Change
      end
      object ListBox2: TListBox
        Left = 135
        Top = 51
        Width = 242
        Height = 10
        ItemHeight = 13
        TabOrder = 10
        OnDblClick = ListBox2DblClick
        OnKeyPress = ListBox2KeyPress
      end
      object Edit17: TEdit
        Left = 623
        Top = 31
        Width = 107
        Height = 21
        TabOrder = 11
        OnChange = Edit17Change
      end
      object Edit18: TEdit
        Left = 736
        Top = 31
        Width = 110
        Height = 21
        TabOrder = 12
        OnChange = Edit18Change
      end
      object DateTimePicker1: TDateTimePicker
        Left = 623
        Top = 123
        Width = 107
        Height = 21
        Date = 42892.967707372690000000
        Time = 42892.967707372690000000
        TabOrder = 13
        OnChange = DateTimePicker1Change
      end
      object DateTimePicker2: TDateTimePicker
        Left = 736
        Top = 123
        Width = 110
        Height = 21
        Date = 42892.967707372690000000
        Time = 42892.967707372690000000
        TabOrder = 14
        OnChange = DateTimePicker2Change
      end
      object CheckBox2: TCheckBox
        Left = 509
        Top = 123
        Width = 108
        Height = 17
        Caption = #1041#1077#1079' '#1091#1095#1105#1090#1072' '#1083#1080#1084#1080#1090#1072
        TabOrder = 15
        OnClick = CheckBox2Click
      end
      object Edit8: TEdit
        Left = 383
        Top = 31
        Width = 234
        Height = 21
        TabOrder = 16
        OnChange = Edit8Change
      end
    end
    object Button6: TButton
      Left = 1058
      Top = 8
      Width = 132
      Height = 30
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      TabOrder = 2
      OnClick = Button6Click
    end
    object Button1: TButton
      Left = 1058
      Top = 42
      Width = 132
      Height = 30
      Caption = #1055#1086#1076#1088#1086#1073#1085#1077#1077
      TabOrder = 3
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 1058
      Top = 75
      Width = 132
      Height = 30
      Caption = #1055#1077#1095#1072#1090#1100' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1072
      TabOrder = 4
      OnClick = Button2Click
    end
    object CheckBox1: TCheckBox
      Left = 1059
      Top = 15
      Width = 122
      Height = 17
      Caption = #1041#1086#1083#1100#1096#1077' '#1087#1072#1088#1072#1084#1077#1090#1088#1086#1074
      TabOrder = 5
      OnClick = CheckBox1Click
    end
    object AccGridDeps: TDBGrid
      Left = 199
      Top = 162
      Width = 1026
      Height = 436
      DataSource = DataModule1.RecursiveSource
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 6
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnDblClick = AccGridDepsDblClick
    end
    object GridPhoneBook: TDBGrid
      Left = 199
      Top = 162
      Width = 1026
      Height = 433
      DataSource = DataModule1.PBGridSrc
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 7
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnDblClick = GridPhoneBookDblClick
    end
    object ProgressBar1: TProgressBar
      Left = 1058
      Top = 105
      Width = 132
      Height = 5
      TabOrder = 8
      Visible = False
    end
  end
  object MainMenu1: TMainMenu
    Left = 208
    Top = 65528
    object N1: TMenuItem
      Caption = #1060#1072#1081#1083
      object N2: TMenuItem
        Caption = #1048#1084#1087#1086#1088#1090' '#1076#1077#1090#1072#1083#1080#1079#1072#1094#1080#1080
        OnClick = N2Click
      end
      object N5: TMenuItem
        Caption = #1042#1086#1089#1089#1090#1072#1085#1086#1074#1083#1077#1085#1080#1077' '#1076#1072#1085#1085#1099#1093
        OnClick = N5Click
      end
      object N7: TMenuItem
        Caption = #1042#1099#1093#1086#1076
        OnClick = N7Click
      end
    end
    object N3: TMenuItem
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
      OnClick = N3Click
    end
    object N4: TMenuItem
      Caption = #1057#1086#1086#1073#1097#1080#1090#1100' '#1086#1073' '#1086#1096#1080#1073#1082#1077
      OnClick = N4Click
    end
  end
end
