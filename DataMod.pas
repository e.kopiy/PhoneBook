unit DataMod;

interface

uses
  System.SysUtils, System.Classes, Data.DB, DBAccess, MyAccess, MemDS, Vcl.ComCtrls, Vcl.Dialogs, WinApi.Windows;

type
  TDataModule1 = class(TDataModule)
    MyConn: TMyConnection;
    RTQuery: TMyQuery; //query for runtime operations
    RTQuery1: TMyQuery; //query for runtime operations
    PBGridQuery: TMyQuery; // query for phonebook
    AccGridQuery: TMyQuery; // query for detailization form
    PBGridSrc: TMyDataSource; // for phonebook
    AccGridSrc: TMyDataSource; // for detailization form
    RecursiveQuery: TMyQuery; // for deps grid
    RecursiveSource: TMyDataSource; // for deps grid
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);//DataSource for accounting DBGGrid
  private
    { Private declarations }
  public
    { Public declarations }
    function DoQuery(QueryComp:TMyQuery; Query: String): boolean;
    function DoEditQuery(QueryComp:TMyQuery; Query:string): boolean;
    procedure RefreshDepPath;
    procedure RefreshNodes(TreeView: TTreeView);
    procedure RefreshChildNodes(InpTree: TTreeView; ParentNode: TTreeNode);
    procedure RefreshQuery(InpQuery: TMyQuery; Mode: String);
  end;

var
  DM: TDataModule1;
  ErrMess: String;
  ttt: integer;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

uses main, uShowAccountingForm;

procedure TDataModule1.RefreshDepPath;
var
  SqlStr, DepName, IdDep, IdPar, PathToDep: String;
  DepList: TStringList;
  Stop: Boolean;
begin
  SqlStr:= 'SELECT id_dep, id_par FROM dep_list';
  DoQuery(RTQuery, SqlStr);
  try
    DepList:= TStringList.Create;
    while not RTQuery.Eof do
    begin
      PathToDep:= '';
      IdDep:= RTQuery.FieldByName('id_dep').AsString;
      IdPar:= RTQuery.FieldByName('id_par').AsString;
      if IdPar.Equals('0') then
      begin
        SqlStr:= 'UPDATE dep_list SET dep_path=' + IdDep + ' WHERE id_dep=' + IdDep;
        dm.DoEditQuery(dm.AccGridQuery, SqlStr);
      end
      else
      begin
        Stop:= False;
        repeat
          SqlStr:= 'SELECT id_dep, id_par FROM dep_list WHERE';
          SqlStr:= SqlStr + ' id_dep=' + IdPar;
          if dm.DoQuery(dm.AccGridQuery, SqlStr) then
          begin
            IdPar:= AccGridQuery.FieldByName('id_par').AsString;
            if IdPar.Equals('0') then
            begin
              PathToDep:= ',' + AccGridQuery.FieldByName('id_dep').AsString + PathToDep;
              Delete(PathToDep, 1, 1);
              SqlStr:= 'UPDATE dep_list SET dep_path=' + QuotedStr(PathToDep) + ' WHERE id_dep=' + IdDep;
              dm.DoEditQuery(dm.AccGridQuery, SqlStr);
              Stop:= True;
            end
            else
              PathToDep:= ',' + AccGridQuery.FieldByName('id_dep').AsString + PathToDep;
          end;
        until Stop;
      end;
      RTQuery.Next;
    end;
  finally
    DepList.Free;
  end;
end;

procedure TDataModule1.RefreshChildNodes(InpTree: TTreeView; ParentNode: TTreeNode);
var
  i: Integer;
  pId: ^Integer;
  Id: integer;
  SqlStr: String;
begin
  pId:= (ParentNode.Data);
  Id:= pId^;
  SqlStr:=('SELECT dep_name, id_dep FROM dep_list WHERE id_par=');
  SqlStr:= SqlStr + IntToStr(Id) + ' ORDER BY dep_name';
  try
    dm.DoQuery(dm.RTQuery, SqlStr);
    while not dm.RTQuery.Eof do
    begin
      New(pId);
      pId^ := dm.RTQuery.FieldByName('id_dep').AsInteger;
      InpTree.Items.AddChildObject(ParentNode, dm.RTQuery.FieldByName('dep_name').AsString, pId);
      dm.RTQuery.Next;
    end;
  except
    on E: Exception do
      //nothing
  end;
  for i:= 0 to ParentNode.Count - 1 do
    RefreshChildNodes(InpTree, ParentNode.Item[i]);
end;

procedure TDataModule1.RefreshNodes(TreeView: TTreeView);
var
  rec_counter: Integer;
  pId: ^Integer;
  SqlStr: String;
begin
  main.TreeState:= 'Updating';
  TreeView.Items.BeginUpdate;
  SqlStr:='SELECT * FROM dep_list ORDER BY dep_name';
  try
    Form1.DepTree.Items.Clear;
    dm.DoQuery(dm.RTQuery, SqlStr);
    while not dm.RTQuery.Eof do
    begin
      if (dm.RTQuery.FieldByName('id_dep_group').AsString).Equals('������') then
      begin
        New(pId);
        pId^:= dm.RTQuery.FieldByName('id_dep').AsInteger;
        TreeView.Items.AddObject(nil, dm.RTQuery.FieldByName('dep_name').AsString, pId);
      end;
      dm.RTQuery.Next;
    end;
  except
    on E: Exception do
      ShowMessage('������ ���������� ������ ��������');
  end;

  for rec_counter:= TreeView.Items.Count-1 downto 0 do
  begin
    RefreshChildNodes(TreeView, TreeView.Items[rec_counter]);
  end;
  TreeView.Items.EndUpdate;
  main.TreeState:= 'ReadyToWork';
end;

procedure TDataModule1.RefreshQuery(InpQuery: TMyQuery; Mode: String);
var
  i: Integer;
begin
  InpQuery.Active:= False;
  InpQuery.Active:= True;
  if (Mode.Equals('detailization')) then
  begin
    AccForm.DBGrid1.Columns[0].Width:= 562;
    AccForm.DBGrid1.Columns[1].Width:= 150;
    AccForm.DBGrid1.Columns[2].Width:= 150;
  end;
  if (Mode.Equals('empl_accounting_mode')) then
  begin
    Form1.GridPhoneBook.Columns[0].Width:= 150;
    Form1.GridPhoneBook.Columns[1].Width:= 150;
    Form1.GridPhoneBook.Columns[2].Width:= 271;
    Form1.GridPhoneBook.Columns[3].Width:= 91;
    Form1.GridPhoneBook.Columns[4].Width:= 91;
    Form1.GridPhoneBook.Columns[5].Width:= 91;
    Form1.GridPhoneBook.Columns[6].Width:= 91;
    Form1.GridPhoneBook.Columns[7].Width:= 48;
    Form1.GridPhoneBook.Columns[8].Width:= 271;
    Form1.GridPhoneBook.Columns[10].Visible:= False;
    Form1.GridPhoneBook.Columns[11].Visible:= False;
  end;
  if (Mode.Equals('subdep_accounting_mode')) then
  begin
    Form1.AccGridDeps.Columns[0].Width:= 464;
    Form1.AccGridDeps.Columns[1].Width:= 464;
    Form1.AccGridDeps.Columns[2].Width:= 60;
    Form1.AccGridDeps.Columns[3].Visible:= False;
  end;
  if (Mode.Equals('dep_accounting_mode')) then
  begin
    Form1.AccGridDeps.Columns[0].Width:= 464;
    Form1.AccGridDeps.Columns[1].Width:= 464;
    Form1.AccGridDeps.Columns[2].Width:= 60;
    Form1.AccGridDeps.Columns[3].Visible:= False;
  end;
  if (Mode.Equals('serv_accounting_mode')) then
  begin
    Form1.AccGridDeps.Columns[0].Width:= 500;
    Form1.AccGridDeps.Columns[1].Width:= 60;
    Form1.AccGridDeps.Columns[2].Visible:= False;
  end;
  if (Mode.Equals('empl_mode')) then
  begin
    Form1.GridPhoneBook.Columns[0].Width:= 150;
    Form1.GridPhoneBook.Columns[1].Width:= 150;
    Form1.GridPhoneBook.Columns[2].Width:= 271;
    Form1.GridPhoneBook.Columns[3].Width:= 91;
    Form1.GridPhoneBook.Columns[4].Width:= 91;
    Form1.GridPhoneBook.Columns[5].Width:= 91;
    Form1.GridPhoneBook.Columns[6].Width:= 91;
    Form1.GridPhoneBook.Columns[7].Width:= 271;
    Form1.GridPhoneBook.Columns[8].Width:= 29;
    Form1.GridPhoneBook.Columns[9].Visible:= False;
    Form1.GridPhoneBook.Columns[10].Visible:= False;
  end;
  if (Mode.Equals('ServList_mode')) then
  begin
    Form1.GridPhoneBook.Columns[1].Visible:= False;
  end;
  if (Mode.Equals('DepList_mode')) then
  begin
    Form1.GridPhoneBook.Columns[0].Width:= 400;
    Form1.GridPhoneBook.Columns[1].Width:= 400;
    Form1.GridPhoneBook.Columns[Form1.GridPhoneBook.Columns.Count - 1].Visible:= False;
  end;
end;

procedure TDataModule1.DataModuleCreate(Sender: TObject);
begin
  RefreshQuery(PBGridQuery, 'empl_mode');
  RefreshNodes(Form1.DepTree);
end;

procedure TDataModule1.DataModuleDestroy(Sender: TObject);
begin
  MyConn.Disconnect;
end;

function TDataModule1.DoEditQuery(QueryComp:TMyQuery; Query:string): boolean;
begin
  if dm.MyConn.Connected then
  begin
    try
    QueryComp.Close;
    QueryComp.Sql.Clear;
    QueryComp.Sql.Add(Query);
    QueryComp.ExecSql;
    Result:=True;
    except
      on E: Exception do begin Result:=False; ShowMessage('������ �������: ' + Query); end;
    end;
  end
  else
  begin
    dm.MyConn.Connect;
    if dm.MyConn.Connected then
    begin
      try
        QueryComp.Close;
        QueryComp.Sql.Clear;
        QueryComp.Sql.Add(Query);
        QueryComp.ExecSql;
      except
        on E: Exception do begin Result:=False; ShowMessage('������ �������: ' + Query); end;
      end;
    end;
  end;
end;

function TDataModule1.DoQuery(QueryComp:TMyQuery; Query: String): boolean;
begin
  Result:= False;
  if MyConn.Connected then
  begin
    //Query:=mysql_real_escape_string(Query);
    try
      QueryComp.Close;
      QueryComp.Sql.Clear;
      QueryComp.Sql.Add(Query);
      QueryComp.Open;
      if not QueryComp.IsEmpty then
      begin
        Result:=True;
      end
      else
      begin
        Result:= False;
      end;
    except
      on E: Exception do begin ttt:= 1; Result:=False; ShowMessage('������ �������: ' + Query); end;
    end;
  end
  else
  begin
    ShowMessage('������ ���������� � MySQL');
  end;
end;

end.
