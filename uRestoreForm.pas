unit uRestoreForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.StdCtrls;

type
  TRestoreForm = class(TForm)
    Label1: TLabel;
    ListBox1: TListBox;
    Button1: TButton;
    Button2: TButton;
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure RestoreData(TimePoint: String);
    procedure RemoveDuplicates(TimePoint: String);
  end;

var
  RestoreForm: TRestoreForm;

implementation

{$R *.dfm}

uses DataMod, main;

procedure TRestoreForm.RemoveDuplicates(TimePoint: String);
var
  SqlStr: String;
begin
  SqlStr:= 'DELETE FROM call_data WHERE id_item IN (SELECT id_item FROM call_data_bkp';
  SqlStr:= SqlStr + ' WHERE date_time='+QuotedStr(TimePoint) + ')';
  dm.DoEditQuery(dm.RTQuery, SqlStr);

  SqlStr:= 'DELETE FROM empl_list WHERE id_empl IN (SELECT id_empl FROM empl_list_bkp';
  SqlStr:= SqlStr + ' WHERE date_time='+QuotedStr(TimePoint) + ')';
  dm.DoEditQuery(dm.RTQuery, SqlStr);

  SqlStr:= 'DELETE FROM dep_list WHERE id_dep IN (SELECT id_dep FROM dep_list_bkp';
  SqlStr:= SqlStr + ' WHERE date_time='+QuotedStr(TimePoint) + ')';
  dm.DoEditQuery(dm.RTQuery, SqlStr);
end;

procedure TRestoreForm.RestoreData(TimePoint: String);
var
  SqlStr: String;
begin
  TimePoint:= FormatDateTime('yyyy-mm-dd hh:mm:ss', StrToDateTime(TimePoint));

  RemoveDuplicates(TimePoint);

  SqlStr:= 'INSERT INTO dep_list';
  SqlStr:= SqlStr + ' (id_dep, id_dep_group, dep_name, dep_name_short, dep_path,';
  SqlStr:= SqlStr + ' id_par)';
  SqlStr:= SqlStr + ' SELECT id_dep, id_dep_group, dep_name,';
  SqlStr:= SqlStr + ' dep_name_short, dep_path, id_par';
  SqlStr:= SqlStr + ' FROM dep_list_bkp WHERE dep_list_bkp.date_time='+QuotedStr(TimePoint);
  dm.DoEditQuery(dm.RTQuery, SqlStr);

  SqlStr:= 'INSERT INTO empl_list';
  SqlStr:= SqlStr + ' (id_empl,id_dep,empl_post,empl_fname,empl_lfname,empl_lname,';
  SqlStr:= SqlStr + 'empl_phone,empl_fax,empl_int_phone,empl_city_te_number,';
  SqlStr:= SqlStr + 'empl_addr,empl_room)';
  SqlStr:= SqlStr + ' SELECT id_empl,id_dep,empl_post,empl_fname,empl_lfname,';
  SqlStr:= SqlStr + 'empl_lname,empl_phone,empl_fax,empl_int_phone,empl_city_te_number,';
  SqlStr:= SqlStr + 'empl_addr,empl_room FROM empl_list_bkp ';
  SqlStr:= SqlStr + 'WHERE empl_list_bkp.date_time='+QuotedStr(TimePoint);
  dm.DoEditQuery(dm.RTQuery, SqlStr);

  SqlStr:= 'INSERT INTO call_data';
  SqlStr:= SqlStr + ' (id_item,id_empl,item_date,item_time,item_amount,item_cost)';
  SqlStr:= SqlStr + ' SELECT id_item,id_empl,item_date,item_time,item_amount,item_cost';
  SqlStr:= SqlStr + ' FROM call_data_bkp WHERE call_data_bkp.date_time='+QuotedStr(TimePoint);
  dm.DoEditQuery(dm.RTQuery, SqlStr);

  SqlStr:= 'DELETE FROM time_points WHERE';
  SqlStr:= SqlStr + ' time_point='+QuotedStr(TimePoint);
  dm.DoEditQuery(dm.RTQuery, SqlStr);
  SqlStr:= 'DELETE FROM call_data_bkp WHERE';
  SqlStr:= SqlStr + ' date_time='+QuotedStr(TimePoint);
  dm.DoEditQuery(dm.RTQuery, SqlStr);
  SqlStr:= 'DELETE FROM empl_list_bkp WHERE';
  SqlStr:= SqlStr + ' date_time='+QuotedStr(TimePoint);
  dm.DoEditQuery(dm.RTQuery, SqlStr);
  SqlStr:= 'DELETE FROM dep_list_bkp WHERE';
  SqlStr:= SqlStr + ' date_time='+QuotedStr(TimePoint);
  dm.DoEditQuery(dm.RTQuery, SqlStr);

  case Form1.ComboBox1.ItemIndex of
    0: dm.RefreshQuery(dm.PBGridQuery, 'ServList_mode');
    1: dm.RefreshQuery(dm.PBGridQuery, 'DepList_mode');
    2: dm.RefreshQuery(dm.PBGridQuery, 'DepList_mode');
    3: dm.RefreshQuery(dm.PBGridQuery, 'empl_mode');
  end;
  dm.RefreshNodes(Form1.DepTree);
end;

procedure TRestoreForm.Button1Click(Sender: TObject);
var
  i, j: integer;
begin
  i:= 0;
  j:= ListBox1.ItemIndex;
  if ListBox1.ItemIndex >= 0 then
  begin
    repeat
      RestoreData(ListBox1.Items[i]);
      inc(i,1);
    until i > j;
    i:= 0;
    repeat
      ListBox1.Items.Delete(0);
      inc(i,1);
    until i > j;
    dm.RefreshDepPath;
    ShowMessage('�������������� ���������');
  end
  else
    ShowMessage('����������, �������� ����� ��������������');
end;

procedure TRestoreForm.Button2Click(Sender: TObject);
begin
  RestoreForm.Close;
end;

procedure TRestoreForm.FormShow(Sender: TObject);
begin
  ListBox1.Items.Clear;
  dm.DoQuery(dm.RTQuery, 'SELECT time_point FROM time_points ORDER BY time_point DESC');
  while not dm.RTQuery.Eof do
  begin
    ListBox1.Items.Add(dm.RTQuery.FieldByName('time_point').AsString);
    dm.RTQuery.Next;
  end;
  ListBox1.ItemIndex:= 0;
end;

end.
