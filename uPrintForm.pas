unit uPrintForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.CheckLst,
  Vcl.ExtCtrls, uReportGenerator;

type
  TPrintForm = class(TForm)
    GroupBox1: TGroupBox;
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    Label1: TLabel;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    Button1: TButton;
    Button2: TButton;
    CheckListBox1: TCheckListBox;
    Button3: TButton;
    Button4: TButton;
    ProgressBar1: TProgressBar;
    Label2: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GroupBox1Click(Sender: TObject);
    procedure DateTimePicker1Change(Sender: TObject);
    procedure DateTimePicker2Change(Sender: TObject);
  private
    { Private declarations }
    procedure LoadData;
  public
    { Public declarations }
  end;

var
  PrintForm: TPrintForm;
  ReportGenerateThread: TReportGeneratorThread;

implementation

{$R *.dfm}

uses Main, DataMod;

procedure TPrintForm.Button1Click(Sender: TObject);
begin
  CheckListBox1.CheckAll(cbChecked);
end;

procedure TPrintForm.Button2Click(Sender: TObject);
begin
  CheckListBox1.CheckAll(cbUnchecked);
end;

procedure TPrintForm.Button3Click(Sender: TObject);
begin
  PrintForm.Close;
end;

procedure TPrintForm.Button4Click(Sender: TObject);
begin
  ProgressBar1.Visible:= True;
  ReportGenerateThread:= TReportGeneratorThread.Create(True);
  ReportGenerateThread.Priority:= tpNormal;
  ReportGenerateThread.FreeOnTerminate:= True;
  ReportGenerateThread.Start;

  DateTimePicker1.Enabled:= False;
  DateTimePicker2.Enabled:= False;
  CheckListBox1.Enabled:= False;
  Button1.Enabled:= False;
  Button2.Enabled:= False;
  Button3.Enabled:= False;
  Button4.Enabled:= False;
  Label2.Visible:= True;
  ProgressBar1.Visible:= True;
  Button3.SendToBack;
  Button4.SendToBack;
  //PrintForm.Close;
end;

procedure TPrintForm.DateTimePicker1Change(Sender: TObject);
begin
  Form1.DateTimePicker1.Date:= DateTimePicker1.Date;
  Form1.DateTimePicker1Change(Form1);
end;

procedure TPrintForm.DateTimePicker2Change(Sender: TObject);
begin
  Form1.DateTimePicker2.Date:= DateTimePicker2.Date;
  Form1.DateTimePicker2Change(Form1);
end;

procedure TPrintForm.LoadData;
var
  SqlStr, FieldName, DepName, UpperDepName: String;
  IdDep: Integer;
begin
  UpperDepName:= '';
  case Form1.ComboBox1.ItemIndex of
    0: FieldName:= '������������ ������';
    1:
    begin
      FieldName:= '������������ ������';
    end;
    2:
    begin
      FieldName:= '������������ ���������';
    end;
  end;
  dm.RecursiveQuery.First;
  while not dm.RecursiveQuery.Eof do
  begin
    IdDep:= Form1.AccGridDeps.DataSource.DataSet.FieldByName('id_dep').AsInteger;
    DepName:= Form1.AccGridDeps.DataSource.DataSet.FieldByName(FieldName).AsString;
    if Form1.ComboBox1.ItemIndex>0 then
      UpperDepName:= ' (' + Form1.AccGridDeps.DataSource.DataSet.FieldByName('����������� ������').AsString + ')';
    CheckListBox1.AddItem(DepName + UpperDepName, TObject(IdDep));
    dm.RecursiveQuery.Next;
  end;
  CheckListBox1.CheckAll(cbChecked);
end;

procedure TPrintForm.FormShow(Sender: TObject);
begin
  CheckListBox1.Items.Clear;
  Label2.Parent:= ProgressBar1;
  Label2.Top:= 9;
  Label2.Left:= 68;
  Label2.Transparent:= true;
  Label2.bringtofront;
  LoadData;
  if Form1.CheckBox1.Checked then
  begin
    DateTimePicker1.Enabled:= True;
    DateTimePicker2.Enabled:= True;
    DateTimePicker1.Date:= Form1.DateTimePicker1.Date;
    DateTimePicker2.Date:= Form1.DateTimePicker2.Date;
  end
  else
  begin
    DateTimePicker1.Enabled:= False;
    DateTimePicker2.Enabled:= False;
  end;
end;

procedure TPrintForm.GroupBox1Click(Sender: TObject);
begin
  if not DateTimePicker1.Enabled then
    ShowMessage('��� ������� �� ���� ���������� ������ `������ ����������` �� ������� ����� ����������');
end;

end.
