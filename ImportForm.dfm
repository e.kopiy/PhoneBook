object FormImport: TFormImport
  Left = 0
  Top = 0
  Caption = #1048#1084#1087#1086#1088#1090' '#1088#1072#1089#1093#1086#1076#1086#1074
  ClientHeight = 268
  ClientWidth = 774
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 150
    Height = 13
    Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1092#1072#1081#1083' '#1076#1083#1103' '#1080#1084#1087#1086#1088#1090#1072':'
  end
  object Label2: TLabel
    Left = 288
    Top = 8
    Width = 235
    Height = 13
    Caption = #1053#1086#1084#1077#1088#1072' '#1090#1077#1083#1077#1092#1086#1085#1086#1074', '#1087#1088#1086#1080#1075#1085#1086#1088#1080#1088#1086#1074#1072#1085#1085#1099#1093' '#1080#1079'-'#1079#1072
  end
  object Label3: TLabel
    Left = 288
    Top = 22
    Width = 192
    Height = 13
    Caption = #1085#1077#1089#1086#1086#1090#1074#1077#1090#1089#1090#1074#1080#1103' '#1092#1086#1088#1084#1072#1090#1091' '#1061#1061#1061#1061#1061#1061#1061#1061#1061#1061
  end
  object Label5: TLabel
    Left = 529
    Top = 8
    Width = 228
    Height = 13
    Caption = #1053#1086#1084#1077#1088#1072' '#1090#1077#1083#1077#1092#1086#1085#1086#1074', '#1085#1077' '#1074#1085#1077#1089#1105#1085#1085#1099#1093' '#1074' '#1041#1044' '#1080#1079'-'#1079#1072
  end
  object Label4: TLabel
    Left = 529
    Top = 22
    Width = 135
    Height = 13
    Caption = #1086#1090#1089#1091#1090#1089#1090#1074#1080#1103' '#1074' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1077
  end
  object ListBox1: TListBox
    Left = 8
    Top = 27
    Width = 266
    Height = 184
    ItemHeight = 13
    TabOrder = 0
  end
  object Button1: TButton
    Left = 8
    Top = 217
    Width = 115
    Height = 33
    Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1076#1072#1085#1085#1099#1077
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 159
    Top = 217
    Width = 115
    Height = 33
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 2
    OnClick = Button2Click
  end
  object ProgressBar1: TProgressBar
    Left = 8
    Top = 251
    Width = 266
    Height = 12
    TabOrder = 3
    Visible = False
  end
  object Memo1: TMemo
    Left = 288
    Top = 41
    Width = 235
    Height = 222
    TabOrder = 4
  end
  object Memo2: TMemo
    Left = 529
    Top = 41
    Width = 235
    Height = 222
    TabOrder = 5
  end
end
