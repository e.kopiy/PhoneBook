unit settingsForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Main, uRegHashUtils;

type
  TForm8 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Edit4: TEdit;
    Edit5: TEdit;
    Label5: TLabel;
    Edit6: TEdit;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Edit2: TEdit;
    Label1: TLabel;
    Edit1: TEdit;
    Label3: TLabel;
    Edit3: TEdit;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form8: TForm8;

implementation

{$R *.dfm}

procedure TForm8.Button1Click(Sender: TObject);
begin
  Settings.ColPhoneId:= StrToInt(Edit1.Text);
  Settings.CostLimit:= StrToInt(Edit2.Text);
  Settings.ColAmount:= StrToInt(Edit3.Text);
  Settings.ColPhoneCode:= StrToInt(Edit4.Text);
  Settings.ColDateTime:= StrToInt(Edit5.Text);
  Settings.ColCost:= StrToInt(Edit6.Text);
  WriteSettings(Settings);
  Form8.Close;
end;

procedure TForm8.Button2Click(Sender: TObject);
begin
  Form8.Close;
end;

procedure TForm8.FormShow(Sender: TObject);
var
  Res: integer;
begin
  Res:= 0;
  ReadSettings(Settings, Res);
  if Res < 6 then
  begin
    Settings.ColDateTime:= 1;
    Settings.ColPhoneCode:= 4;
    Settings.ColPhoneId:= 5;
    Settings.ColAmount:= 10;
    Settings.ColCost:= 16;
    Settings.CostLimit:= 500;
    WriteSettings(Settings);
  end
  else
  begin
    Edit1.Text:= IntToStr(Settings.ColPhoneId);
    Edit2.Text:= IntToStr(Settings.CostLimit);
    Edit3.Text:= IntToStr(Settings.ColAmount);
    Edit4.Text:= IntToStr(Settings.ColPhoneCode);
    Edit5.Text:= IntToStr(Settings.ColDateTime);
    Edit6.Text:= IntToStr(Settings.ColCost);
  end;

end;

end.
