object Form8: TForm8
  Left = 0
  Top = 0
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
  ClientHeight = 220
  ClientWidth = 648
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 419
    Top = 179
    Width = 97
    Height = 33
    Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 518
    Top = 179
    Width = 122
    Height = 33
    Caption = #1054#1090#1084#1077#1085#1080#1090#1100' '#1080' '#1074#1099#1081#1090#1080
    TabOrder = 1
    OnClick = Button2Click
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 405
    Height = 206
    Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' '#1080#1084#1087#1086#1088#1090#1072
    TabOrder = 2
    object Label4: TLabel
      Left = 8
      Top = 37
      Width = 170
      Height = 13
      Caption = #1053#1086#1084#1077#1088' '#1089#1090#1086#1083#1073#1094#1072' '#1089' '#1082#1086#1076#1086#1084' '#1090#1077#1083#1077#1092#1086#1085#1072
    end
    object Label5: TLabel
      Left = 8
      Top = 85
      Width = 177
      Height = 13
      Caption = #1053#1086#1084#1077#1088' '#1089#1090#1086#1083#1073#1094#1072' '#1089' '#1076#1072#1090#1086#1081' '#1080' '#1074#1088#1077#1084#1077#1085#1077#1084
    end
    object Label6: TLabel
      Left = 205
      Top = 104
      Width = 191
      Height = 13
      Caption = #1053#1086#1084#1077#1088' '#1089#1090#1086#1083#1073#1094#1072' '#1089#1086' '#1089#1090#1086#1080#1084#1086#1089#1090#1100#1102' '#1079#1074#1086#1085#1082#1072
    end
    object Label7: TLabel
      Left = 8
      Top = 104
      Width = 91
      Height = 13
      Caption = #1085#1072#1095#1072#1083#1072' '#1088#1072#1079#1075#1086#1074#1086#1088#1072
    end
    object Label8: TLabel
      Left = 8
      Top = 18
      Width = 154
      Height = 13
      Caption = '! '#1053#1091#1084#1077#1088#1072#1094#1080#1103' '#1089#1090#1086#1083#1073#1094#1086#1074' '#1089' '#1085#1091#1083#1103' !'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 205
      Top = 37
      Width = 181
      Height = 13
      Caption = #1053#1086#1084#1077#1088' '#1089#1090#1086#1083#1073#1094#1072' '#1089' '#1085#1086#1084#1077#1088#1086#1084' '#1090#1077#1083#1077#1092#1086#1085#1072
    end
    object Label3: TLabel
      Left = 8
      Top = 152
      Width = 184
      Height = 13
      Caption = #1053#1086#1084#1077#1088' '#1089#1090#1086#1083#1073#1094#1072' '#1089' '#1082#1086#1083#1080#1095#1077#1089#1090#1074#1086#1084' '#1084#1080#1085#1091#1090
    end
    object Edit4: TEdit
      Left = 8
      Top = 56
      Width = 191
      Height = 21
      NumbersOnly = True
      TabOrder = 0
    end
    object Edit5: TEdit
      Left = 8
      Top = 123
      Width = 191
      Height = 21
      NumbersOnly = True
      TabOrder = 1
    end
    object Edit6: TEdit
      Left = 205
      Top = 123
      Width = 191
      Height = 21
      NumbersOnly = True
      TabOrder = 2
    end
    object Edit1: TEdit
      Left = 205
      Top = 56
      Width = 191
      Height = 21
      NumbersOnly = True
      TabOrder = 3
    end
    object Edit3: TEdit
      Left = 8
      Top = 171
      Width = 191
      Height = 21
      NumbersOnly = True
      TabOrder = 4
    end
  end
  object GroupBox2: TGroupBox
    Left = 419
    Top = 8
    Width = 222
    Height = 72
    Caption = #1051#1080#1084#1080#1090#1099
    TabOrder = 3
    object Label2: TLabel
      Left = 6
      Top = 19
      Width = 121
      Height = 13
      Caption = #1051#1080#1084#1080#1090' '#1090#1088#1072#1090' '#1089#1086#1090#1088#1091#1076#1085#1080#1082#1072
    end
    object Edit2: TEdit
      Left = 6
      Top = 38
      Width = 210
      Height = 21
      TabOrder = 0
    end
  end
end
