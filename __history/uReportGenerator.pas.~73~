unit uReportGenerator;

interface

uses
  System.Classes, LibXL, ShellAPI, Winapi.Windows, SysUtils,
  Vcl.ComCtrls, Vcl.Dialogs, uStat, Vcl.Forms;

type
  TReportGeneratorThread = class(TThread)
  private
    { Private declarations }
    Progress: integer;
    Delta: integer;
    IdDepList: TStringList;
    DateRangeStr: String;
    CostLimit: String;
    procedure GenerateReport;
    procedure GetIdDepList;
    procedure GetDateRange;
    procedure GetCostLimit;
    procedure FreeVCL;
    procedure SetProgress;
    function CheckToInclude(rec1: TOvercostCnt; rec2:TSummaryStat): boolean;
    function GetDepCntPhrase(IdDep: Integer): String;
  protected
    procedure Execute; override;
  end;

implementation

uses Main, uPrintForm, DataMod;

{ 
  Important: Methods and properties of objects in visual components can only be
  used in a method called using Synchronize, for example,

      Synchronize(UpdateCaption);  

  and UpdateCaption could look like,

    procedure TReportGeneratorThread.UpdateCaption;
    begin
      Form1.Caption := 'Updated in a thread';
    end; 

    or
    
    Synchronize( 
      procedure 
      begin
        Form1.Caption := 'Updated in thread via an anonymous method' 
      end
      )
    );
    
  where an anonymous method is passed.
  
  Similarly, the developer can call the Queue method with similar parameters as 
  above, instead passing another TThread class as the first parameter, putting
  the calling thread in a queue with the other thread.
    
}

{ TReportGeneratorThread }

procedure TReportGeneratorThread.SetProgress;
begin
  PrintForm.ProgressBar1.Position:= Progress;
end;

procedure TReportGeneratorThread.FreeVCL;
begin
  if Form1.CheckBox1.Checked then
  begin
    PrintForm.DateTimePicker1.Enabled:= True;
    PrintForm.DateTimePicker2.Enabled:= True;
  end
  else
  begin
    PrintForm.DateTimePicker1.Enabled:= False;
    PrintForm.DateTimePicker2.Enabled:= False;
  end;
  PrintForm.CheckListBox1.Enabled:= True;
  PrintForm.Button1.Enabled:= True;
  PrintForm.Button2.Enabled:= True;
  PrintForm.Button3.Enabled:= True;
  PrintForm.Button4.Enabled:= True;
  PrintForm.Label2.Visible:= False;
  PrintForm.ProgressBar1.Visible:= False;
  PrintForm.Button3.BringToFront;
  PrintForm.Button4.BringToFront;
end;

procedure TReportGeneratorThread.GetIdDepList;
var
  i: Integer;
begin
  for i:= 0 to PrintForm.CheckListBox1.Count - 1 do
  begin
    if PrintForm.CheckListBox1.Checked[i] then
      IdDepList.AddObject(PrintForm.CheckListBox1.Items[i], PrintForm.CheckListBox1.Items.Objects[i]);
  end;
end;

procedure TReportGeneratorThread.GetDateRange;
var
  Range: TDateRange;
begin
  DateRangeStr:= '';
  if Form1.CheckBox1.Checked then
  begin
    Range:= Form1.GetDateRange;
    DateRangeStr:= ' t1.item_date >= ' + Range[0];
    DateRangeStr:= DateRangeStr + ' and t1.item_date <= ' + Range[1] + ' and ';
  end;
end;

procedure TReportGeneratorThread.GetCostLimit;
begin
  CostLimit:= IntToStr(main.Settings.CostLimit);
end;

function TReportGeneratorThread.CheckToInclude(rec1: TOvercostCnt; rec2:TSummaryStat): boolean;
begin
  Result:= False;
  if (StrToInt(rec1.EmplCnt) > 0) then
    Result:= True;
  if (StrToInt(rec1.DepCnt) > 0) then
    Result:= True;
  if (StrToInt(rec1.OverCost) > 0) then
    Result:= True;
  if (StrToInt(rec2.CallCnt) > 0) then
    Result:= True;
  if (StrToInt(rec2.MinutesCnt) > 0) then
    Result:= True;
end;

function TReportGeneratorThread.GetDepCntPhrase(IdDep: Integer): String;
var
  SqlStr: String;
begin
  Result:= '';
  SqlStr:= 'SELECT id_dep_group FROM dep_list WHERE id_dep=' + IntToStr(IdDep);
  dm.DoQuery(dm.RTQuery, SqlStr);
  if dm.RTQuery.FieldByName('id_dep_group').AsString.Equals('������') then
    Result:= '���������� �������� ����������� ����� (����������� � ������)';
  if dm.RTQuery.FieldByName('id_dep_group').AsString.Equals('�����') then
    Result:= '���������� �������� ����������� ����� (����������� � �����)';
  if dm.RTQuery.FieldByName('id_dep_group').AsString.Equals('���������') then
    Result:= '���������� �����������, ����������� �����';
end;

procedure TReportGeneratorThread.GenerateReport;
var
  Book: TBook;
  Sheet, ServDepSheet, SubDepSheet: TSheet;
  FEmplPost, FSheetHeader, FDescString, FCasualText: TFormat;
  FSubObjectName, FOverCostEmplCnt: TFormat;
  PDepNameStr, DepName, OvercostDepCnt, OvercostEmplCnt, SumOvercost, CallCnt, MinutesCnt, DepCntPhrase: PAnsiChar;
  SubDepName, SubOvercostEmplCnt, SubSumOvercost, SubCallCnt, SubMinutesCnt: PAnsiChar;
  EmplLastFirstMidName, EmplPost, EmplMinutesCnt, EmplCallCnt, EmplOvercost: PAnsiChar;
  EmplPhonesStr, AnsiRepDateTime, PathToRep: PAnsiChar;
  IdDep, SubIdDep, SqlStr, StringEmplPhones: String;
  i, c, k, DepRowToWrite, EmplRowToWrite, PhonesCnt: integer;
  buf1: TOvercostCnt;
  buf2: TSummaryStat;
  buf3: TDepInfo;
  EmplInfo: TEmplInfo;
  SubDepList: TStringList;
  RepDateTime: TDateTime;
  StrRepDateTime: PChar;
  DepAppendix: integer;// ���� ��������� ��������, ��������� ����� � ����� �����
  DepNameStr: String;
  DepNameList: TStringList;
begin
  Synchronize(GetIdDepList);
  Synchronize(GetDateRange);
  Synchronize(GetCostLimit);
  Progress:= 0;
  Synchronize(SetProgress);
  Delta:= Round(100/(IdDepList.Count));
  EmplInfo.Phones:= TStringList.Create;
  SubDepList:= TStringList.Create;
  Book:= TBinBook.Create;
  Book.load('report_tpl.xls');
  DepAppendix:=1;
  DepNameList:= TStringList.Create;
//BEGIN: Formats init
  ServDepSheet:= Book.getSheet(0);
  SubDepSheet:= Book.getSheet(1);  
  FSheetHeader:= ServDepSheet.cellFormat(0, 0); //��������� �������� �������
  FDescString:= ServDepSheet.cellFormat(2, 0); //���������� ����������� ����������� �����
  FCasualText:= ServDepSheet.cellFormat(1, 2);
  FSubObjectName:= ServDepSheet.cellFormat(10, 1); // ��� ������������ �������
  FOverCostEmplCnt:= ServDepSheet.cellFormat(10, 3); // ���������� �����������, ����������� �����
  FEmplPost:= ServDepSheet.cellFormat(15, 1); //���������
  FSheetHeader.wrap:= True;
  FCasualText.wrap:= True;
  FSubObjectName.wrap:= True;
  FEmplPost.wrap:= True;
  FSubObjectName.setBorder(BORDERSTYLE_THIN);
  FOverCostEmplCnt.setBorder(BORDERSTYLE_THIN);
  FSubObjectName.setBorderColor(COLOR_BLACK);
  FOverCostEmplCnt.setBorderColor(COLOR_BLACK);
//END: Formats init

  for i:=0 to IdDepList.Count - 1 do
  begin
//BEGIN: ������ ������ ��� ��������� �������
    DepNameStr:= IdDepList[i];
    Delete(DepNameStr, 8, (Length(DepNameStr)));
    if DepNameList.IndexOf(DepNameStr) <> -1 then
    begin
      Insert(IntToStr(DepAppendix), DepNameStr, 8);
      inc(DepAppendix,1);
    end;
    DepNameList.Add(DepNameStr);

    PDepNameStr:= PAnsiChar(AnsiString(DepNameStr));
    DepName:= PAnsiChar(AnsiString(IdDepList[i]));
    IdDep:= IntToStr(Integer(IdDepList.Objects[i]));
    DepRowToWrite:= 10;
    EmplRowToWrite:= 15;
    SubDepList.Clear;

    buf1:= GetOvercosted(IdDep, DateRangeStr, 'WithSubDeps');
    OvercostEmplCnt:= PAnsiChar(AnsiString(buf1.EmplCnt));
    OvercostDepCnt:= PAnsiChar(AnsiString(buf1.DepCnt));
    DepCntPhrase:= PAnsiChar(AnsiString(GetDepCntPhrase(Integer(IdDepList.Objects[i]))));
    SumOvercost:= PAnsiChar(AnsiString(buf1.OverCost));

    buf2:= GetSummaryStat(IdDep, DateRangeStr, 'WithSubDeps');
    CallCnt:= PAnsiChar(AnsiString(buf2.CallCnt));
    MinutesCnt:= PAnsiChar(AnsiString(buf2.MinutesCnt));

    if CheckToInclude(buf1, buf2) then
    begin
      if GetObjType(Integer(IdDepList.Objects[i])).Equals('���������') then
      begin
        Sheet:= Book.addSheet(PDepNameStr, SubDepSheet)
      end
      else
        Sheet:= Book.addSheet(PDepNameStr, ServDepSheet);
      Sheet.writeStr(0, 0, PDepNameStr, FSheetHeader);
      Sheet.writeStr(1, 3, SumOvercost, FCasualText);
      Sheet.writeStr(2, 3, OvercostDepCnt, FCasualText);
      Sheet.writeStr(2, 0, DepCntPhrase, FDescString);
      Sheet.writeStr(3, 3, OvercostEmplCnt, FCasualText);
      Sheet.writeStr(4, 3, CallCnt, FCasualText);
      Sheet.writeStr(5, 3, MinutesCnt, FCasualText);
    end;
//END: ������ ������ ��� ��������� ������

//BEGIN: ������ ������ �� �������

  //BEGIN: ���� ����������� ������ � ��������� � ����� �� � ������
    if not GetObjType(Integer(IdDepList.Objects[i])).Equals('���������') then
    begin
      SqlStr:= 'SET @a = ' + IntToStr(Integer(IdDepList.Objects[i]));
      dm.DoEditQuery(dm.RTQuery, SqlStr);
      SqlStr:= 'SELECT id_dep FROM dep_list WHERE id_dep in (SELECT id FROM (';
      SqlStr:= SqlStr + 'SELECT @index:=INSTR(@a,' + QuotedStr(',') + ') as coma_index,';
      SqlStr:= SqlStr + '@id:=SUBSTRING_INDEX(@a, ' + QuotedStr(',') + ', 1)  as id,';
      SqlStr:= SqlStr + '@b:=SUBSTRING(@a,IF(@index,@index+1,0)),';
      SqlStr:= SqlStr + '@a:=CONCAT_WS(IF(LENGTH(@b)>0,' + QuotedStr(',') + ',' + QuotedStr('') + '),';
      SqlStr:= SqlStr + '@b,(SELECT group_concat(id_dep) FROM dep_list WHERE id_par=@id)) as array ';
      SqlStr:= SqlStr + 'FROM dep_list WHERE LENGTH(@a)>0) as tt1)';
      if dm.DoQuery(dm.RTQuery, SqlStr) then
      begin
        while not dm.RTQuery.Eof do
        begin
          SubDepList.Add(dm.RTQuery.FieldByName('id_dep').AsString);
          dm.RTQuery.Next;
        end;
      end;
    end
    else
    begin
      SubDepList.Clear;
      SubDepList.Add(IdDep);
    end;
  //END: ���� ����������� ������ � ��������� � ����� �� � ������

  //BEGIN: ��� �� ������ � ����� ���������� � ������ ������� � �����
  //���� ����� �� ����������, �� �� ��������� ���� ����
    if not (GetObjType(Integer(IdDepList.Objects[i])).Equals('���������')) then
    begin
      for c:= 1 to SubDepList.Count - 1 do
      begin
        buf3:= GetDepInfo(StrToInt(SubDepList[c]), DateRangeStr, 'WithoutSubDeps');
        if StrToInt(buf3.CallCnt) > 0 then
        begin
          SubDepName:= PAnsiChar(AnsiString(buf3.DepName));
          SubOvercostEmplCnt:= PAnsiChar(AnsiString(buf3.OvercostEmplCnt));
          SubSumOvercost:= PAnsiChar(AnsiString(buf3.SumOvercost));
          SubCallCnt:= PAnsiChar(AnsiString(buf3.CallCnt));
          SubMinutesCnt:= PAnsiChar(AnsiString(buf3.MinutesCnt));

          Sheet.writeStr(DepRowToWrite, 0, SubDepName, FSubObjectName);
          Sheet.writeStr(DepRowToWrite, 2, SubOvercostEmplCnt, FOverCostEmplCnt);
          Sheet.writeStr(DepRowToWrite, 4, SubMinutesCnt, FCasualText);
          Sheet.writeStr(DepRowToWrite, 5, SubCallCnt, FCasualText);
          Sheet.writeStr(DepRowToWrite, 6, SubSumOvercost, FCasualText);
          Sheet.setMerge(DepRowToWrite, DepRowToWrite, 0, 1);
          Sheet.setMerge(DepRowToWrite, DepRowToWrite, 2, 3);
          Sheet.setRow(DepRowToWrite, 15);
          Sheet.insertRow(DepRowToWrite + 1, DepRowToWrite + 1);
          DepRowToWrite:= DepRowToWrite + 1;
          EmplRowToWrite:= EmplRowToWrite + 1;
        end;
      end;
    end
    else
      EmplRowToWrite:= 10;
  //END: ��� �� ������ � ����� ���������� � ������ ������� � �����

  //BEGIN: ��� ��� �������� �� ������ � ����� ���������� � ������ ����������, � �������� ���� �������
    for c:= 0 to SubDepList.Count - 1 do
    begin
      SqlStr:= 'SELECT t2.id_empl FROM grouped_costs_date_id t1 LEFT JOIN empl_list t2 on t1.id_empl=t2.id_empl';
      SqlStr:= SqlStr + ' WHERE ' + DateRangeStr + ' t2.id_dep=' + SubDepList[c] + ' GROUP BY t2.id_empl';
      if dm.DoQuery(dm.RTQuery1, SqlStr) then
        while not dm.RTQuery1.Eof do
        begin
          EmplInfo:= GetEmplInfo(dm.RTQuery1.FieldByName('id_empl').AsString, DateRangeStr);
          PhonesCnt:= 0;
          if not EmplInfo.Post.Equals('empty') then
          begin
            if StrToInt(EmplInfo.CallCnt) > 0 then
            begin         
              EmplLastFirstMidName:= PAnsiChar(AnsiString(EmplInfo.LastFirstMidName));
              EmplPost:= PAnsiChar(AnsiString(EmplInfo.Post));
              EmplMinutesCnt:= PAnsiChar(AnsiString(EmplInfo.MinutesCnt));
              EmplCallCnt:= PAnsiChar(AnsiString(EmplInfo.CallCnt));
              EmplOvercost:= PAnsiChar(AnsiString(EmplInfo.OverCost));

              StringEmplPhones:= '';
              for k:= 0 to EmplInfo.Phones.Count -1 do
              begin
                if not EmplInfo.Phones[k].IsEmpty then
                begin
                  StringEmplPhones:= StringEmplPhones + EmplInfo.Phones[k];
                  PhonesCnt:= PhonesCnt + 1;
                end;
                if k < 2 then
                  if not EmplInfo.Phones[k+1].IsEmpty then
                    StringEmplPhones:= StringEmplPhones + #13#10;
              end;
              EmplPhonesStr:= PAnsiChar(AnsiString(StringEmplPhones));

              Sheet.writeStr(EmplRowToWrite, 0, EmplLastFirstMidName, FCasualText);
              Sheet.writeStr(EmplRowToWrite, 1, EmplPost, FCasualText);
              Sheet.writeStr(EmplRowToWrite, 3, EmplPhonesStr, FCasualText);
              Sheet.writeStr(EmplRowToWrite, 4, EmplMinutesCnt, FCasualText);
              Sheet.writeStr(EmplRowToWrite, 5, EmplCallCnt, FCasualText);
              Sheet.writeStr(EmplRowToWrite, 6, EmplOvercost, FCasualText);
              Sheet.setMerge(EmplRowToWrite, EmplRowToWrite, 1, 2);
              case PhonesCnt of
                0: Sheet.setRow(EmplRowToWrite, 15);
                1: Sheet.setRow(EmplRowToWrite, 15);
                2: Sheet.setRow(EmplRowToWrite, 30);
                3: Sheet.setRow(EmplRowToWrite, 45);
              end;
              EmplRowToWrite:= EmplRowToWrite + 1;
            end;
          end;
          EmplInfo.LastFirstMidName:='';
          EmplInfo.Post:='';
          EmplInfo.Phones.Clear;
          EmplInfo.MinutesCnt:='';
          EmplInfo.CallCnt:='';
          EmplInfo.OverCost:='';
          dm.RTQuery1.Next;
        end;
    end;
  //END: ��� ��� �������� �� ������ � ����� ���������� � ������ ����������, � �������� ���� �������
    inc(Progress, Delta);
    Synchronize(SetProgress);
//END: ������ ������ �� �������
  end;

  Book.delSheet(0); //Delete ServTplSheet
  Book.delSheet(0); //Delete SubDepTplSheet

  RepDateTime:= Now;
  AnsiRepDateTime:= PAnsiChar(AnsiString(FormatDateTime('dd.mm.yy hh_mm_ss', RepDateTime)));
  PathToRep:= PAnsiChar(AnsiString(ExtractFilePath(Application.ExeName) + '������\'+'����� ' + AnsiRepDateTime + '.xls'));
  StrRepDateTime:= PChar(ExtractFilePath(Application.ExeName) + '������\'+'����� ' + String(AnsiRepDateTime) + '.xls');

  try
    Book.save(PathToRep);
    Book.Free;
    Progress:= 100;
    Synchronize(SetProgress);
    Synchronize(FreeVCL);
    ShellExecute(0, 'open', StrRepDateTime, nil, nil, SW_SHOW);
  except
    on E: Exception do
    begin
      ShowMessage('������ ���������� �����. �������� �� ������ ��� ����� ����� ������� "������ ��� ������"?');
      Book.Free;
      Progress:= 100;
      Synchronize(SetProgress);
      Synchronize(FreeVCL);
      exit;
    end;
  end;

end;

procedure TReportGeneratorThread.Execute;
begin
  IdDepList:= TStringList.Create;
  GenerateReport;
end;

end.
