unit addObjectForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls;

type
  TForm3 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    SheetObjectData: TPageControl;
    EmplSheet: TTabSheet;
    Label1: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label20: TLabel;
    Label23: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    EdLastName: TEdit;
    EdFirstName: TEdit;
    EdMidName: TEdit;
    CmbUpperObjType: TComboBox;
    EdMobilePhone: TEdit;
    EdFax: TEdit;
    EdInternalPhone: TEdit;
    EdAddress: TEdit;
    EdRoomNumb: TEdit;
    ServiceSheet: TTabSheet;
    DepartSheet: TTabSheet;
    SubDepatSheet: TTabSheet;
    Label10: TLabel;
    ChkBoxIsMobile: TCheckBox;
    ChkBoxIsFax: TCheckBox;
    Label21: TLabel;
    ChkBoxIsInternal: TCheckBox;
    Label19: TLabel;
    Edit3: TEdit;
    Edit2: TEdit;
    Label14: TLabel;
    Label15: TLabel;
    ComboBox3: TComboBox;
    Label16: TLabel;
    Label11: TLabel;
    Edit1: TEdit;
    Label12: TLabel;
    ComboBox1: TComboBox;
    Edit4: TEdit;
    Label17: TLabel;
    ListBox1: TListBox;
    Label7: TLabel;
    CmbObjectType: TComboBox;
    ListBox2: TListBox;
    Label18: TLabel;
    Edit5: TEdit;
    ListBox3: TListBox;
    Label22: TLabel;
    Edit6: TEdit;
    Label24: TLabel;
    Edit7: TEdit;
    Label13: TLabel;
    Label25: TLabel;
    CheckBox1: TCheckBox;
    Edit8: TEdit;
    Label26: TLabel;
    Edit9: TEdit;
    Memo1: TMemo;
    Memo2: TMemo;
    Memo3: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure ChkBoxIsMobileClick(Sender: TObject);
    procedure ChkBoxIsFaxClick(Sender: TObject);
    procedure ChkBoxIsInternalClick(Sender: TObject);
    procedure CmbObjectTypeChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmbUpperObjTypeChange(Sender: TObject);
    procedure ComboBox3Change(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure Edit6Change(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Edit5Change(Sender: TObject);
    procedure Edit4Change(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure SheetObjectDataChange(Sender: TObject);
    procedure ListBox2Click(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure ListBox3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AddEmpl;
    procedure AddDepart;
    procedure AddSubDep;
    procedure AddService;
    procedure ClearFields;
    procedure ShowFullName(InpListBox: TListBox; InpMemo: TMemo);
  end;

var
  Form3: TForm3;

implementation

{$R *.dfm}

uses main, confirmForm, DataMod;

procedure TForm3.ShowFullName(InpListBox: TListBox; InpMemo: TMemo);
begin
  if InpListBox.ItemIndex>=0 then
  begin
    InpMemo.Lines.Clear;
    InpMemo.Lines[0]:= InpListBox.Items[InpListBox.ItemIndex];
  end;
end;

function TabSheetRefresh: boolean;
begin
  Result:= False;
  with Form3 do
  begin
    case CmbObjectType.ItemIndex of
      0://Service
      begin
        SheetObjectData.ActivePageIndex:=1;
        SheetObjectData.Width:= 393;
        SheetObjectData.Height:= 121;
        Button1.Left:= 302;
        Button1.Top:= 191;
        Button2.Left:= 199;
        Button2.Top:= 191;
        Form3.Height:= 268;
        Form3.Width:= 411;
      end;
      1://Depart
      begin
        SheetObjectData.ActivePageIndex:=2;
        SheetObjectData.Width:= 401;
        SheetObjectData.Height:= 361;
        Button1.Left:= 312;
        Button1.Top:= 431;
        Button2.Left:= 209;
        Button2.Top:= 431;
        Form3.Width:= 432;
        Form3.Height:= 520;
        Form3.ComboBox3Change(Form3);
      end;
      2://SubDepart
      begin
        SheetObjectData.ActivePageIndex:=3;
        SheetObjectData.Width:= 401;
        SheetObjectData.Height:= 361;
        Button1.Left:= 312;
        Button1.Top:= 431;
        Button2.Left:= 209;
        Button2.Top:= 431;
        Form3.Width:= 432;
        Form3.Height:= 520;
        Form3.ComboBox1Change(Form3);
      end;
      3://Empl
      begin
        SheetObjectData.ActivePageIndex:=0;
        SheetObjectData.Height:= 393;
        SheetObjectData.Width:= 777;
        Button1.Left:= 686;
        Button1.Top:= 461;
        Button2.Left:= 583;
        Button2.Top:= 461;
        Form3.Height:= 538;
        Form3.Width:= 798;
        Form3.CmbUpperObjTypeChange(Form3);
      end;
    end;
  end;
  Result:= True;
end;

function FixFormat(InpEdit: TEdit): boolean;
var
  i: integer;
begin
  Result:= True;
  case Form3.CmbObjectType.ItemIndex of
    3:
    begin
      InpEdit.Text:= Trim(InpEdit.Text);
      for i:=0 to (Length(InpEdit.Text) - 1) do
      begin
        if InpEdit.Text[i]=' ' then
        begin
          if InpEdit.Name='EdLastName' then
          begin
            ConfirmForm.Form2.Label1.Caption:= '� ������� ���������� ���������� ������. ������� ���?';
            ConfirmForm.Form2.Label1.Left:= Round(ConfirmForm.Form2.Width/2) - Round(ConfirmForm.Form2.Label1.Width/2);
            ConfirmForm.Form2.ShowModal;
            if confirmForm.Confirmed then
              begin
                InpEdit.Text:= StringReplace(InpEdit.Text,' ','',[rfReplaceAll]);
              end
            else exit;
          end;
          if InpEdit.Name='EdFirstName' then
          begin
            ConfirmForm.Form2.Label1.Caption:= '� ����� ���������� ���������� ������. ������� ���?';
            ConfirmForm.Form2.Label1.Left:= Round(ConfirmForm.Form2.Width/2) - Round(ConfirmForm.Form2.Label1.Width/2);
            ConfirmForm.Form2.ShowModal;
            if confirmForm.Confirmed then
              begin
                InpEdit.Text:= StringReplace(InpEdit.Text,' ','',[rfReplaceAll]);
              end
            else exit;
          end;
          if InpEdit.Name='EdMidName' then
          begin
            ConfirmForm.Form2.Label1.Caption:= '� �������� ���������� ���������� ������. ������� ���?';
            ConfirmForm.Form2.Label1.Left:= Round(ConfirmForm.Form2.Width/2) - Round(ConfirmForm.Form2.Label1.Width/2);
            ConfirmForm.Form2.ShowModal;
            if confirmForm.Confirmed then
              begin
                InpEdit.Text:= StringReplace(InpEdit.Text,' ','',[rfReplaceAll]);
              end
            else exit;
          end;
          if InpEdit.Name='EdAddress' then
          begin
            ConfirmForm.Form2.Label1.Caption:= '�� ������ ����� ����������. ����������?';
            ConfirmForm.Form2.Label1.Left:= Round(ConfirmForm.Form2.Width/2) - Round(ConfirmForm.Form2.Label1.Width/2);
            ConfirmForm.Form2.ShowModal;
            if not confirmForm.Confirmed then exit;
          end;
          if InpEdit.Name='EdRoomNumb' then
          begin
            ConfirmForm.Form2.Label1.Caption:= '�� ������ ����� �������� ����������. ����������?';
            ConfirmForm.Form2.Label1.Left:= Round(ConfirmForm.Form2.Width/2) - Round(ConfirmForm.Form2.Label1.Width/2);
            ConfirmForm.Form2.ShowModal;
            if not confirmForm.Confirmed then exit;
          end;
        end;
      end;
    end;
    2: InpEdit.Text:= Trim(InpEdit.Text);
    1: InpEdit.Text:= Trim(InpEdit.Text);
    0: InpEdit.Text:= Trim(InpEdit.Text);
  end;
end;

procedure TForm3.Button2Click(Sender: TObject);
begin
  ClearFields;
  Form3.Close;
end;

procedure TForm3.CheckBox1Click(Sender: TObject);
begin
  case CheckBox1.Checked of
    True:
    begin
      Edit8.Text:='';
      Edit8.Enabled:=False;
    end;
    False:
    begin
      Edit8.Text:='';
      Edit8.Enabled:=True;
    end;
  end;
end;

procedure TForm3.ChkBoxIsFaxClick(Sender: TObject);
begin
  case ChkBoxIsFax.Checked of
    True:
    begin
      EdFax.Text:='';
      EdFax.Enabled:=False;
    end;
    False:
    begin
      EdFax.Text:='';
      EdFax.Enabled:=True;
    end;
  end;
end;

procedure TForm3.ChkBoxIsInternalClick(Sender: TObject);
begin
  case ChkBoxIsInternal.Checked of
    True:
    begin
      EdInternalPhone.Text:='';
      EdInternalPhone.Enabled:=False;
    end;
    False:
    begin
      EdInternalPhone.Text:='';
      EdInternalPhone.Enabled:=True;
    end;
  end;
end;

procedure TForm3.ChkBoxIsMobileClick(Sender: TObject);
begin
  case ChkBoxIsMobile.Checked of
    True:
    begin
      EdMobilePhone.Text:='';
      EdMobilePhone.Enabled:=False;
    end;
    False:
    begin
      EdMobilePhone.Text:='';
      EdMobilePhone.Enabled:=True;
    end;
  end;
end;

procedure TForm3.CmbObjectTypeChange(Sender: TObject);
begin
  TabSheetRefresh();
end;

procedure TForm3.CmbUpperObjTypeChange(Sender: TObject);
var
  SqlStr, ItemText: String;
begin
  if CmbUpperObjType.ItemIndex>0 then
  begin
    SqlStr:= 'SELECT t1.dep_name, t1.id_dep as `obj_id`, t1.id_par, t2.dep_name as `par_dep_name`';
    SqlStr:= SqlStr + ', t2.id_dep FROM dep_list t1 LEFT JOIN dep_list t2 on t1.id_par=';
    SqlStr:= SqlStr + 't2.id_dep WHERE t1.dep_name like ';
    SqlStr:= SqlStr + QuotedStr('%'+CmbUpperObjType.Items[CmbUpperObjType.ItemIndex]+'%');
    SqlStr:= SqlStr + ' ORDER BY t2.dep_name, t1.dep_name';
  end
  else
  begin
    SqlStr:= 'SELECT t1.dep_name, t1.id_dep as `obj_id`, t1.id_par, t2.dep_name as `par_dep_name`';
    SqlStr:= SqlStr + ', t2.id_dep FROM dep_list t1 LEFT JOIN dep_list t2 on t1.id_par=';
    SqlStr:= SqlStr + 't2.id_dep WHERE t1.id_dep_group=' + QuotedStr('������');
    SqlStr:= SqlStr + ' ORDER BY t2.dep_name, t1.dep_name';
  end;
  if dm.DoQuery(dm.RTQuery, SqlStr) then
  begin
    ListBox1.Clear;
    while not dm.RTQuery.Eof do
    begin
      ItemText:= dm.RTQuery.FieldByName('par_dep_name').AsString + '->' + dm.RTQuery.FieldByName('dep_name').AsString;
      ListBox1.AddItem(ItemText, TObject(dm.RTQuery.FieldByName('obj_id').AsInteger));
      dm.RTQuery.Next;
    end;
  end;
end;

procedure TForm3.ComboBox1Change(Sender: TObject);
var
  SqlStr, ItemText: String;
begin
  if ComboBox1.ItemIndex>0 then
  begin
    SqlStr:= 'SELECT t1.dep_name, t1.id_dep as `obj_id`, t1.id_par, t2.dep_name as `par_dep_name`';
    SqlStr:= SqlStr + ', t2.id_dep FROM dep_list t1 LEFT JOIN dep_list t2 on t1.id_par=';
    SqlStr:= SqlStr + 't2.id_dep WHERE t1.dep_name like ';
    SqlStr:= SqlStr + QuotedStr('%'+ComboBox1.Items[ComboBox1.ItemIndex]+'%');
    SqlStr:= SqlStr + ' and t1.id_dep_group <> ' + QuotedStr('���������') + ' ORDER BY t2.dep_name, t1.dep_name';
  end
  else
  begin
    SqlStr:= 'SELECT t1.dep_name, t1.id_dep as `obj_id`, t1.id_par, t2.dep_name as `par_dep_name`';
    SqlStr:= SqlStr + ', t2.id_dep FROM dep_list t1 LEFT JOIN dep_list t2 on t1.id_par=';
    SqlStr:= SqlStr + 't2.id_dep WHERE t1.id_dep_group=' + QuotedStr('������');
    SqlStr:= SqlStr + ' ORDER BY t2.dep_name, t1.dep_name';
  end;
  if dm.DoQuery(dm.RTQuery, SqlStr) then
  begin
    ListBox3.Clear;
    while not dm.RTQuery.Eof do
    begin
      ItemText:= dm.RTQuery.FieldByName('par_dep_name').AsString + '->' + dm.RTQuery.FieldByName('dep_name').AsString;
      ListBox3.AddItem(ItemText, TObject(dm.RTQuery.FieldByName('obj_id').AsInteger));
      dm.RTQuery.Next;
    end;
  end;
end;

procedure TForm3.ComboBox3Change(Sender: TObject);
var
  SqlStr, ItemText: String;
begin
  if ComboBox3.ItemIndex>0 then
  begin
    SqlStr:= 'SELECT t1.dep_name, t1.id_dep as `obj_id`, t1.id_par, t2.dep_name as `par_dep_name`';
    SqlStr:= SqlStr + ', t2.id_dep FROM dep_list t1 LEFT JOIN dep_list t2 on t1.id_par=';
    SqlStr:= SqlStr + 't2.id_dep WHERE t1.dep_name like ';
    SqlStr:= SqlStr + QuotedStr('%'+ComboBox3.Items[ComboBox3.ItemIndex]+'%');
    SqlStr:= SqlStr + ' and t1.id_dep_group <> ' + QuotedStr('���������') + ' ORDER BY t2.dep_name, t1.dep_name';
  end
  else
  begin
    SqlStr:= 'SELECT t1.dep_name, t1.id_dep as `obj_id`, t1.id_par, t2.dep_name as `par_dep_name`';
    SqlStr:= SqlStr + ', t2.id_dep FROM dep_list t1 LEFT JOIN dep_list t2 on t1.id_par=';
    SqlStr:= SqlStr + 't2.id_dep WHERE t1.id_dep_group=' + QuotedStr('������');
    SqlStr:= SqlStr + ' ORDER BY t2.dep_name, t1.dep_name';
  end;
  if dm.DoQuery(dm.RTQuery, SqlStr) then
  begin
    ListBox2.Clear;
    while not dm.RTQuery.Eof do
    begin
      ItemText:= dm.RTQuery.FieldByName('par_dep_name').AsString + '->' + dm.RTQuery.FieldByName('dep_name').AsString;
      ListBox2.AddItem(ItemText, TObject(dm.RTQuery.FieldByName('obj_id').AsInteger));
      dm.RTQuery.Next;
    end;
  end;
end;

procedure TForm3.Edit4Change(Sender: TObject);
var
  SqlStr, ItemText: String;
begin
  if Length(Edit4.Text) > 0 then
  begin
    SqlStr:= 'SELECT t1.dep_name, t1.id_dep as `obj_id`, t1.id_par, t2.dep_name as `par_dep_name`';
    SqlStr:= SqlStr + ', t2.id_dep FROM dep_list t1 LEFT JOIN dep_list t2 on t1.id_par=';
    SqlStr:= SqlStr + 't2.id_dep WHERE t1.dep_name like ';
    SqlStr:= SqlStr + QuotedStr('%'+Edit4.Text+'%') + ' and t1.id_dep_group=';
    SqlStr:= SqlStr + QuotedStr(CmbUpperObjType.Items[CmbUpperObjType.ItemIndex]);
    SqlStr:= SqlStr + ' ORDER BY t2.dep_name, t1.dep_name';
    if dm.DoQuery(dm.RTQuery, SqlStr) then
    begin
      ListBox1.Clear;
      while not dm.RTQuery.Eof do
      begin
        ItemText:= dm.RTQuery.FieldByName('par_dep_name').AsString + '->' + dm.RTQuery.FieldByName('dep_name').AsString;
        ListBox1.AddItem(ItemText, TObject(dm.RTQuery.FieldByName('obj_id').AsInteger));
        dm.RTQuery.Next;
      end;
    end;
  end
  else
  begin
    SqlStr:= 'SELECT t1.dep_name, t1.id_dep as `obj_id`, t1.id_par, t2.dep_name as `par_dep_name`';
    SqlStr:= SqlStr + ', t2.id_dep FROM dep_list t1 LEFT JOIN dep_list t2 on t1.id_par=';
    SqlStr:= SqlStr + 't2.id_dep WHERE t1.id_dep_group=' + QuotedStr(CmbUpperObjType.Items[CmbUpperObjType.ItemIndex]);
    SqlStr:= SqlStr + ' ORDER BY t2.dep_name, t1.dep_name';
    if dm.DoQuery(dm.RTQuery, SqlStr) then
    begin
      ListBox1.Clear;
      while not dm.RTQuery.Eof do
      begin
        ItemText:= dm.RTQuery.FieldByName('par_dep_name').AsString + '->' + dm.RTQuery.FieldByName('dep_name').AsString;
        ListBox1.AddItem(ItemText, TObject(dm.RTQuery.FieldByName('obj_id').AsInteger));
        dm.RTQuery.Next;
      end;
    end;
  end;
end;

procedure TForm3.Edit5Change(Sender: TObject);
var
  SqlStr, ItemText: String;
begin
  if Length(Edit5.Text) > 0 then
  begin
    SqlStr:= 'SELECT t1.dep_name, t1.id_dep as `obj_id`, t1.id_par, t2.dep_name as `par_dep_name`';
    SqlStr:= SqlStr + ', t2.id_dep FROM dep_list t1 LEFT JOIN dep_list t2 on t1.id_par=';
    SqlStr:= SqlStr + 't2.id_dep WHERE t1.dep_name like ';
    SqlStr:= SqlStr + QuotedStr('%'+Edit5.Text+'%') + ' and t1.id_dep_group=';
    SqlStr:= SqlStr + QuotedStr(ComboBox3.Items[ComboBox3.ItemIndex]);
    SqlStr:= SqlStr + ' ORDER BY t2.dep_name, t1.dep_name';
    if dm.DoQuery(dm.RTQuery, SqlStr) then
    begin
      ListBox2.Clear;
      while not dm.RTQuery.Eof do
      begin
        ItemText:= dm.RTQuery.FieldByName('par_dep_name').AsString + '->' + dm.RTQuery.FieldByName('dep_name').AsString;
        ListBox2.AddItem(ItemText, TObject(dm.RTQuery.FieldByName('obj_id').AsInteger));
        dm.RTQuery.Next;
      end;
    end;
  end
  else
  begin
    SqlStr:= 'SELECT t1.dep_name, t1.id_dep as `obj_id`, t1.id_par, t2.dep_name as `par_dep_name`';
    SqlStr:= SqlStr + ', t2.id_dep FROM dep_list t1 LEFT JOIN dep_list t2 on t1.id_par=';
    SqlStr:= SqlStr + 't2.id_dep WHERE t1.id_dep_group=' + QuotedStr(ComboBox3.Items[ComboBox3.ItemIndex]);
    SqlStr:= SqlStr + ' ORDER BY t2.dep_name, t1.dep_name';
    if dm.DoQuery(dm.RTQuery, SqlStr) then
    begin
      ListBox2.Clear;
      while not dm.RTQuery.Eof do
      begin
        ItemText:= dm.RTQuery.FieldByName('par_dep_name').AsString + '->' + dm.RTQuery.FieldByName('dep_name').AsString;
        ListBox2.AddItem(ItemText, TObject(dm.RTQuery.FieldByName('obj_id').AsInteger));
        dm.RTQuery.Next;
      end;
    end;
  end;
end;

procedure TForm3.Edit6Change(Sender: TObject);
var
  SqlStr, ItemText: String;
begin
  if Length(Edit6.Text) > 0 then
  begin
    SqlStr:= 'SELECT t1.dep_name, t1.id_dep as `obj_id`, t1.id_par, t2.dep_name as `par_dep_name`';
    SqlStr:= SqlStr + ', t2.id_dep FROM dep_list t1 LEFT JOIN dep_list t2 on t1.id_par=';
    SqlStr:= SqlStr + 't2.id_dep WHERE t1.dep_name like ';
    SqlStr:= SqlStr + QuotedStr('%'+Edit6.Text+'%') + ' and t1.id_dep_group=';
    SqlStr:= SqlStr + QuotedStr(ComboBox1.Items[ComboBox1.ItemIndex]);
    SqlStr:= SqlStr + ' and t1.id_dep_group <> ' + QuotedStr('���������') + ' ORDER BY t2.dep_name, t1.dep_name';
    if dm.DoQuery(dm.RTQuery, SqlStr) then
    begin
      ListBox3.Clear;
      while not dm.RTQuery.Eof do
      begin
        ItemText:= dm.RTQuery.FieldByName('par_dep_name').AsString + '->' + dm.RTQuery.FieldByName('dep_name').AsString;
        ListBox3.AddItem(ItemText, TObject(dm.RTQuery.FieldByName('obj_id').AsInteger));
        dm.RTQuery.Next;
      end;
    end;
  end
  else
  begin
    SqlStr:= 'SELECT t1.dep_name, t1.id_dep as `obj_id`, t1.id_par, t2.dep_name as `par_dep_name`';
    SqlStr:= SqlStr + ', t2.id_dep FROM dep_list t1 LEFT JOIN dep_list t2 on t1.id_par=';
    SqlStr:= SqlStr + 't2.id_dep WHERE t1.id_dep_group=' + QuotedStr(ComboBox1.Items[ComboBox1.ItemIndex]);
    SqlStr:= SqlStr + ' ORDER BY t2.dep_name, t1.dep_name';
    if dm.DoQuery(dm.RTQuery, SqlStr) then
    begin
      ListBox3.Clear;
      while not dm.RTQuery.Eof do
      begin
        ItemText:= dm.RTQuery.FieldByName('par_dep_name').AsString + '->' + dm.RTQuery.FieldByName('dep_name').AsString;
        ListBox3.AddItem(ItemText, TObject(dm.RTQuery.FieldByName('obj_id').AsInteger));
        dm.RTQuery.Next;
      end;
    end;
  end;
end;

procedure TForm3.AddEmpl;
var
  CustomFieldsDef, CustomFieldsData, id_dep, SqlStr: String;
begin
/////////////////////////////////BEGIN: Check & fix input data
  if FixFormat(Form3.EdLastName) then
    if FixFormat(Form3.EdFirstName) then
      if FixFormat(Form3.EdMidName) then
  if FixFormat(EdAddress) then
    if FixFormat(EdRoomNumb) then
      begin
        if (Form3.Listbox1.ItemIndex = -1) then begin ShowMessage('���������� ����������. '+#13#10+'�� ������� �������� ��� �� ������ ����������� ������'); Exit; end;
        if (Form3.EdMobilePhone.Text='') and (Form3.ChkBoxIsMobile.Checked = False) then begin ShowMessage('���������� ����������. '+#13#10+'�� ������ ����� ���������� ��������. '+#13#10+'���������� ������� ����� ���������� �������� '+#13#10+'��� ���������� ������ "�����������" � �������� ���������'); Exit; end;
        if (Form3.Edit8.Text='') and (Form3.CheckBox1.Checked = False) then begin ShowMessage('���������� ����������. '+#13#10+'�� ������ ����� ����. '+#13#10+'���������� ������� ����� ���� '+#13#10+'��� ���������� ������ "�����������" � �������� ���������'); Exit; end;
        if (Form3.EdFax.Text='') and (Form3.ChkBoxIsFax.Checked = False) then begin ShowMessage('���������� ����������. '+#13#10+'�� ������ ����� �����. '+#13#10+'���������� ������� ����� ����� '+#13#10+' ��� ���������� ������ "�����������" � �������� ���������'); Exit; end;
        if (Form3.EdInternalPhone.Text='') and (Form3.ChkBoxIsInternal.Checked = False) then begin ShowMessage('���������� ����������. '+#13#10+'�� ������ ���������� ����� ��������. '+#13#10+'���������� ������� ���������� ����� �������� '+#13#10+'��� ���������� ������ "�����������" � �������� ���������'); Exit; end;
        if (Form3.Edit7.Text='') then begin ShowMessage('���������� ����������. '+#13#10+'�� ������� ��������� ����������.'); Exit; end;
      end;
/////////////////////////////////END: Check & fix input data

/////////////////////////////////BEGIN: Generating template of custom fields
  CustomFieldsDef:='';
  CustomFieldsData:='';

  if Form3.ChkBoxIsMobile.Checked=False then
  begin
    CustomFieldsDef:=CustomFieldsDef+', empl_phone';
    CustomFieldsData:=CustomFieldsData+QuotedStr(Form3.EdMobilePhone.Text)+', ';
  end;

  if Form3.CheckBox1.Checked=False then
  begin
    CustomFieldsDef:=CustomFieldsDef+', empl_city_te_number';
    CustomFieldsData:=CustomFieldsData+QuotedStr(Form3.Edit8.Text)+', ';
  end;

  if Form3.ChkBoxIsFax.Checked=False then
  begin
    CustomFieldsDef:=CustomFieldsDef+', empl_fax';
    CustomFieldsData:=CustomFieldsData+QuotedStr(Form3.EdFax.Text)+', ';
  end;

  if Form3.ChkBoxIsInternal.Checked=False then
  begin
    CustomFieldsDef:=CustomFieldsDef+', empl_int_phone';
    CustomFieldsData:=CustomFieldsData+QuotedStr(Form3.EdInternalPhone.Text)+', ';
  end;
/////////////////////////////////END: Generating template of custom fields

  id_dep:= IntToStr(Integer(Form3.ListBox1.Items.Objects[Form3.ListBox1.ItemIndex]));
  SqlStr:= 'insert into empl_list (id_dep, empl_post, empl_fname, empl_lfname, empl_lname';
  SqlStr:= SqlStr + CustomFieldsDef+', empl_addr, empl_room)'+' values ('+id_dep;
  SqlStr:= SqlStr + ', '+QuotedStr(Form3.Edit7.Text)+ ', '+QuotedStr(Form3.EdFirstName.Text)+', ';
  SqlStr:= SqlStr + QuotedStr(Form3.EdMidName.Text)+', '+QuotedStr(Form3.EdLastName.Text);
  SqlStr:= SqlStr + ', '+CustomFieldsData+QuotedStr(Form3.EdAddress.Text)+', ';
  SqlStr:= SqlStr + QuotedStr(Form3.EdRoomNumb.Text)+')';
  if dm.DoEditQuery(dm.RTQuery,SqlStr) then
  begin
    ShowMessage('������ ��������� �������');
    dm.RefreshNodes(Form1.DepTree);
    Form1.ComboBox1.ItemIndex:= Form3.CmbObjectType.ItemIndex;
    Form1.ComboBox1Change(Form3);
    ClearFields;
    dm.RefreshQuery(dm.PBGridQuery, 'empl_mode');
    Form3.Close;
  end;
end;

procedure TForm3.AddSubDep;
var
  id_par, SqlStr: String;
begin
/////////////////////////////////BEGIN: Check input data
  if (Form3.Listbox3.ItemIndex = -1) then begin ShowMessage('���������� ����������. '+#13#10+'�� ������� �������� ��� �� ������ ����������� ������'); Exit; end;
  if (Form3.Edit1.Text='') then begin ShowMessage('���������� ����������. '+#13#10+'�� ������� �������� ���������.'); Exit; end;
/////////////////////////////////END: Check input data

  id_par:= IntToStr(Integer(Form3.ListBox3.Items.Objects[Form3.ListBox3.ItemIndex]));
  SqlStr:='insert into dep_list (id_dep_group, dep_name, id_par) values ('+QuotedStr('���������')+', '+QuotedStr(Edit1.Text)+', '+QuotedStr(id_par)+')';
  if dm.DoEditQuery(dm.RTQuery,SqlStr) then
  begin
    ShowMessage('������ ��������� �������');
    dm.RefreshNodes(Form1.DepTree);
    Form1.ComboBox1.ItemIndex:= Form3.CmbObjectType.ItemIndex;
    Form1.ComboBox1Change(Form3);
    ClearFields;
    dm.RefreshQuery(dm.PBGridQuery, 'DepList_mode');
    Form3.Close;
  end;
end;

procedure TForm3.AddDepart;
var
  id_par, SqlStr: String;
begin
/////////////////////////////////BEGIN: Check input data
  if (Form3.Listbox2.ItemIndex = -1) then begin ShowMessage('���������� ����������. '+#13#10+'�� ������� �������� ��� �� ������ ����������� ������'); Exit; end;
  if Form3.Edit2.Text='' then begin ShowMessage('���������� ����������. '+#13#10+'�� ������� �������� ������.'); Exit; end;
/////////////////////////////////END: Check input data

  id_par:= IntToStr(Integer(Form3.ListBox2.Items.Objects[Form3.ListBox2.ItemIndex]));
  SqlStr:='insert into dep_list (id_dep_group, dep_name, id_par)'+' values ('+QuotedStr('�����')+', '+QuotedStr(Edit2.Text)+', '+QuotedStr(id_par)+')';
  if dm.DoEditQuery(dm.RTQuery,SqlStr) then
  begin
    ShowMessage('������ ��������� �������');
    dm.RefreshNodes(Form1.DepTree);
    Form1.ComboBox1.ItemIndex:= Form3.CmbObjectType.ItemIndex;
    Form1.ComboBox1Change(Form3);
    ClearFields;
    dm.RefreshQuery(dm.PBGridQuery, 'DepList_mode');
    Form3.Close;
  end;
end;

procedure TForm3.AddService;
var
  SqlStr: String;
begin
/////////////////////////////////BEGIN: Check input data
  if Form3.Edit3.Text='' then begin ShowMessage('���������� ����������. '+#13#10+'�� ������� �������� ������.'); Exit; end;
  if Form3.Edit9.Text='' then begin ShowMessage('���������� ����������. '+#13#10+'�� ������� ����������� �������� ������.'); Exit; end;
  if Length(String(Form3.Edit9.Text)) > 10 then begin ShowMessage('���������� ����������. '+#13#10+'C���������� �������� ������ �� ������ ��������� 10 ��������'); Exit; end;
/////////////////////////////////END: Check input data
  SqlStr:= 'SELECT * FROM dep_list WHERE dep_name_short=' + QuotedStr(Edit9.Text);
  dm.DoQuery(dm.RTQuery, SqlStr);
  if not dm.RTQuery.IsEmpty then begin ShowMessage('���������� ����������. '+#13#10+'����� ����������� �������� ��� ���������� � ������ ������.'); Exit; end;

  SqlStr:='INSERT INTO dep_list (id_dep_group, dep_name, id_par, dep_name_short)';
  SqlStr:= SqlStr + ' values ('+QuotedStr('������')+', ' + QuotedStr(Edit3.Text);
  SqlStr:= SqlStr + ', 0, ' + QuotedStr(Edit9.Text) + ')';
  if dm.DoEditQuery(dm.RTQuery, SqlStr) then
  begin
    dm.RefreshNodes(Form1.DepTree);
    Form1.ComboBox1.ItemIndex:= Form3.CmbObjectType.ItemIndex;
    Form1.ComboBox1Change(Form3);
    ClearFields;
    dm.RefreshQuery(dm.PBGridQuery, 'ServList_mode');
    ShowMessage('������ ��������� �������');
    Form3.Close;
  end;
end;

procedure TForm3.ClearFields;
begin
  //Service
   Edit3.Clear;
   Edit9.Clear;
  //Service

  //Dep
  Edit2.Clear;
  Edit5.Clear;
  ListBox2.Items.Clear;
  Memo1.Lines.Clear;
  //Dep

  //SubDep
  Edit1.Clear;
  Edit6.Clear;
  ListBox3.Items.Clear;
  Memo3.Lines.Clear;
  //SubDep

  //Empl
  Edit8.Clear;
  EdLastName.Clear;
  EdFirstName.Clear;
  EdMidName.Clear;
  EdMobilePhone.Clear;
  EdFax.Clear;
  EdInternalPhone.Clear;
  EdAddress.Clear;
  EdRoomNumb.Clear;
  Edit9.Clear;
  Edit4.Clear;
  ListBox1.Items.Clear;
  Memo2.Lines.Clear;
  //Empl
end;

procedure TForm3.Button1Click(Sender: TObject);
begin
  case CmbObjectType.ItemIndex of
  0:
    begin
      AddService;
    end;
  1:
    begin
      AddDepart;
    end;
  2:
    begin
      AddSubDep;
    end;
  3:
    begin
      AddEmpl;
    end;
  end;
  dm.RefreshDepPath;
end;

procedure TForm3.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Form1.Show;
end;

procedure TForm3.FormShow(Sender: TObject);
begin
  CmbObjectType.ItemIndex:= Form1.ComboBox1.ItemIndex;
  TabSheetRefresh();
end;

procedure TForm3.ListBox1Click(Sender: TObject);
begin
  ShowFullName(Form3.ListBox1, Form3.Memo2);
end;

procedure TForm3.ListBox2Click(Sender: TObject);
begin
  ShowFullName(Form3.ListBox2, Form3.Memo1);
end;

procedure TForm3.ListBox3Click(Sender: TObject);
begin
  ShowFullName(Form3.ListBox3, Form3.Memo3);
end;

procedure TForm3.SheetObjectDataChange(Sender: TObject);
begin
  ShowMessage('yes');
end;

end.


