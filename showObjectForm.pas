unit showObjectForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls,
  Vcl.WinXCtrls;

type
  TForm4 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    SheetObjectData: TPageControl;
    EmplSheet: TTabSheet;
    Label1: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label20: TLabel;
    Label23: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    Label21: TLabel;
    Label17: TLabel;
    EdLastName: TEdit;
    EdFirstName: TEdit;
    EdMidName: TEdit;
    CmbUpperObjType: TComboBox;
    EdMobilePhone: TEdit;
    EdFax: TEdit;
    EdInternalPhone: TEdit;
    EdAddress: TEdit;
    EdRoomNumb: TEdit;
    ChkBoxIsMobile: TCheckBox;
    ChkBoxIsFax: TCheckBox;
    ChkBoxIsInternal: TCheckBox;
    Edit4: TEdit;
    ListBox1: TListBox;
    ServiceSheet: TTabSheet;
    Label19: TLabel;
    Edit3: TEdit;
    DepartSheet: TTabSheet;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label18: TLabel;
    Edit2: TEdit;
    ComboBox3: TComboBox;
    ListBox2: TListBox;
    Edit5: TEdit;
    SubDepatSheet: TTabSheet;
    Label11: TLabel;
    Label12: TLabel;
    Label22: TLabel;
    Label24: TLabel;
    Edit1: TEdit;
    ComboBox1: TComboBox;
    ListBox3: TListBox;
    Edit6: TEdit;
    CmbObjectType: TComboBox;
    Button3: TButton;
    Switch1: TToggleSwitch;
    Label7: TLabel;
    Label13: TLabel;
    Edit8: TEdit;
    Edit9: TEdit;
    CheckBox1: TCheckBox;
    Label25: TLabel;
    Edit10: TEdit;
    Label26: TLabel;
    Memo3: TMemo;
    Memo1: TMemo;
    Memo2: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure ChkBoxIsMobileClick(Sender: TObject);
    procedure ChkBoxIsFaxClick(Sender: TObject);
    procedure ChkBoxIsInternalClick(Sender: TObject);
    procedure CmbObjectTypeChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Edit4Change(Sender: TObject);
    procedure CmbUpperObjTypeChange(Sender: TObject);
    procedure ComboBox3Change(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure Edit6Change(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Edit5Change(Sender: TObject);
    procedure CmbUpperObjTypeClick(Sender: TObject);
    procedure Switch1Click(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure ListBox2Click(Sender: TObject);
    procedure ListBox3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure UpdateEmpl;
    procedure UpdateDepart;
    procedure UpdateSubDep;
    procedure UpdateService;
    procedure LoadObjectData(ObjId: integer; ObjType: integer);
    procedure ChangeMode(ModeType: String);
    procedure ClearFields;
    procedure BackupData(id: String; ObjType: String);
    procedure RemoveOldData;
    function GetEmplIdList(IdDepList: String): String;
  end;

var
  Form4: TForm4;
  OldShortName: String;

implementation

{$R *.dfm}

uses main, confirmForm, DataMod, addObjectForm;

procedure TForm4.Button2Click(Sender: TObject);
begin
  ClearFields;
  Form4.Close;
end;

function TForm4.GetEmplIdList(IdDepList: String): String;
var
  SqlStr: String;
begin
  Result:= '';
  SqlStr:= 'SELECT id_empl FROM empl_list WHERE id_dep in (' + IdDepList + ')';
  if dm.DoQuery(dm.RTQuery, SqlStr) then
  while not dm.RTQuery.Eof do
  begin
    Result:= Result + ',' + dm.RTQuery.FieldByName('id_empl').AsString;
    dm.RTQuery.Next;
  end;
  Delete(Result, 1, 1);
end;

procedure TForm4.RemoveOldData;
var
  SqlStr, TimePoint: String;
  i: Integer;
begin
  SqlStr:= 'SELECT time_point FROM time_points';
  dm.DoQuery(dm.RTQuery, SqlStr);
  if dm.RTQuery.RecordCount > 20 then
    for i:=1 to (dm.RTQuery.RecordCount - 20) do
    begin
      SqlStr:= 'SELECT MIN(time_point) as `time_point` FROM time_points';
      dm.DoQuery(dm.RTQuery, SqlStr);

      TimePoint:= dm.RTQuery.FieldByName('time_point').AsString;
      TimePoint:= FormatDateTime('yyyy-mm-dd hh:mm:ss', StrToDateTime(TimePoint));

      SqlStr:= 'DELETE FROM time_points WHERE';
      SqlStr:= SqlStr + ' time_point='+QuotedStr(TimePoint);
      dm.DoEditQuery(dm.RTQuery, SqlStr);
      SqlStr:= 'DELETE FROM call_data_bkp WHERE';
      SqlStr:= SqlStr + ' date_time='+QuotedStr(TimePoint);
      dm.DoEditQuery(dm.RTQuery, SqlStr);
      SqlStr:= 'DELETE FROM empl_list_bkp WHERE';
      SqlStr:= SqlStr + ' date_time='+QuotedStr(TimePoint);
      dm.DoEditQuery(dm.RTQuery, SqlStr);
      SqlStr:= 'DELETE FROM dep_list_bkp WHERE';
      SqlStr:= SqlStr + ' date_time='+QuotedStr(TimePoint);
      dm.DoEditQuery(dm.RTQuery, SqlStr);
    end;
end;

procedure TForm4.BackupData(id: String; ObjType: String);
var
  SqlStr, IdDepStr, DateTimeStr, IdEmplStr: String;
  DateTime: TDateTime;
begin
  RemoveOldData;
  IdDepStr:= '';
  IdEmplStr:= '';
  DateTime:= Now;
  DateTimeStr:= FormatDateTime('yyyy-mm-dd hh:mm:ss', DateTime);
  if ObjType.Equals('Dep') then
  begin
    //BEGIN: ���� ��� ������/�����/��������� ��������� ������ � ������������ ���������
    SqlStr:= 'SET @a = ' + id;
    dm.DoEditQuery(dm.RTQuery, SqlStr);
    SqlStr:= 'SELECT * FROM dep_list WHERE id_dep in (SELECT id FROM (';
    SqlStr:= SqlStr + 'SELECT @index:=INSTR(@a,' + QuotedStr(',') + ') as coma_index,';
    SqlStr:= SqlStr + ' @id:=SUBSTRING_INDEX(@a, ' + QuotedStr(',') + ', 1)  as id,';
    SqlStr:= SqlStr + ' @b:=SUBSTRING(@a,IF(@index,@index+1,0)),';
    SqlStr:= SqlStr + ' @a:=CONCAT_WS(IF(LENGTH(@b)>0,' + QuotedStr(',') + ',' + QuotedStr('') + '),';
    SqlStr:= SqlStr + ' @b,(SELECT group_concat(id_dep) FROM dep_list WHERE id_par=@id)) as array';
    SqlStr:= SqlStr + ' FROM dep_list WHERE LENGTH(@a)>0) as tt1)';
    if dm.DoQuery(dm.RTQuery, SqlStr) then
      while not dm.RTQuery.Eof do
      begin
        IdDepStr:= IdDepStr + ',' + dm.RTQuery.FieldByName('id_dep').AsString;
        dm.RTQuery.Next;
      end;
    Delete(IdDepStr, 1, 1);

    SqlStr:= 'SELECT id_empl FROM empl_list WHERE id_dep in (' + IdDepStr + ')';
    dm.DoQuery(dm.RTQuery, SqlStr);
    while not dm.RTQuery.Eof do
    begin
      IdEmplStr:= IdEmplStr + ',' + dm.RTQuery.FieldByName('id_empl').AsString;
      dm.RTQuery.Next;
    end;
    Delete(IdEmplStr, 1, 1);
    //END: ���� ��� ������/�����/��������� ��������� ������ � ������������
    //BEGIN: ����� ������ � ��������� �������, ��������� ����, �����
    SqlStr:= 'INSERT INTO dep_list_bkp ';
    SqlStr:= SqlStr + '(id_dep,	id_dep_group, dep_name, dep_name_short, dep_path,';
    SqlStr:= SqlStr + ' id_par, date_time)';
    SqlStr:= SqlStr + ' SELECT id_dep, id_dep_group, dep_name,';
    SqlStr:= SqlStr + ' dep_name_short, dep_path, id_par,';
    SqlStr:= SqlStr + QuotedStr(DateTimeStr) + ' FROM dep_list WHERE dep_list.id_dep in ('+IdDepStr+')';
    SqlStr:= SqlStr + ' ON DUPLICATE KEY UPDATE dep_list_bkp.id_dep=dep_list.id_dep';
    dm.DoEditQuery(dm.RTQuery, SqlStr);

    SqlStr:= 'INSERT INTO empl_list_bkp ';
    SqlStr:= SqlStr + '(id_empl,id_dep,empl_post,empl_fname,empl_lfname,empl_lname,';
    SqlStr:= SqlStr + 'empl_phone,empl_fax,empl_int_phone,empl_city_te_number,';
    SqlStr:= SqlStr + 'empl_addr,empl_room,date_time)';
    SqlStr:= SqlStr + ' SELECT id_empl,id_dep,empl_post,empl_fname,empl_lfname,';
    SqlStr:= SqlStr + 'empl_lname,empl_phone,empl_fax,empl_int_phone,empl_city_te_number,';
    SqlStr:= SqlStr + 'empl_addr,empl_room,' + QuotedStr(DateTimeStr) + ' FROM empl_list ';
    SqlStr:= SqlStr + 'WHERE empl_list.id_dep in('+IdDepStr+')';
    SqlStr:= SqlStr + ' ON DUPLICATE KEY UPDATE empl_list_bkp.id_empl=empl_list.id_empl';
    dm.DoEditQuery(dm.RTQuery, SqlStr);

    SqlStr:= 'INSERT INTO call_data_bkp';
    SqlStr:= SqlStr + ' (id_item,id_empl,item_date,item_time,item_amount,item_cost,date_time)';
    SqlStr:= SqlStr + ' SELECT id_item,id_empl,item_date,item_time,item_amount,item_cost,';
    SqlStr:= SqlStr + QuotedStr(DateTimeStr) + ' FROM call_data WHERE call_data.id_empl in ('+IdEmplStr+')';
    SqlStr:= SqlStr + ' ON DUPLICATE KEY UPDATE call_data_bkp.id_empl=call_data.id_empl';
    dm.DoEditQuery(dm.RTQuery, SqlStr);

    SqlStr:= 'INSERT INTO time_points (time_point) VALUES (' + QuotedStr(DateTimeStr) + ')';
    dm.DoEditQuery(dm.RTQuery, SqlStr);
    //END: ����� ������ � ��������� �������, ��������� ����, �����
  end;
  if ObjType.Equals('Empl') then
  begin
    SqlStr:= 'INSERT INTO empl_list_bkp ';
    SqlStr:= SqlStr + '(id_empl,id_dep,empl_post,empl_fname,empl_lfname,empl_lname,';
    SqlStr:= SqlStr + 'empl_phone,empl_fax,empl_int_phone,empl_city_te_number,';
    SqlStr:= SqlStr + 'empl_addr,empl_room,date_time)';
    SqlStr:= SqlStr + ' SELECT id_empl,id_dep,empl_post,empl_fname,empl_lfname,';
    SqlStr:= SqlStr + 'empl_lname,empl_phone,empl_fax,empl_int_phone,empl_city_te_number,';
    SqlStr:= SqlStr + 'empl_addr,empl_room,' + QuotedStr(DateTimeStr) + ' FROM empl_list ';
    SqlStr:= SqlStr + 'WHERE empl_list.id_empl='+Id;
    SqlStr:= SqlStr + ' ON DUPLICATE KEY UPDATE empl_list_bkp.id_empl=empl_list.id_empl';
    dm.DoEditQuery(dm.RTQuery, SqlStr);

    SqlStr:= 'INSERT INTO call_data_bkp';
    SqlStr:= SqlStr + ' (id_item,id_empl,item_date,item_time,item_amount,item_cost,date_time)';
    SqlStr:= SqlStr + ' SELECT id_item,id_empl,item_date,item_time,item_amount,item_cost,';
    SqlStr:= SqlStr + QuotedStr(DateTimeStr) + ' FROM call_data WHERE call_data.id_empl='+Id;
    SqlStr:= SqlStr + ' ON DUPLICATE KEY UPDATE call_data_bkp.id_empl=call_data.id_empl';
    dm.DoEditQuery(dm.RTQuery, SqlStr);
  end;
end;

procedure TForm4.Button3Click(Sender: TObject);
var
  SqlStr, IdString, id_empl, id_dep, EmplIdString: String;
begin
  Form1.ListToDel.Items.Clear;
  case CmbObjectType.ItemIndex of
  0:
    begin
      ConfirmForm.Form2.Label1.Caption:= '�� ������������� ������ ������� ������? ��� �����ͨ���� �������, ���������� � �� ������� ����� �������';
      ConfirmForm.Form2.Width:= 800;
      ConfirmForm.Form2.Label1.Left:= Round(ConfirmForm.Form2.Width/2) - Round(ConfirmForm.Form2.Label1.Width/2);
      ConfirmForm.Form2.ShowModal;
      if ConfirmForm.confirmed then
      begin
        id_dep:= Form1.GridPhoneBook.DataSource.DataSet.FieldByName('id_dep').AsString;
        BackupData(id_dep, 'Dep');
        IdString:= Form1.GetChilds(StrToInt(id_dep));
        EmplIdString:= GetEmplIdList(IdString);
        if not IdString.IsEmpty then
        begin
          dm.DoQuery(dm.RTQuery, 'SELECT * FROM empl_list WHERE id_dep in (' + IdString + ')');
          if dm.RTQuery.IsEmpty then
          begin
            SqlStr:= 'DELETE FROM dep_list WHERE';
            SqlStr:= SqlStr + ' dep_list.id_dep in (' + IdString + ')';
          end
          else
          begin
            SqlStr:= 'SELECT * FROM call_data WHERE id_empl in (' + EmplIdString + ')';
            if dm.DoQuery(dm.RTQuery, SqlStr) then
            begin
              SqlStr:= 'DELETE empl_list, dep_list, call_data FROM empl_list, dep_list, call_data WHERE';
              SqlStr:= SqlStr + ' empl_list.id_dep in (' + IdString + ') and';
              SqlStr:= SqlStr + ' dep_list.id_dep in (' + IdString + ') and';
              SqlStr:= SqlStr + ' call_data.id_empl in (' + EmplIdString + ')';
            end
            else
            begin
              SqlStr:= 'DELETE empl_list, dep_list FROM empl_list, dep_list WHERE';
              SqlStr:= SqlStr + ' empl_list.id_dep in (' + IdString + ') and';
              SqlStr:= SqlStr + ' dep_list.id_dep in (' + IdString + ')';
            end;
          end;
          dm.DoEditQuery(dm.RTQuery, SqlStr);
          dm.RefreshQuery(dm.PBGridQuery, 'ServList_mode');
          dm.RefreshNodes(Form1.DepTree);
        end;
      end;
    end;
  1:
    begin
      ConfirmForm.Form2.Label1.Caption:= '�� ������������� ������ ������� �����? ��� �����ͨ���� �������, ���������� � �� ������� ����� �������';
      ConfirmForm.Form2.Width:= 800;
      ConfirmForm.Form2.Label1.Left:= Round(ConfirmForm.Form2.Width/2) - Round(ConfirmForm.Form2.Label1.Width/2);
      ConfirmForm.Form2.ShowModal;
      if ConfirmForm.confirmed then
        begin
        id_dep:= Form1.GridPhoneBook.DataSource.DataSet.FieldByName('id_dep').AsString;
        BackupData(id_dep, 'Dep');
        IdString:= Form1.GetChilds(StrToInt(id_dep));
        EmplIdString:= GetEmplIdList(IdString);
        if not IdString.IsEmpty then
        begin
          dm.DoQuery(dm.RTQuery, 'SELECT id_empl FROM empl_list WHERE id_dep in (' + IdString + ')');
          if dm.RTQuery.IsEmpty then
          begin
            SqlStr:= 'DELETE FROM dep_list WHERE';
            SqlStr:= SqlStr + ' dep_list.id_dep in (' + IdString + ')';
          end
          else
          begin
            SqlStr:= 'SELECT id_empl FROM call_data WHERE id_empl in (' + EmplIdString + ')';
            if dm.DoQuery(dm.RTQuery, SqlStr) then
            begin
              SqlStr:= 'DELETE empl_list, dep_list, call_data FROM empl_list, dep_list, call_data WHERE';
              SqlStr:= SqlStr + ' empl_list.id_dep in (' + IdString + ') and';
              SqlStr:= SqlStr + ' dep_list.id_dep in (' + IdString + ') and';
              SqlStr:= SqlStr + ' call_data.id_empl in (' + EmplIdString + ')';
            end
            else
            begin
              SqlStr:= 'DELETE empl_list, dep_list FROM empl_list, dep_list WHERE';
              SqlStr:= SqlStr + ' empl_list.id_dep in (' + IdString + ') and';
              SqlStr:= SqlStr + ' dep_list.id_dep in (' + IdString + ')';
            end;
          end;
          dm.DoEditQuery(dm.RTQuery, SqlStr);
          dm.RefreshQuery(dm.PBGridQuery, 'DepList_mode');
          dm.RefreshNodes(Form1.DepTree);
        end;
      end;
    end;
  2:
    begin
      ConfirmForm.Form2.Label1.Caption:= '�� ������������� ������ ������� ���������? ��� �����ͨ���� �������, ���������� � �� ������� ����� �������';
      ConfirmForm.Form2.Width:= 800;
      ConfirmForm.Form2.Label1.Left:= Round(ConfirmForm.Form2.Width/2) - Round(ConfirmForm.Form2.Label1.Width/2);
      ConfirmForm.Form2.ShowModal;
      if ConfirmForm.confirmed then
      begin
        id_dep:= Form1.GridPhoneBook.DataSource.DataSet.FieldByName('id_dep').AsString;
        BackupData(id_dep, 'Dep');
        IdString:= Form1.GetChilds(StrToInt(id_dep));
        EmplIdString:= GetEmplIdList(IdString);
        if not IdString.IsEmpty then
        begin
          dm.DoQuery(dm.RTQuery, 'SELECT id_empl FROM empl_list WHERE id_dep in (' + IdString + ')');
          if dm.RTQuery.IsEmpty then
          begin
            SqlStr:= 'DELETE FROM dep_list WHERE';
            SqlStr:= SqlStr + ' dep_list.id_dep in (' + IdString + ')';
          end
          else
          begin
            SqlStr:= 'SELECT id_empl FROM call_data WHERE id_empl in (' + EmplIdString + ')';
            if dm.DoQuery(dm.RTQuery, SqlStr) then
            begin
              SqlStr:= 'DELETE empl_list, dep_list, call_data FROM empl_list, dep_list, call_data WHERE';
              SqlStr:= SqlStr + ' empl_list.id_dep in (' + IdString + ') and';
              SqlStr:= SqlStr + ' dep_list.id_dep in (' + IdString + ') and';
              SqlStr:= SqlStr + ' call_data.id_empl in (' + EmplIdString + ')';
            end
            else
            begin
              SqlStr:= 'DELETE empl_list, dep_list FROM empl_list, dep_list WHERE';
              SqlStr:= SqlStr + ' empl_list.id_dep in (' + IdString + ') and';
              SqlStr:= SqlStr + ' dep_list.id_dep in (' + IdString + ')';
            end;
          end;
          dm.DoEditQuery(dm.RTQuery, SqlStr);
          dm.RefreshQuery(dm.PBGridQuery, 'DepList_mode');
          dm.RefreshNodes(Form1.DepTree);
        end;
      end;
    end;
  3:
    begin
      ConfirmForm.Form2.Label1.Caption:= '�� ������������� ������ ������� ����������? ��� ��� ������� ����� �������';
      ConfirmForm.Form2.Width:= 800;
      ConfirmForm.Form2.Label1.Left:= Round(ConfirmForm.Form2.Width/2) - Round(ConfirmForm.Form2.Label1.Width/2);
      ConfirmForm.Form2.ShowModal;
      if ConfirmForm.confirmed then
      begin
        id_empl:= Form1.GridPhoneBook.DataSource.DataSet.FieldByName('id_empl').AsString;
        BackupData(id_empl, 'Empl');
        SqlStr:= 'DELETE FROM empl_list WHERE id_empl=' + id_empl;
        dm.DoEditQuery(dm.RTQuery, SqlStr);
        SqlStr:= 'DELETE FROM call_data WHERE id_empl=' + id_empl;
        dm.DoEditQuery(dm.RTQuery, SqlStr);
        dm.RefreshQuery(dm.PBGridQuery, 'empl_mode');
      end;
    end;
  end;
  ClearFields;
  dm.RefreshDepPath;
  Form1.Button4Click(Form4);
  Form4.Close;
end;

procedure TForm4.ChkBoxIsFaxClick(Sender: TObject);
begin
  case ChkBoxIsFax.Checked of
    True:
    begin
      EdFax.Text:='';
      EdFax.Enabled:=False;
    end;
    False:
    begin
      EdFax.Text:='';
      EdFax.Enabled:=True;
    end;
  end;
end;

procedure TForm4.ChangeMode(ModeType: String);
begin
  if ModeType.Equals('View') then
  begin
    case CmbObjectType.ItemIndex of
    0:
      begin
        Edit3.ReadOnly:= True;
        Edit10.ReadOnly:= True;
      end;
    1:
      begin
        Edit2.ReadOnly:= True;
        Edit5.ReadOnly:= True;
        ListBox2.Enabled:= False;
      end;
    2:
      begin
        Edit1.ReadOnly:= True;
        Edit6.ReadOnly:= True;
        ListBox3.Enabled:= False;
      end;
    3:
      begin
        Edit4.ReadOnly:= True;
        EdMobilePhone.ReadOnly:= True;
        EdFax.ReadOnly:= True;
        EdInternalPhone.ReadOnly:= True;
        EdAddress.ReadOnly:= True;
        EdRoomNumb.ReadOnly:= True;
        EdLastName.ReadOnly:= True;
        Edit8.ReadOnly:= True;
        Edit9.ReadOnly:= True;
        EdFirstName.ReadOnly:= True;
        EdMidName.ReadOnly:= True;
        ListBox1.Enabled:= False;
      end;
    end;
  end;

  if ModeType.Equals('Edit') then
  begin
    case CmbObjectType.ItemIndex of
    0:
      begin
        Edit3.ReadOnly:= False;
        Edit10.ReadOnly:= False;
      end;
    1:
      begin
        Edit2.ReadOnly:= False;
        Edit5.ReadOnly:= False;
        ListBox2.Enabled:= True;
        Edit5Change(Form4);
        if ListBox2.Items.Count > 0 then
          ListBox2.ItemIndex:= 0;
      end;
    2:
      begin
        Edit1.ReadOnly:= False;
        Edit6.ReadOnly:= False;
        ListBox3.Enabled:= True;
        Edit6Change(Form4);
        if ListBox3.Items.Count > 0 then
          ListBox3.ItemIndex:= 0;
      end;
    3:
      begin
        Edit4.ReadOnly:= False;
        EdMobilePhone.ReadOnly:= False;
        EdFax.ReadOnly:= False;
        EdInternalPhone.ReadOnly:= False;
        EdAddress.ReadOnly:= False;
        EdRoomNumb.ReadOnly:= False;
        EdLastName.ReadOnly:= False;
        Edit8.ReadOnly:= False;
        Edit9.ReadOnly:= False;
        EdFirstName.ReadOnly:= False;
        EdMidName.ReadOnly:= False;
        ListBox1.Enabled:= True;
        Edit4Change(Form4);
        if ListBox1.Items.Count > 0 then
          ListBox1.ItemIndex:= 0;
      end;
    end;
  end;
end;

procedure TForm4.CheckBox1Click(Sender: TObject);
begin
  case CheckBox1.Checked of
    True:
    begin
      Edit9.Text:='';
      Edit9.Enabled:=False;
    end;
    False:
    begin
      Edit9.Text:='';
      Edit9.Enabled:=True;
    end;
  end;
end;

procedure TForm4.ClearFields;
begin
  //Service
   Edit3.Clear;
   Edit10.Clear;
  //Service

  //Dep
  ComboBox1.ItemIndex:= 0;
  Edit2.Clear;
  Edit5.Clear;
  ListBox2.Items.Clear;
  //Dep

  //SubDep
  ComboBox1.ItemIndex:= 2;
  Edit1.Clear;
  Edit6.Clear;
  ListBox3.Items.Clear;
  //SubDep

  //Empl
  CmbUpperObjType.ItemIndex:= 2;
  Edit8.Clear;
  EdLastName.Clear;
  EdFirstName.Clear;
  EdMidName.Clear;
  EdMobilePhone.Clear;
  EdFax.Clear;
  EdInternalPhone.Clear;
  EdAddress.Clear;
  EdRoomNumb.Clear;
  Edit9.Clear;
  Edit4.Clear;
  ListBox1.Items.Clear;
  //Empl
end;

procedure TForm4.ListBox1Click(Sender: TObject);
begin
  Form3.ShowFullName(Form4.ListBox1, Form4.Memo1);
end;

procedure TForm4.ListBox2Click(Sender: TObject);
begin
  Form3.ShowFullName(Form4.ListBox2, Form4.Memo3);
end;

procedure TForm4.ListBox3Click(Sender: TObject);
begin
  Form3.ShowFullName(Form4.ListBox3, Form4.Memo2);
end;

procedure TForm4.LoadObjectData(ObjId: integer; ObjType: integer);
var
  SqlStr: String;
  UpperObjType, UpperObj, EmplPost, LName, FName, MidName, Mobile, Fax, Internal, city_TE, Addr, Room: String;
begin
  case ObjType of
  0:
  begin
    SqlStr:= 'SELECT dep_name, dep_name_short FROM dep_list WHERE id_dep=' + IntToStr(ObjId);
    if dm.DoQuery(dm.RTQuery, SqlStr) then
    begin
      Edit3.Text:= dm.RTQuery.FieldByName('dep_name').AsString;
      Edit10.Text:= dm.RTQuery.FieldByName('dep_name_short').AsString;
      OldShortName:= String(Edit10.Text);
    end;
  end;
  1:
  begin
    SqlStr:= 'SELECT t2.dep_name as `ParDepName`, t2.id_dep_group, t1.dep_name';
    SqlStr:= SqlStr + ' as `DepName`, t1.id_dep FROM dep_list t1 left join ';
    SqlStr:= SqlStr + 'dep_list t2 on t1.id_par=t2.id_dep WHERE t1.id_dep=';
    SqlStr:= SqlStr + IntToStr(ObjId);
    if dm.DoQuery(dm.RTQuery, SqlStr) then
    begin
      Edit2.Text:= dm.RTQuery.FieldByName('DepName').AsString;
      Edit5.Text:= dm.RTQuery.FieldByName('ParDepName').AsString;
      ComboBox3.ItemIndex:= ComboBox3.Items.IndexOf(dm.RTQuery.FieldByName('id_dep_group').AsString);
    end;
  end;
  2:
  begin
    SqlStr:= 'SELECT t2.dep_name as `ParDepName`, t2.id_dep_group, t1.dep_name';
    SqlStr:= SqlStr + ' as `DepName`, t1.id_dep FROM dep_list t1 left join ';
    SqlStr:= SqlStr + 'dep_list t2 on t1.id_par=t2.id_dep WHERE t1.id_dep=';
    SqlStr:= SqlStr + IntToStr(ObjId);
    if dm.DoQuery(dm.RTQuery, SqlStr) then
    begin
      Edit1.Text:= dm.RTQuery.FieldByName('DepName').AsString;
      Edit6.Text:= dm.RTQuery.FieldByName('ParDepName').AsString;
      ComboBox1.ItemIndex:= ComboBox1.Items.IndexOf(dm.RTQuery.FieldByName('id_dep_group').AsString);
    end;
  end;
  3:
  begin
    SqlStr:= 'SELECT t2.dep_name, t1.empl_post, t1.empl_fname, t1.empl_lfname, t1.empl_lname, t1.empl_phone';
    SqlStr:= SqlStr + ', t1.empl_fax, t1.empl_int_phone, t1.empl_city_te_number,';
    SqlStr:= SqlStr + ' t1.empl_addr, t1.empl_room, t2.id_dep,';
    SqlStr:= SqlStr + ' t2.id_dep_group, t1.id_empl FROM empl_list t1 left join';
    SqlStr:= SqlStr + ' dep_list t2 on t1.id_dep=t2.id_dep WHERE t1.id_empl=';
    SqlStr:= SqlStr + IntToStr(ObjId);
    if dm.DoQuery(dm.RTQuery, SqlStr) then
    begin
      EmplPost:= dm.RTQuery.FieldByName('empl_post').AsString;
      Edit8.Text:= EmplPost;

      LName:= dm.RTQuery.FieldByName('empl_lname').AsString;
      EdLastName.Text:= LName;

      FName:= dm.RTQuery.FieldByName('empl_fname').AsString;
      EdFirstName.Text:= FName;

      MidName:= dm.RTQuery.FieldByName('empl_lfname').AsString;
      EdMidName.Text:= MidName;

      Mobile:= dm.RTQuery.FieldByName('empl_phone').AsString;

      Fax:= dm.RTQuery.FieldByName('empl_fax').AsString;

      Internal:= dm.RTQuery.FieldByName('empl_int_phone').AsString;

      city_TE:= dm.RTQuery.FieldByName('empl_city_te_number').AsString;

      Addr:= dm.RTQuery.FieldByName('empl_addr').AsString;

      Room:= dm.RTQuery.FieldByName('empl_room').AsString;

      if Length(Mobile)<2 then
      begin
        ChkBoxIsMobile.Checked:= True;
      end
      else
      begin
        ChkBoxIsMobile.Checked:= False;
        EdMobilePhone.Text:= Mobile;
      end;

      if Length(Fax)<2 then
      begin
        ChkBoxIsFax.Checked:= True;
      end
      else
      begin
        ChkBoxIsFax.Checked:= False;
        EdFax.Text:= Fax;
      end;

      if Length(Internal)<2 then
      begin
        ChkBoxIsInternal.Checked:= True;
      end
      else
      begin
        ChkBoxIsInternal.Checked:= False;
        EdInternalPhone.Text:= Internal;
      end;

      if Length(city_TE)<2 then
      begin
        CheckBox1.Checked:= True;
      end
      else
      begin
        CheckBox1.Checked:= False;
        Edit9.Text:= Internal;
      end;

      EdAddress.Text:= Addr;
      EdRoomNumb.Text:= Room;

      UpperObj:= dm.RTQuery.FieldByName('dep_name').AsString;
      Edit4.Text:= UpperObj;

      UpperObjType:= dm.RTQuery.FieldByName('id_dep_group').AsString;
      CmbUpperObjType.ItemIndex:= CmbUpperObjType.Items.IndexOf(UpperObjType);
    end;
  end;
  end;
end;

procedure TForm4.Switch1Click(Sender: TObject);
begin
  if Switch1.State = tsson then
  begin
    ChangeMode('Edit');
  end
  else
    ChangeMode('View');
end;

function TabSheetRefresh: boolean;
begin
  Result:= False;
  with Form4 do
  begin
    case CmbObjectType.ItemIndex of
      0://Service
      begin
        SheetObjectData.ActivePageIndex:=1;
        SheetObjectData.Width:= 397;
        SheetObjectData.Height:= 111;
        Button1.Left:= 311;
        Button1.Top:= SheetObjectData.Height + 10;
        Button3.Left:= 208;
        Button3.Top:= Button1.Top;
        Button2.Left:= Button3.Left - 103;
        Button2.Top:= Button1.Top;
        Width:= 426;
      end;
      1://Depart
      begin
        SheetObjectData.ActivePageIndex:=2;
        SheetObjectData.Width:= 401;
        SheetObjectData.Height:= 361;
        Button1.Left:= 311;
        Button1.Top:= 371;
        Button3.Left:= 208;
        Button3.Top:= 371;
        Button2.Left:= Button3.Left - 103;
        Button2.Top:= Button1.Top;
        Width:= 432;
        Height:= 520;
      end;
      2://SubDepart
      begin
        SheetObjectData.ActivePageIndex:=3;
        SheetObjectData.Width:= 401;
        SheetObjectData.Height:= 361;
        Button1.Left:= 311;
        Button1.Top:= 371;
        Button3.Left:= 208;
        Button3.Top:= 371;
        Button2.Left:= Button3.Left - 103;
        Button2.Top:= Button1.Top;
        Width:= 432;
        Height:= 520;
      end;
      3://Empl
      begin
        SheetObjectData.ActivePageIndex:=0;
        SheetObjectData.Height:= 393;
        SheetObjectData.Width:= 777;
        Button1.Left:= 686;
        Button1.Top:= 404;
        Button3.Left:= 583;
        Button3.Top:= 404;
        Button2.Left:= Button3.Left - 103;
        Button2.Top:= Button1.Top;
        Height:= 490;
        Width:= 807;
      end;
    end;
    Height:= Button1.Top + 88;
    Switch1.Top:= Button2.Top + 20;
    Switch1.Left:= SheetObjectData.Left + 2;
    Label7.Top:= Switch1.Top - 21;
    Label7.Left:= Switch1.Left;
  end;
  Result:= True;
end;

function FixFormat(InpEdit: TEdit): boolean;
var
  i: integer;
begin
  Result:= True;
  case Form4.CmbObjectType.ItemIndex of
    3:
    begin
      InpEdit.Text:= Trim(InpEdit.Text);
      for i:=0 to (Length(InpEdit.Text) - 1) do
      begin
        if InpEdit.Text[i]=' ' then
        begin
          if InpEdit.Name='EdLastName' then
          begin
            ConfirmForm.Form2.Label1.Caption:= '� ������� ���������� ���������� ������. ������� ���?';
            ConfirmForm.Form2.Label1.Left:= Round(ConfirmForm.Form2.Width/2) - Round(ConfirmForm.Form2.Label1.Width/2);
            ConfirmForm.Form2.ShowModal;
            if confirmForm.Confirmed then
              begin
                InpEdit.Text:= StringReplace(InpEdit.Text,' ','',[rfReplaceAll]);
              end
            else exit;
          end;
          if InpEdit.Name='EdFirstName' then
          begin
            ConfirmForm.Form2.Label1.Caption:= '� ����� ���������� ���������� ������. ������� ���?';
            ConfirmForm.Form2.Label1.Left:= Round(ConfirmForm.Form2.Width/2) - Round(ConfirmForm.Form2.Label1.Width/2);
            ConfirmForm.Form2.ShowModal;
            if confirmForm.Confirmed then
              begin
                InpEdit.Text:= StringReplace(InpEdit.Text,' ','',[rfReplaceAll]);
              end
            else exit;
          end;
          if InpEdit.Name='EdMidName' then
          begin
            ConfirmForm.Form2.Label1.Caption:= '� �������� ���������� ���������� ������. ������� ���?';
            ConfirmForm.Form2.Label1.Left:= Round(ConfirmForm.Form2.Width/2) - Round(ConfirmForm.Form2.Label1.Width/2);
            ConfirmForm.Form2.ShowModal;
            if confirmForm.Confirmed then
              begin
                InpEdit.Text:= StringReplace(InpEdit.Text,' ','',[rfReplaceAll]);
              end
            else exit;
          end;
        end;
      end;
      if InpEdit.Name='EdAddress' then
        if String(InpEdit.Text).IsEmpty then
          begin
            ConfirmForm.Form2.Label1.Caption:= '�� ������ ����� ����������. ����������?';
            ConfirmForm.Form2.Label1.Left:= Round(ConfirmForm.Form2.Width/2) - Round(ConfirmForm.Form2.Label1.Width/2);
            ConfirmForm.Form2.ShowModal;
            if confirmForm.Confirmed then
              begin
                InpEdit.Text:= StringReplace(InpEdit.Text,' ','',[rfReplaceAll]);
              end
            else exit;
          end;
      if InpEdit.Name='EdRoomNumb' then
        if String(InpEdit.Text).IsEmpty then
          begin
            ConfirmForm.Form2.Label1.Caption:= '�� ������ ����� �������� ����������. ����������?';
            ConfirmForm.Form2.Label1.Left:= Round(ConfirmForm.Form2.Width/2) - Round(ConfirmForm.Form2.Label1.Width/2);
            ConfirmForm.Form2.ShowModal;
            if confirmForm.Confirmed then
              begin
                InpEdit.Text:= StringReplace(InpEdit.Text,' ','',[rfReplaceAll]);
              end
            else exit;
          end;
    end;
    2: InpEdit.Text:= Trim(InpEdit.Text);
    1: InpEdit.Text:= Trim(InpEdit.Text);
    0: InpEdit.Text:= Trim(InpEdit.Text);
  end;
end;

procedure TForm4.UpdateEmpl;
var
  CustomFieldsDef, CustomFieldsData, id_dep, SqlStr: String;
begin
/////////////////////////////////BEGIN: Check & fix input data
  if FixFormat(Form4.EdLastName) then
    if FixFormat(Form4.EdFirstName) then
      if FixFormat(Form4.EdMidName) then
  if FixFormat(EdAddress) then
    if FixFormat(EdRoomNumb) then
      begin
        if (Form4.Listbox1.ItemIndex = -1) then begin ShowMessage('��������� ����������. '+#13#10+'�� ������� �������� ��� �� ������ ����������� ������'); Exit; end;
        if (Form4.EdMobilePhone.Text='') and (Form4.ChkBoxIsMobile.Checked = False) then begin ShowMessage('��������� ����������. '+#13#10+'�� ������ ����� ���������� ��������. '+#13#10+'���������� ������� ����� ���������� �������� '+#13#10+'��� ���������� ������ "�����������" � �������� ���������'); Exit; end;
        if (Form4.EdFax.Text='') and (Form4.ChkBoxIsFax.Checked = False) then begin ShowMessage('��������� ����������. '+#13#10+'�� ������ ����� �����. '+#13#10+'���������� ������� ����� ����� '+#13#10+' ��� ���������� ������ "�����������" � �������� ���������'); Exit; end;
        if (Form4.EdInternalPhone.Text='') and (Form4.ChkBoxIsInternal.Checked = False) then begin ShowMessage('��������� ����������. '+#13#10+'�� ������ ���������� ����� ��������. '+#13#10+'���������� ������� ���������� ����� �������� '+#13#10+'��� ���������� ������ "�����������" � �������� ���������'); Exit; end;
        if (Form4.Edit9.Text='') and (Form4.CheckBox1.Checked = False) then begin ShowMessage('��������� ����������. '+#13#10+'�� ������ ���������� ����� ����. '+#13#10+'���������� ������� ���������� ����� ���� '+#13#10+'��� ���������� ������ "�����������" � �������� ���������'); Exit; end;
      end;
/////////////////////////////////END: Check & fix input data

/////////////////////////////////BEGIN: Generating template of custom fields
  CustomFieldsDef:='';
  CustomFieldsData:='';

  if Form4.ChkBoxIsMobile.Checked=False then
  begin
    CustomFieldsData:= CustomFieldsData + ', empl_phone=';
    CustomFieldsData:= CustomFieldsData + QuotedStr(Form4.EdMobilePhone.Text);
  end;

  if Form4.ChkBoxIsFax.Checked=False then
  begin
    CustomFieldsData:= CustomFieldsData + ', empl_fax=';
    CustomFieldsData:= CustomFieldsData + QuotedStr(Form4.EdFax.Text);
  end;

  if Form4.ChkBoxIsInternal.Checked=False then
  begin
    CustomFieldsData:= CustomFieldsData + ', empl_int_phone=';
    CustomFieldsData:= CustomFieldsData + QuotedStr(Form4.EdInternalPhone.Text);
  end;

  if Form4.CheckBox1.Checked=False then
  begin
    CustomFieldsData:= CustomFieldsData + ', empl_city_te_number=';
    CustomFieldsData:= CustomFieldsData + QuotedStr(Form4.Edit9.Text);
  end;

/////////////////////////////////END: Generating template of custom fields
  BackupData(Form1.GridPhoneBook.DataSource.DataSet.FieldByName('id_empl').AsString, 'Empl');
  id_dep:= IntToStr(Integer(Form4.ListBox1.Items.Objects[Form4.ListBox1.ItemIndex]));
  SqlStr:= 'UPDATE empl_list SET id_dep=' + QuotedStr(id_dep) + ', empl_post=';
  SqlStr:= SqlStr + QuotedStr(Edit8.Text) + ', empl_fname=' + QuotedStr(EdFirstName.Text);
  SqlStr:= SqlStr + ', empl_lfname=' + QuotedStr(EdMidName.Text);
  SqlStr:= SqlStr + ', empl_lname=' + QuotedStr(EdLastName.Text) + CustomFieldsData;
  SqlStr:= SqlStr + ', empl_addr=' + QuotedStr(EdAddress.Text) + ', empl_room=';
  SqlStr:= SqlStr + QuotedStr(EdRoomNumb.Text) + ' WHERE id_empl=';
  SqlStr:= SqlStr + Form1.GridPhoneBook.DataSource.DataSet.FieldByName('id_empl').AsString;
  if dm.DoEditQuery(dm.RTQuery,SqlStr) then
  begin
    ShowMessage('������ ��������');
    dm.RefreshQuery(dm.PBGridQuery, 'empl_mode');
    dm.RefreshNodes(Form1.DepTree);
    Form4.Close;
  end;
end;

procedure TForm4.UpdateSubDep;
var
  id_par, SqlStr, id_dep_group: String;
begin
/////////////////////////////////BEGIN: Check input data
  if (Form4.Listbox3.ItemIndex = -1) then begin ShowMessage('��������� ����������. '+#13#10+'�� ������� �������� ��� �� ������ ����������� ������'); Exit; end;
  if Form4.Edit1.Text='' then begin ShowMessage('��������� ����������. '+#13#10+'�� ������� �������� ���������.'); Exit; end;
/////////////////////////////////END: Check input data
  BackupData(Form1.GridPhoneBook.DataSource.DataSet.FieldByName('id_dep').AsString, 'Dep');
  id_par:= IntToStr(Integer(Form4.ListBox3.Items.Objects[Form4.ListBox3.ItemIndex]));
  id_dep_group:= ComboBox1.Items[ComboBox1.ItemIndex];
  SqlStr:='UPDATE dep_list SET dep_name=' + QuotedStr(Edit1.Text)+ ', id_par=';
  SqlStr:= SqlStr + QuotedStr(id_par) + ' WHERE id_dep=';
  SqlStr:= SqlStr + Form1.GridPhoneBook.DataSource.DataSet.FieldByName('id_dep').AsString;
  if dm.DoEditQuery(dm.RTQuery,SqlStr) then
  begin
    ShowMessage('������ ��������');
    dm.RefreshQuery(dm.PBGridQuery, 'DepList_mode');
    dm.RefreshNodes(Form1.DepTree);
    Form4.Close;
  end;
end;

procedure TForm4.UpdateDepart;
var
  id_par, SqlStr, id_dep_group: String;
begin
/////////////////////////////////BEGIN: Check input data
  if (Form4.Listbox2.ItemIndex = -1) then begin ShowMessage('��������� ����������. '+#13#10+'�� ������� �������� ��� �� ������ ����������� ������'); Exit; end;
  if Form4.Edit2.Text='' then begin ShowMessage('��������� ����������. '+#13#10+'�� ������� �������� ������.'); Exit; end;
/////////////////////////////////END: Check input data
  id_par:= IntToStr(Integer(Form4.ListBox2.Items.Objects[Form4.ListBox2.ItemIndex]));
  id_dep_group:= ComboBox3.Items[ComboBox3.ItemIndex];
  BackupData(Form1.GridPhoneBook.DataSource.DataSet.FieldByName('id_dep').AsString, 'Dep');
  SqlStr:='UPDATE dep_list SET dep_name=' + QuotedStr(Edit2.Text)+ ', id_par=';
  SqlStr:= SqlStr + QuotedStr(id_par) + ' WHERE id_dep=';
  SqlStr:= SqlStr + Form1.GridPhoneBook.DataSource.DataSet.FieldByName('id_dep').AsString;
  if dm.DoEditQuery(dm.RTQuery,SqlStr) then
  begin
    ShowMessage('������ ��������');
    dm.RefreshQuery(dm.PBGridQuery, 'DepList_mode');
    dm.RefreshNodes(Form1.DepTree);
    Form4.Close;
  end;
end;

procedure TForm4.UpdateService;
var
  SqlStr: String;
begin
/////////////////////////////////BEGIN: Check input data
  if Form4.Edit3.Text='' then begin ShowMessage('�� ������� �������� ������.'); Exit; end;
  if Form4.Edit10.Text='' then begin ShowMessage('�� ������� ����������� �������� ������.'); Exit; end;
  SqlStr:= 'SELECT dep_name_short FROM dep_list WHERE dep_name_short=' + QuotedStr(Edit10.Text);
  dm.DoQuery(dm.RTQuery, SqlStr);
  if not dm.RTQuery.IsEmpty then
  begin
    if not dm.RTQuery.FieldByName('dep_name_short').AsString.Equals(OldShortName) then
    begin
      ShowMessage('����� ����������� �������� ��� ���������� � ������ ������.');
      Exit;
    end;
  end;
/////////////////////////////////END: Check input data
  BackupData(Form1.GridPhoneBook.DataSource.DataSet.FieldByName('id_dep').AsString, 'Dep');
  SqlStr:='UPDATE dep_list SET dep_name=' + QuotedStr(Edit3.Text);
  SqlStr:= SqlStr + ', dep_name_short=' + QuotedStr(Edit10.Text);
  SqlStr:= SqlStr + 'WHERE id_dep=' + Form1.GridPhoneBook.DataSource.DataSet.FieldByName('id_dep').AsString;
  if dm.DoEditQuery(dm.RTQuery, SqlStr) then
  begin
    ShowMessage('������ ��������');
    dm.RefreshQuery(dm.PBGridQuery, '');
    dm.RefreshNodes(Form1.DepTree);
    Form4.Close;
  end;
end;

procedure TForm4.Button1Click(Sender: TObject);
begin
  if Switch1.State = tssoff then
  begin
    ShowMessage('����������, �������� ����� ��������������');
    exit;
  end;
  case CmbObjectType.ItemIndex of
  0:
  begin
    UpdateService;
  end;
  1:
  begin
    UpdateDepart;
  end;
  2:
  begin
    UpdateSubDep;
  end;
  3:
  begin
    UpdateEmpl;
  end;
  end;
  ClearFields;
  dm.RefreshDepPath;
end;

procedure TForm4.ChkBoxIsInternalClick(Sender: TObject);
begin
  case ChkBoxIsInternal.Checked of
    True:
    begin
      EdInternalPhone.Text:='';
      EdInternalPhone.Enabled:=False;
    end;
    False:
    begin
      EdInternalPhone.Text:='';
      EdInternalPhone.Enabled:=True;
    end;
  end;
end;

procedure TForm4.ChkBoxIsMobileClick(Sender: TObject);
begin
  case ChkBoxIsMobile.Checked of
    True:
    begin
      EdMobilePhone.Text:='';
      EdMobilePhone.Enabled:=False;
    end;
    False:
    begin
      EdMobilePhone.Text:='';
      EdMobilePhone.Enabled:=True;
    end;
  end;
end;

procedure TForm4.CmbObjectTypeChange(Sender: TObject);
begin
  TabSheetRefresh();
end;

procedure TForm4.CmbUpperObjTypeChange(Sender: TObject);
var
  SqlStr, ItemText: String;
begin
  if CmbUpperObjType.ItemIndex>0 then
  begin
    SqlStr:= 'SELECT t1.dep_name, t1.id_dep as `obj_id`, t1.id_par, t2.dep_name as `par_dep_name`';
    SqlStr:= SqlStr + ', t2.id_dep FROM dep_list t1 LEFT JOIN dep_list t2 on t1.id_par=';
    SqlStr:= SqlStr + 't2.id_dep WHERE t1.dep_name like ';
    SqlStr:= SqlStr +QuotedStr('%'+CmbUpperObjType.Items[CmbUpperObjType.ItemIndex]+'%');
  end
  else
  begin
    SqlStr:= 'SELECT t1.dep_name, t1.id_dep as `obj_id`, t1.id_par, t2.dep_name as `par_dep_name`';
    SqlStr:= SqlStr + ', t2.id_dep FROM dep_list t1 LEFT JOIN dep_list t2 on t1.id_par=';
    SqlStr:= SqlStr + 't2.id_dep WHERE t1.id_dep_group=' + QuotedStr('������');
  end;
  if dm.DoQuery(dm.RTQuery, SqlStr) then
  begin
    ListBox1.Clear;
    while not dm.RTQuery.Eof do
    begin
      ItemText:= dm.RTQuery.FieldByName('par_dep_name').AsString + '->' + dm.RTQuery.FieldByName('dep_name').AsString;
      ListBox1.AddItem(ItemText, TObject(dm.RTQuery.FieldByName('obj_id').AsInteger));
      dm.RTQuery.Next;
    end;
  end;
end;


procedure TForm4.CmbUpperObjTypeClick(Sender: TObject);
begin
  CmbUpperObjType.Perform(CB_SHOWDROPDOWN, 0, 0);
end;

procedure TForm4.ComboBox1Change(Sender: TObject);
var
  SqlStr, ItemText: String;
begin
  if ComboBox1.ItemIndex>0 then
  begin
    SqlStr:= 'SELECT t1.dep_name, t1.id_dep as `obj_id`, t1.id_par, t2.dep_name as `par_dep_name`';
    SqlStr:= SqlStr + ', t2.id_dep FROM dep_list t1 LEFT JOIN dep_list t2 on t1.id_par=';
    SqlStr:= SqlStr + 't2.id_dep WHERE t1.dep_name like ';
    SqlStr:= SqlStr +QuotedStr('%'+ComboBox1.Items[ComboBox1.ItemIndex]+'%');
    SqlStr:= SqlStr + ' and t1.id_dep_group <> ' + QuotedStr('���������');
  end
  else
  begin
    SqlStr:= 'SELECT t1.dep_name, t1.id_dep as `obj_id`, t1.id_par, t2.dep_name as `par_dep_name`';
    SqlStr:= SqlStr + ', t2.id_dep FROM dep_list t1 LEFT JOIN dep_list t2 on t1.id_par=';
    SqlStr:= SqlStr + 't2.id_dep WHERE t1.id_dep_group=' + QuotedStr('������');
  end;
  if dm.DoQuery(dm.RTQuery, SqlStr) then
  begin
    ListBox3.Clear;
    while not dm.RTQuery.Eof do
    begin
      ItemText:= dm.RTQuery.FieldByName('par_dep_name').AsString + '->' + dm.RTQuery.FieldByName('dep_name').AsString;
      ListBox3.AddItem(ItemText, TObject(dm.RTQuery.FieldByName('obj_id').AsInteger));
      dm.RTQuery.Next;
    end;
  end;
end;

procedure TForm4.ComboBox3Change(Sender: TObject);
var
  SqlStr, ItemText: String;
begin
  if ComboBox3.ItemIndex>0 then
  begin
    SqlStr:= 'SELECT t1.dep_name, t1.id_dep as `obj_id`, t1.id_par, t2.dep_name as `par_dep_name`';
    SqlStr:= SqlStr + ', t2.id_dep FROM dep_list t1 LEFT JOIN dep_list t2 on t1.id_par=';
    SqlStr:= SqlStr + 't2.id_dep WHERE t1.dep_name like ';
    SqlStr:= SqlStr + QuotedStr('%'+ComboBox3.Items[ComboBox3.ItemIndex]+'%');
    SqlStr:= SqlStr + ' and t1.id_dep_group <> ' + QuotedStr('���������');
  end
  else
  begin
    SqlStr:= 'SELECT t1.dep_name, t1.id_dep as `obj_id`, t1.id_par, t2.dep_name as `par_dep_name`';
    SqlStr:= SqlStr + ', t2.id_dep FROM dep_list t1 LEFT JOIN dep_list t2 on t1.id_par=';
    SqlStr:= SqlStr + 't2.id_dep WHERE t1.id_dep_group=' + QuotedStr('������');
  end;
  if dm.DoQuery(dm.RTQuery, SqlStr) then
  begin
    ListBox2.Clear;
    while not dm.RTQuery.Eof do
    begin
      ItemText:= dm.RTQuery.FieldByName('par_dep_name').AsString + '->' + dm.RTQuery.FieldByName('dep_name').AsString;
      ListBox2.AddItem(ItemText, TObject(dm.RTQuery.FieldByName('obj_id').AsInteger));
      dm.RTQuery.Next;
    end;
  end;
end;

procedure TForm4.Edit4Change(Sender: TObject);
var
  SqlStr, ItemText: String;
begin
  if Switch1.State = tsson then
  begin
    if Length(Edit4.Text) > 0 then
    begin
      SqlStr:= 'SELECT t1.dep_name, t1.id_dep as `obj_id`, t1.id_par, t2.dep_name as `par_dep_name`';
      SqlStr:= SqlStr + ', t2.id_dep FROM dep_list t1 LEFT JOIN dep_list t2 on t1.id_par=';
      SqlStr:= SqlStr + 't2.id_dep WHERE t1.dep_name like ';
      SqlStr:= SqlStr + QuotedStr('%'+Edit4.Text+'%') + ' and t1.id_dep_group=';
      SqlStr:= SqlStr + QuotedStr(CmbUpperObjType.Items[CmbUpperObjType.ItemIndex]);
      if dm.DoQuery(dm.RTQuery, SqlStr) then
      begin
        ListBox1.Clear;
        while not dm.RTQuery.Eof do
        begin
          ItemText:= dm.RTQuery.FieldByName('par_dep_name').AsString + '->' + dm.RTQuery.FieldByName('dep_name').AsString;
          ListBox1.AddItem(ItemText, TObject(dm.RTQuery.FieldByName('obj_id').AsInteger));
          dm.RTQuery.Next;
        end;
      end;
    end
    else
    begin
      SqlStr:= 'SELECT t1.dep_name, t1.id_dep as `obj_id`, t1.id_par, t2.dep_name as `par_dep_name`';
      SqlStr:= SqlStr + ', t2.id_dep FROM dep_list t1 LEFT JOIN dep_list t2 on t1.id_par=';
      SqlStr:= SqlStr + 't2.id_dep WHERE t1.id_dep_group=' + QuotedStr(CmbUpperObjType.Items[CmbUpperObjType.ItemIndex]);
      if dm.DoQuery(dm.RTQuery, SqlStr) then
      begin
        ListBox1.Clear;
        while not dm.RTQuery.Eof do
        begin
          ItemText:= dm.RTQuery.FieldByName('par_dep_name').AsString + '->' + dm.RTQuery.FieldByName('dep_name').AsString;
          ListBox1.AddItem(ItemText, TObject(dm.RTQuery.FieldByName('obj_id').AsInteger));
          dm.RTQuery.Next;
        end;
      end;
    end;
  end;
end;

procedure TForm4.Edit5Change(Sender: TObject);
var
  SqlStr, ItemText: String;
begin
  if Switch1.State = tsson then
  begin
    if Length(Edit5.Text) > 0 then
    begin
      SqlStr:= 'SELECT t1.dep_name, t1.id_dep as `obj_id`, t1.id_par, t2.dep_name as `par_dep_name`';
      SqlStr:= SqlStr + ', t2.id_dep FROM dep_list t1 LEFT JOIN dep_list t2 on t1.id_par=';
      SqlStr:= SqlStr + 't2.id_dep WHERE t1.dep_name like ';
      SqlStr:= SqlStr + QuotedStr('%'+Edit5.Text+'%') + ' and t1.id_dep_group=';
      SqlStr:= SqlStr + QuotedStr(ComboBox3.Items[ComboBox3.ItemIndex]);
      if dm.DoQuery(dm.RTQuery, SqlStr) then
      begin
        ListBox2.Clear;
        while not dm.RTQuery.Eof do
        begin
          ItemText:= dm.RTQuery.FieldByName('par_dep_name').AsString + '->' + dm.RTQuery.FieldByName('dep_name').AsString;
          ListBox2.AddItem(ItemText, TObject(dm.RTQuery.FieldByName('obj_id').AsInteger));
          dm.RTQuery.Next;
        end;
      end;
    end
    else
    begin
      SqlStr:= 'SELECT t1.dep_name, t1.id_dep as `obj_id`, t1.id_par, t2.dep_name as `par_dep_name`';
      SqlStr:= SqlStr + ', t2.id_dep FROM dep_list t1 LEFT JOIN dep_list t2 on t1.id_par=';
      SqlStr:= SqlStr + 't2.id_dep WHERE t1.id_dep_group=' + QuotedStr(ComboBox3.Items[ComboBox3.ItemIndex]);
      if dm.DoQuery(dm.RTQuery, SqlStr) then
      begin
        ListBox2.Clear;
        while not dm.RTQuery.Eof do
        begin
          ItemText:= dm.RTQuery.FieldByName('par_dep_name').AsString + '->' + dm.RTQuery.FieldByName('dep_name').AsString;
          ListBox2.AddItem(ItemText, TObject(dm.RTQuery.FieldByName('obj_id').AsInteger));
          dm.RTQuery.Next;
        end;
      end;
    end;
  end;
end;

procedure TForm4.Edit6Change(Sender: TObject);
var
  SqlStr, ItemText: String;
begin
  if Switch1.State = tsson then
  begin
    if Length(Edit6.Text) > 0 then
    begin
      SqlStr:= 'SELECT t1.dep_name, t1.id_dep as `obj_id`, t1.id_par, t2.dep_name as `par_dep_name`';
      SqlStr:= SqlStr + ', t2.id_dep FROM dep_list t1 LEFT JOIN dep_list t2 on t1.id_par=';
      SqlStr:= SqlStr + 't2.id_dep WHERE t1.dep_name like ';
      SqlStr:= SqlStr + QuotedStr('%'+Edit6.Text+'%') + ' and t1.id_dep_group=';
      SqlStr:= SqlStr + QuotedStr(ComboBox1.Items[ComboBox1.ItemIndex]);
      if dm.DoQuery(dm.RTQuery, SqlStr) then
      begin
        ListBox3.Clear;
        while not dm.RTQuery.Eof do
        begin
          ItemText:= dm.RTQuery.FieldByName('par_dep_name').AsString + '->' + dm.RTQuery.FieldByName('dep_name').AsString;
          ListBox3.AddItem(ItemText, TObject(dm.RTQuery.FieldByName('obj_id').AsInteger));
          dm.RTQuery.Next;
        end;
      end;
    end
    else
    begin
      SqlStr:= 'SELECT t1.dep_name, t1.id_dep as `obj_id`, t1.id_par, t2.dep_name as `par_dep_name`';
      SqlStr:= SqlStr + ', t2.id_dep FROM dep_list t1 LEFT JOIN dep_list t2 on t1.id_par=';
      SqlStr:= SqlStr + 't2.id_dep WHERE t1.id_dep_group=' + QuotedStr(ComboBox1.Items[ComboBox1.ItemIndex]);
      if dm.DoQuery(dm.RTQuery, SqlStr) then
      begin
        ListBox3.Clear;
        while not dm.RTQuery.Eof do
        begin
          ItemText:= dm.RTQuery.FieldByName('par_dep_name').AsString + '->' + dm.RTQuery.FieldByName('dep_name').AsString;
          ListBox3.AddItem(ItemText, TObject(dm.RTQuery.FieldByName('obj_id').AsInteger));
          dm.RTQuery.Next;
        end;
      end;
    end;
  end;
end;

procedure TForm4.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ClearFields;
end;

procedure TForm4.FormShow(Sender: TObject);
begin
  Switch1.State:= TssOff;
  CmbObjectType.ItemIndex:= Form1.Combobox1.ItemIndex;
  CmbObjectTypeChange(Form4);
  if CmbObjectType.ItemIndex < 3 then
  begin
    LoadObjectData(Form1.GridPhoneBook.DataSource.DataSet.FieldByName('id_dep').AsInteger, CmbObjectType.ItemIndex);
  end
  else
    LoadObjectData(Form1.GridPhoneBook.DataSource.DataSet.FieldByName('id_empl').AsInteger, CmbObjectType.ItemIndex);
  ChangeMode('View');
end;

end.
